<?php

use Drupal\rdfxp_arc2\DependencyAware;

require_once 'visitors.php';

// OntClass.php
require_once RDFAPI_INCLUDE_DIR . 'resModel/ResIterator.php';

// OntResource
require_once RDFAPI_INCLUDE_DIR . 'resModel/ResResource.php';

// OntModel
require_once RDFAPI_INCLUDE_DIR . 'ontModel/OntResource.php';
require_once RDFAPI_INCLUDE_DIR . 'ontModel/OntClass.php';
require_once RDFAPI_INCLUDE_DIR . 'ontModel/OntProperty.php';
require_once RDFAPI_INCLUDE_DIR . 'ontModel/OntVocabulary.php';
require_once RDFAPI_INCLUDE_DIR . 'ontModel/Individual.php';

require_once RDFAPI_INCLUDE_DIR . 'resModel/ResModel.php';
require_once RDFAPI_INCLUDE_DIR . 'resModel/ResLiteral.php';

// ResModel
require_once RDFAPI_INCLUDE_DIR . 'resModel/ResProperty.php';

require_once RDFAPI_INCLUDE_DIR . 'ontModel/OntModel.php';

interface SwoopOntologyElement {

}

class SwoopElement implements WalkableElement, SwoopOntologyElement {
  public function hasVisitor($visitorName) {
    return isset($this->visitors[$visitorName]);
  }

  public function accept(MethodsVisitor $visitor) {

    $visitor->visit($this);

    foreach ($visitor->methods(NULL) as $method) {
      $this->callbacks[$method]['callback'] = array($visitor, $method);
      $this->callbacks[$method]['arguments'] = array();
    }
  }

  public function __construct(array $items) {
    $this->items = $items;
  }

  public function items($items = null) {

    if(isset($items) && is_array($items)) {
      $this->items = $items;
    }

    return $this->items;
  }

  // Element functions ()
  // Pseudo visitor pattern
  public function walker(WalkVisitor $visitor) {

    $this->visitors['walker'] = $visitor;

    $this->accept($visitor);
  }



  public function __call($name, $arguments) {

    if(isset($this->callbacks[$name])) {

      $arguments = array_merge($this->callbacks[$name]['arguments'], $arguments);
      $return = call_user_func_array($this->callbacks[$name]['callback'], $arguments);
    }else {
      $return = null;
    }

    return $return;
  }
}

/**
 * See decorator pattern
 */
interface SwoopOntologyComponent extends SwoopOntologyElement {
  public function getLocalName();
  public function getUri();
  public function getNamespace();
  public function getAssociatedModel();
//   public function properties();
}

interface SwoopOntologyLitteral extends SwoopOntologyElement{

}

class SwoopModel extends OntModel {

  protected $modelId = null;

	/**
	* Creates an OntModel that wraps an existing base model.
	* $vocabulary defines the ontology language. Currently only
	* RDFS_VOCABULARY is supported.
	*
	* @param   	object  Model	$baseModel
	* @param   	constant  	$vocabulary
	* @return	object		OntModel
	* @access	public
	*/
	public static function &getSwoopModelForBaseModel(&$baseModel, $vocabulary)
	{

		switch ($vocabulary)
		{
			case RDFS_VOCABULARY:
				require_once(RDFAPI_INCLUDE_DIR.'ontModel/'.RDFS_VOCABULARY);
				$vocab_object = new RdfsVocabulary();
				break;
			case OWL_VOCABULARY:
			  require_once(RDFAPI_INCLUDE_DIR.'ontModel/'.OWL_VOCABULARY);
			  $vocab_object = new OWLVocabulary();
			  break;
			default:
                trigger_error("Unknown vocabulary constant '$vocabulary'; only RDFS_VOCABULARY is supported", E_USER_WARNING);
                $vocab_object = null;
				break;
		}
		$mod = new SwoopModel($baseModel, $vocab_object);
		return $mod;
	}

  /**
   * Answer a resource that represents a class description node in this model.
   * @todo #normal: If a resource with the given uri exists in the model, it will be re-used.
   * If not, a new one is created in the updateable sub-model of the ontology model.
   *
   * @param	string	$uri
   * @return	object OntClass
   * @access	public
   */
  function createOntClass($uri = null)
  {
    $class = new SwoopClass($uri);
    $class->setAssociatedModel($this);
    $class->setVocabulary($this->vocabulary);
    $class->setInstanceRdfType($this->vocabulary->ONTCLASS());
    return $class;
  }

  /**
   * Answer a resource that represents an Individual node in this model.
   * @todo #normal: If a resource with the given uri exists in the model, it will be re-used.
   * If not, a new one is created in the updateable sub-model of the ontology model.
   *
   * @param	string	$uri
   * @return	object Individual
   * @access	public
   */
  function createIndividual($uri = null)
  {
    $individual = new SwoopIndividual($uri);
    $individual->setAssociatedModel($this);
    $individual->setVocabulary($this->vocabulary);
    return $individual;
  }

  /**
   * Answer a resource that represents an OntProperty node in this model.
   * @todo #normal: If a resource with the given uri exists in the model, it will be re-used.
   * If not, a new one is created in the updateable sub-model of the ontology model.
   *
   * @param	string	$uri
   * @return	object OntProperty
   * @access	public
   */
  function createOntProperty($uri = null)
  {
    $ontProperty = new SwoopProperty($uri);
    $ontProperty->setAssociatedModel($this);
    $ontProperty->setVocabulary($this->vocabulary);
    $ontProperty->setInstanceRdfType($this->createResource(RDF_NAMESPACE_URI.RDF_PROPERTY));
    return $ontProperty;
  }

  function createOntResource($uri = null)
  {
    $ontResource = new SwoopResource($uri);
    $ontResource->setAssociatedModel($this);
    $ontResource->setVocabulary($this->vocabulary);
    return $ontResource;
  }

  /**
   * @TODO sparqlFind. From performance point of view, it becomes required (against the current find() methods) for bigger dataset or using lot of conditions
   * Meanwhile use sparqlQuery(), then current find() methods on the resulting sub model.
   *
   * See SwoopOntology
     // Every new matches do an UNION
     // steps of one xpath do an intersection
     // Two successive xpath do U(/\)
   * Diference is that with SwoopOntologyRelToXmiIterator (+ find() and rdqlFind()) we can choose the result nested structure,
   * but with rdfxp_model_explore() (+ rev(), rel() and sparqlQuery() we just have ordered flattern triples. Maybe with walk history of rev() and rel()
   * we can restore the wanted nested structure.
   */
  public function rdqlFind($subject, $predicate, $object, $conditions = array()) {

    $vars = array('?s', '?p', '?o');
    foreach ($resources = array($subject, $predicate, $object) as $i => $resource) {
      if(is_object($resource)) {
        $resources[$i] = '<' . $resource->getUri() . '>';
      }
      // compat signature with find
      if($resources[$i] === NULL) {
        $resources[$i] = $vars[$i];
      }

      if(strpos($resources[$i], '?') === 0) {
        $vars[$i] = $resources[$i];
      }
      else {
        unset($vars[$i]);
      }
    }

    $prefixes = '';
    foreach ($namespaces = $this->getParsedNamespaces() as $ns => $prefix) {
      $prefixes .= "\n" . $prefix . ' FOR <' . $ns . '>';
    }

    $walker = new SwoopOntologyConditionsIterator();
    if($swoopElement = $walker->setContainer($conditions)) {
      $swoopElement->walkRecursive1();
      $conditions_str = $walker->context();
    }

    $query = '
  SELECT ' . implode(' ', $vars) . '
  WHERE (' . implode(', ', $resources) . ')
  ' . (!empty($conditions_str) ? 'AND ' . $conditions_str : '') . '
  USING ' . $prefixes;

    krumo($query);

    $results = array();
//     ->find($item['current'], $prop, null);

    $queryResults = $this->rdqlQuery($query);

    if(reset(reset($queryResults)) === NULL) {
      return array();
    }

    foreach ($this->rdqlQuery($query) as $i => $resultRes) {
      $result = array();
      foreach ($resources as $ii => $resource) {
        // from result
        if(strpos($resources[$ii], '?') === 0) {
//           if(!is_object($resultRes[$resources[$ii]])) {
//             krumo($query, $resultRes[$resources[$ii]]);
//           }
          if($resultRes[$resources[$ii]] instanceof Literal) {
            $uri = $resultRes[$resources[$ii]]->getLabel();
          }
          else {
            $uri = $resultRes[$resources[$ii]]->getUri();
          }
        }
        // full uri
        else if(strpos($resource, '<') === 0 && strrpos($resource, '>') === ($strlen = strlen($resource)-1)) {
          $uri = substr($resource, 1, $strlen-1);
        }
        // prefixed short name
        else {
          list($prefix, $shortName) = explode(':', $resource);
          $uri = array_search($prefix, $namespaces) . $shortName;
        }

        $result[$ii] = $this->createOntResource($uri);
      }

      $results[] = $result;
    }
//     krumo($results);
    return $results;
  }

  /**
   *
   * @param string $query
   * @param string $with_prefix
   * @return SwoopModel
   */
  function sparqlQuery($query, $with_prefix = false) {
//     $namespaces = $this->model->parsedNamespaces;
    //   $namespaces[$ontModel->model->baseURI];

    if(!$with_prefix) {
      $prefixes = '';
      foreach ($this->model->parsedNamespaces as $uri => $prefix) {
        $prefixes .= 'PREFIX ' . $prefix . ': <' . $uri . '>' . "\n";
      }

      $prefixed_query = $prefixes . $query;

    }
    else {
      $prefixed_query = $query;
    }

    // Html format only if the sparql form was submitted
    // TODO: #normal: to document or find a better way
    $format = NULL;
    if(Drupal::request()->request->get('form_id') == 'rdfxp_sparql_form') {
      if(strpos($query, 'CONSTRUCT') === FALSE) {
        $result = $this->model->sparqlQuery($prefixed_query, 'HTML');
      }
    }
    else {
      $result = $this->model->sparqlQuery($prefixed_query);
    }

    //CONSTRUCT Query : Mem model
    //SELECT QUERY : Array of triples : ?s ?p ?o witch are Resource objects
    // Visualize only Mem model
    if(is_object($result) && in_array('Model', class_parents($result)) ) {

      // rdfxp_get_model(modelId()) will return the original model
      // @TODO use second line instead of the first
      $result->originalModelid = $this->modelId();

      $toRenderableOntModel = SwoopModel::getSwoopModelForBaseModel($result, OWL_VOCABULARY);
      $toRenderableOntModel->modelId($this->modelId());
      $toRenderableOntModel->model->parsedNamespaces = $this->model->parsedNamespaces;

      return $toRenderableOntModel;
    }

    return $result;
  }

  /**
   * See SwoopProperty::subProperties()
   */
  function modelId($modelId = null) {
    $propName = 'modelId';

    if(isset(${$propName})) {
      $this->{$propName} = ${$propName};
      // Compat for setting value
      $this->model->modelid = $this->{$propName};
    }

    if(isset($this->{$propName}) && ${$propName} !== TRUE) {
      return $this->{$propName};
    }

    return $this->{$propName};
  }
}

class SwoopResource extends OntResource implements SwoopOntologyComponent {

  /**
   *
   * @See SwoopOntology::defaults()
   * @TODO traits or assert code compare
   */
  public function defaults($prop = null) {

    if(!isset($this->defaults)) {
      $class = get_class($this);
      $this->defaults = array_intersect_key(get_class_vars($class), array_flip(get_class_methods($class)) + array('defaults' => true));
    }

    if(isset($prop) && array_key_exists($prop, $this->defaults)) {
      $this->{$prop} = $this->defaults[$prop];
    }

    return $this->defaults;
  }

}

/**
 *
 * xml is array
 */
// class SwoopLitteral extends SwoopArray{
class SwoopLiteral extends ResLiteral implements WalkableElement, SwoopOntologyLitteral{
  protected $element;
  protected $visitors;

  public function __construct($str,$language = null) {
    parent::ResLiteral($str,$language);

    $this->element = new SwoopElement(array());
  }

  public function accept(MethodsVisitor $visitor) {
    $this->element->accept($visitor);
    $visitor->visit($this);
  }

  public function __call($name, $arguments) {
    return call_user_func_array(array($this->element, $name), $arguments);
  }

  public function walker(WalkVisitor $visitor) {
    $this->visitors['walker'] = $visitor;
    $this->accept($visitor);
  }

  public function hasVisitor($visitorName) {
    return isset($this->visitors[$visitorName]);
  }

  public function items($items = null) {
    return $this->element->items($items);
  }

  /**
   * default is string
   */
  public function renderable() {
    return $this->label;
  }
}

class SwoopLiteralXml extends SwoopLiteral {
  public function renderable() {
    $xmlElement = simplexml_load_string($this->label);

    $pattern = '/(^|\s+)([^\s]+)::__set_state\(array\(/';
    $replace = '\1((object)array(\'object\' => \'\2\',';

    eval('$config = array(' . var_export($xmlElement->getName(), true) . ' => ' . preg_replace($pattern, $replace, var_export($xmlElement, true)) . ');');

    // reccurse will continue on next call of getChildren()
    // Assume it is 2 type capable (for dimensions) iterator : property and array
    $this->items($config);

    return 'XMLLiteral';
  }
}

class SwoopLiteralHtml extends SwoopLiteral {

  public function renderable() {

    $simpleDom = simplexml_load_string($this->label);
    $bodyEltsDom = $simpleDom->xpath('/html/body/*');

    if(count($bodyEltsDom)) {
      $html = '';
      foreach ($bodyEltsDom as $bodyEltDom) {
        $html .= $bodyEltDom->asXML();
      }
    }
    else {
      $html = $this->label;
    }

    return $html;
  }
}

class SwoopLiteralObject extends SwoopLiteral {
  public function renderable() {

    $this->items(array($this->label));
    return 'ObjectLiteral';
  }
}

/**
 * Is current resource. Default is first resource of $matches
 * Reimplement qp methods
 * @TODO merge with SwoopResource
 */
class SwoopOntology extends SwoopElement implements ProxyElement, SwoopOntologyComponent {
  protected $comment = null;
  public $isDefinedBy = array();
  public $seeAlso = array();
  public $label = array();
  /**
   *
   * @var SwoopClass[]
   */
  protected $rdfTypes = null;
  public $resource = null;
  public $matches = array();
  public $triples = array();
  public $walkStructure = array();
  public $currentWalk = array();
  public $walkIndex = array();

  protected $properties = NULL;
  /**
   *
   * @var MethodsElement
   */
  protected $element = NULL;

  /**
   * See also command pattern
   */
  protected $callbacks = array();

  protected $visitors = null;

  public function __construct(SwoopOntologyComponent $resource) {

    $this->resource = $resource;

    $this->matches = array($this);

    if(isset($resource->recursionMarker)) {
      $this->recursionMarker = $resource->recursionMarker;
    }

    foreach($resource->listIsDefinedBy() as $prop) {
      $this->isDefinedBy[] = $prop->toString();
    }

    foreach($resource->listSeeAlso() as $prop) {
      $this->seeAlso[] = $prop->toString();
    }

    foreach($resource->listLabelProperties(false) as $prop) {
      $this->label[] = $prop->toString();
    }

  }

  public function comment() {

    foreach($this->resource->listComments(false) as $prop) {

      if($prop instanceof Literal) {
        $comment = $prop->getLabel();

        if(substr($comment, 0, 6) == '<html>') {
          $commentLiteral = new SwoopLiteralHtml($comment);
        }
        else {
          $commentLiteral = new SwoopLiteral($comment);
        }

        $this->comment[] = $commentLiteral;
      }

      $object = new stdClass();
      $object->prop1 = 'val1';
      $object->prop2 = array('key1' => 'val2', 0 => 'val3');

      $this->comment[] = new SwoopLiteralObject($object);
      // for seeAlso
//       else {
//         $comment = $prop;
//       }

    }

    return $this->comment;
  }

  public function rdfTypes($values = NULL) {

    $this->rdfTypes = array();

    // Setting external value + control
    // From aspect - this->domainOf : SwoopProperty[]
    if(isset($values) && is_array($values)) {
      foreach ($values as $value) {
        if($value instanceof SwoopClass) {
          $this->rdfTypes[] = $value;
        }
      }

      return $this->rdfTypes;
    }

    $this->rdfTypes = $this->resource->listProperty($this->resource->vocabulary->TYPE(), 'OntClass');

    return $this->rdfTypes;
  }

  public function getShortName() {
    $namespaces = $this->getAssociatedModel()->model->parsedNamespaces;
    return $namespaces[$this->getNamespace()] . ':' . $this->getLocalName();
  }

  /**
   *
   * @return array(new Litteral()). somewhere ->__toString()
   */
  public function shortName() {

    return array(new SwoopLiteral($this->getShortName()));
  }

  /**
   * @retrun SwoopClass
   */
  function listRDFTypes($direct = true)
  {
    return $this->resource->listProperty($this->resource->vocabulary->TYPE(), 'OntClass');
  }

  // Decorators functions
  public function getLocalName() {
    return $this->resource->getLocalName();
  }

  public function getUri() {
    return $this->resource->getUri();
  }

  public function getNamespace() {
    return $this->resource->getNamespace();
  }

  public function getAssociatedModel() {
    return $this->resource->getAssociatedModel();
  }

  public function properties() {

    $this->properties = array();
    foreach (array_merge($this->defaults(), array('shortName' => TRUE)) as $name => $default) {
      if(!in_array($name, array('properties'))) {
        $this->properties[$name] = $this->{$name}();
      }
    }

    if(isset($this->recursionMarker)) {
      $this->properties['recursionMarker'] = $this->recursionMarker;
    }

    return $this->properties;
  }

  public function proxy(ProxyVisitor $visitor) {

    $this->visitors['proxy'] = $visitor;


    $this->accept($visitor);
  }

  public function __call($name, $arguments) {

    // @TODO visitor part must override decored part - currently it is the reverse
    if(array_key_exists($name, $this->decoredDefaults())) {
      return call_user_func_array(array($this->resource, $name), $arguments);
    }

    return parent::__call($name, $arguments);
  }

  public function decoredDefaults($prop = null) {
    // decored (pseudo parent)
    $decoredDefaults = $this->resource->defaults();

    // reset also in pseudo parent
    if(isset($prop) && array_key_exists($prop, $decoredDefaults)) {
      $this->resource->defaults($prop);
    }

    return $decoredDefaults;
  }

  /**
   * Starting implementation:
   * Needed condition propname has the same name like the getter|setter|reset method
   */
  public function defaults($prop = null) {


    if(!isset($this->defaults)) {
      $class = get_class($this);
      $this->defaults = array_intersect_key(get_class_vars($class), array_flip(get_class_methods($class)) + array('defaults' => true));
    }

    if(isset($prop) && array_key_exists($prop, $this->defaults)) {
      $this->{$prop} = $this->defaults[$prop];
    }

    return array_merge($this->decoredDefaults($prop), $this->defaults);
  }

  public static function swoopType($ontResource) {

    $swoopType = 'SwoopIndividual';
    foreach($ontResource->listRDFTypes() as $typeRes) {
      if($typeRes->getNamespace() == OWL_NS && in_array($typeRes->getLocalName(), array('Class', 'DatatypeProperty', 'ObjectProperty'))) {
        if($typeRes->getLocalName() == 'Class') {
          $swoopType = 'SwoopClass';
        }
        else {
          $swoopType = 'SwoopProperty';
        }

        break;
      }
    }

    return $swoopType;
  }

  public function addResourceTag($resource) {
    $this->resource->addResourceTag($resource);
  }
}

/**
* @TODO merge with SwoopOntology. Currently extending because of parent::__construct() with some inits
* @TODO become flyweight object
*/
class SwoopOntologyRel extends SwoopOntology {

  protected $filterCallback = null;

  public function __construct(SwoopOntologyComponent $resource) {

    $this->resource = $resource;

    $this->matches = array($this);

    //$this->element = new SwoopElement(array());
  }

  public function filterCallback() {
    if(isset($this->filterCallback)) {
      return $this->filterCallback;
    }

    $this->filterCallback = function($name, $value) {

      $return = $value !== NULL
       && (
          strpos($name, 'rel') === 0
          || strpos($name, 'rev') === 0
        );


      return $return;
    };

    return $this->filterCallback;
  }

  public function properties($value = NULL) {

    // Setting external value + control
    if(isset($value) && is_array($value)) {
      $this->properties = $value;

      return $this->properties;
    }

    // Busyness processing
    $this->properties = array();
    foreach (get_object_vars($this) as $name => $value) {

      $function = $this->filterCallback();
      if($function($name, $value)) {
        $this->properties[$name] = $value;
      }
    }


    return $this->properties;
  }

  /**
   * Setter and getter. Setter is some like reset for cache
   * @param mixed $subProperties
   *   null : getter
   *   true|array : reset|setter
   *   For setting bool value to 'true' use '1' or '> 0' instead
   * @see attr()
   * @see double dispatch (visitor)
   */
  function getterSetterReset($propName, $value = NULL) {

    if(isset($value)) {
      $this->{$propName} = $value;
    }

    if(isset($this->{$propName}) && $value !== TRUE) {
      return $this->{$propName};
    }

    if(method_exists($this, $propName)) {
      return $this->{$propName}($value);
    }
  }

  public function setResource($resource) {
    $swoopMatches = $this;
    $swoopMatches->resource = $resource;
    $swoopMatches->setMatches(array($this));
    $swoopMatches->walkStructure = array();
    $swoopMatches->currentWalk = &$swoopMatches->walkStructure;
// krumo($this);
  }

  public function typeAndSuperClasses () {

    if(count($this->matches)) {
      $self = reset($this->matches);
      $ontResource = $self->resource;

      $swoopType = self::swoopType($ontResource);
    }

    $swoopMatches = $this;
    $walkPathArr = array();
    if($swoopType == 'SwoopIndividual') {
      //     $swoopResource->rel('rdf:type');
      // @TODO in hierarchy
      //     krumo($swoopMatches->resource->asClass()->listSuperClasses(false));
      // Do instead:
      $swoopMatches->rel('rdf:type');
      $walkPathArr[] = 'rdf:type';
      //       krumo($swoopMatches);
      do {
        $swoopMatches->rel('rdfs:subClassOf');
        $walkPathArr[] = 'rdfs:subClassOf';
        //         krumo($swoopMatches);
      } while($swoopMatches->length);

      // @TODO human readable version of walkedToSparql()
      if(preg_match('%^rdfxp/rdf_rdfxp%', \Drupal::request()->getRequestUri())) {
        // cache id:
        // rdf:type/rdfs:subClassOf/rdfs:subClassOf
        // ./false/0, ./false/1, ./false/-1
        krumo($walkPathArr);
  // krumo($swoopMatches);
        krumo($swoopMatches->walkedToSparql());
        krumo($swoopMatches->walkedToSparql(false, 1));
        krumo($swoopMatches->walkedToSparql(false, -1));
      }

      /*  $swoopMatchesClone->rel('rdf:type/rdfs:subClassOf');
       krumo($swoopMatchesClone);
      krumo($swoopMatchesClone->walkedToSparql());
      krumo($swoopMatchesClone->walkedToSparql(false, true)); */
    }
    else if($swoopType == 'SwoopClass') {

      $resourceOrig = $swoopMatches->resource;
      do {
        $matches = $swoopMatches->matches;
        $currentWalk = $swoopMatches->currentWalk;
        $swoopMatches->rev('rdfs:subClassOf');
//                 krumo($swoopMatches);
      } while($swoopMatches->length);
      // because last matches are empty set with pre-last matches : $matches
      $swoopMatches->setMatches($matches);
      $swoopMatches->currentWalk = $currentWalk;

      $swoopMatches->rev('rdf:type');
      //       krumo($swoopMatches);

//       $time_end4 = microtime_float();
//       $time = $time_end4 - $time_end3;
//       krumo('Some rev(): ' . $time); // 0.008s

//       krumo($swoopMatches->walkedToSparql(true));
//       krumo($swoopMatches->walkedToSparql(true, true));

//       $time_end5 = microtime_float();
//       $time = $time_end5 - $time_end4;
//       krumo('walkedToSparql normal + contextual: ' . $time); // 0.005s

//       $swoopMatches->setResource($resourceOrig);
//       $swoopMatches->rev('rdf:type');
      // just for fun
//       $swoopMatches->rev('rdfxp-model:fieldInstance');

      $swoopMatches->setResource($resourceOrig);

      do {
        $swoopMatches->rel('rdfs:subClassOf');
      } while($swoopMatches->length);

    }

  }

  /**
   * instances - rev(rdf:type)
   * subClasses - rev(rdfs:subClassOf)
   * ex. - all subClasses then instances - do rev(rdfs:subClassOf) while length; rev(rdf:type);
   *
   * @TODO rel(); then rev()
   */
  public function rev($relative_selector, $explore = false) {
    $this->rel($relative_selector, true, $explore);
  }

  /**
   * See qp::find() but in rel direction
   *
   * Every new matches do an UNION
   * steps of one xpath do an intersection
   * Two successive xpath do U(/\)
   */
  public function rel($relative_selector, $is_rev = false, $explore = false) {
    $debugStructure = false;

    // @TODO escape / if present in step
    $parents = explode('/', $relative_selector);

    $namespaces = $this->resource->model->model->parsedNamespaces;

    if(empty($this->walkStructure)) {
//       $this->walkStructure = $this->matches;
      $this->currentWalk = &$this->walkStructure;
    }
    $tmp = array();
    foreach ($parents as $i => $step) {

      if($step == '*') {
        $prop = null;
        $step = '?p';
      }
      else {
        list($prefix, $localName) = explode(':', $step);
//         krumo($this->resource->model->model->parsedNamespaces);
        $namespace = array_search($prefix, $this->resource->model->model->parsedNamespaces);
        $prop = new ResProperty($namespace . $localName);
      }

      // Every new matches do an UNION
      // steps of one xpath do an intersection
      // Two successive xpath do U(/\)
      if(!$i) {
        $end_i = count($this->triples);
        foreach ($this->matches as $matches_i => $current) {
          $namespaces = $current->resource->model->model->parsedNamespaces;
          if($is_rev) {
            $this->triples[$end_i + $matches_i][] = array(count($parents)-1 == $i ? '?s' : '?b' . $i, $step, $namespaces[$current->getNamespace()] . ':' . $current->getLocalName());
          }
          else {
            $this->triples[$end_i + $matches_i][] = array($namespaces[$current->getNamespace()] . ':' . $current->getLocalName(), $step, count($parents)-1 == $i ? '?o' : '?b' . $i);
          }
        }
      }
      else {
        foreach ($this->triples as $matches_i => $triples) {
          if($is_rev) {
            $this->triples[$end_i + $matches_i][] = array( count($parents)-1 == $i ? '?s' : '?b' . $i, $step, '?b' . ($i-1));
          }
          else {
            $this->triples[$end_i + $matches_i][] = array('?b' . ($i-1), $step, count($parents)-1 == $i ? '?o' : '?b' . $i);
          }
        }
      }

// krumo($prop);
      $matches = $this->matches;
      $this->matches = array();

      foreach ($matches as $current_i => $current) {

        if(!isset($this->currentWalk[$current_i])) {
          if(!empty($debugStructure)) {
            $this->currentWalk[$current_i] = (object)array('uri' => $current->getLocalName());
          }
          else {
            $this->currentWalk[$current_i] = $current;
          }
        }
// krumo($current);

        if($is_rev) {
          $statements = $current->resource->model->find(null, $prop, $current->resource);
        }
        else {
          $statements = $current->resource->model->find($current->resource, $prop, null);
        }
// krumo($statements);

//         $currentStdClass = (object)array('uri' => $current->getLocalName());

        foreach ($statements as $statement)
        {
          if($is_rev) {
            $objectLabel=$statement->getLabelSubject();
          }
          else {
            $objectLabel=$statement->getLabelObject();
          }
//           krumo($statement);
//           krumo($objectLabel);

          $direction = $is_rev? 'rev' : 'rel';
//           $classProp = __FUNCTION__ . preg_replace('/:([a-zA-Z])/e', 'ucfirst(\\1)', $namespaces[$statement->getPredicate()->getNamespace()] . ':' . $statement->getPredicate()->getLocalName());
          $classProp = $direction . ucfirst($namespaces[$statement->getPredicate()->getNamespace()]) . ucfirst($statement->getPredicate()->getLocalName());
          if(!isset($current->{$classProp})) {
            $current->{$classProp} = array();

          }

          if(!isset($this->currentWalk[$current_i]->{$classProp})) {
            $this->currentWalk[$current_i]->{$classProp} = array();
          }

          $object = null;
          if (empty($this->walkIndex[$objectLabel]))
          {


//             $baseUri = $current->resource->model->model->baseURI;

            $ontResource = new SwoopResource($objectLabel);
            $ontResource->setAssociatedModel($current->resource->model);
            $ontResource->setVocabulary($current->resource->model->vocabulary);

            $class = get_class($this);
            $swoopResource = new $class($ontResource);

            $this->matches[] = $swoopResource;



//             krumo($classProp);
//             $current->{$classProp}[] = $swoopResource->getLocalName();
//             $current->{$classProp}[] = (object)array('uri' => $swoopResource->getLocalName());
//             $currentStdClass->{$classProp}[] = (object)array('uri' => $swoopResource->getLocalName());
//             $this->currentWalk[$current_i]->{$classProp}[] = (object)array('uri' => $swoopResource->getLocalName());
//             $this->currentWalk[$current_i]->{$classProp}[] = $swoopResource->getLocalName();
            if(!empty($debugStructure)) {
              $object = (object)array('uri' => $swoopResource->getLocalName());
            }
            else {
              $object = $swoopResource;
            }

            $this->walkIndex[$objectLabel] = $swoopResource;
//             $return[]=$this->model->createOntClass($objectLabel);
          }
          else if(!$explore) { //: is not working
//           else {
//             $current->{$classProp}[] = $this->walkIndex[$objectLabel];
//             $current->{$classProp}[] = (object)array('uri' => $this->walkIndex[$objectLabel]->getLocalName());
//             $currentStdClass->{$classProp}[] = (object)array('uri' => $this->walkIndex[$objectLabel]->getLocalName());
//             $this->currentWalk[$current_i]->{$classProp}[] = $this->walkIndex[$objectLabel];
//             $this->currentWalk[$current_i]->{$classProp}[] = (object)array('uri' => $this->walkIndex[$objectLabel]->getLocalName());
            if(!empty($debugStructure)) {
              $object = (object)array('uri' => $this->walkIndex[$objectLabel]->getLocalName());
            }
            else {
              $object = $this->walkIndex[$objectLabel];
            }

            $this->matches[] = $this->walkIndex[$objectLabel];
          }
          // On explore add some ancor to
          else{

            if($is_rev) {
              $ancorLabel=$statement->getLabelObject();
            }
            else {
              $ancorLabel=$statement->getLabelSubject();
            }

            $this->walkIndex[$objectLabel . '--' . 'anchor'] = array($classProp => $ancorLabel);
          }

          if(!empty($object)) {
            $this->currentWalk[$current_i]->{$classProp}[] = $object;
            $tmp[$i][] = $object;
          }
        }

//         $this->currentWalk[$current_i] = $current;
//         $this->walkStructure[] = $currentStdClass;

      }
      $this->currentWalk = &$tmp[$i];
    }


    $this->setMatches($this->matches);
  }

  public function setMatches(array $matches = array()) {

    $this->matches = $matches;

    // ! need to reset comment, isDefinedBy, ... They aren't good anymore
    $this->length = count($this->matches);
    if($this->length) {
      $reset = reset($this->matches);
      $this->resource = $reset->resource;
    }
    else {
      // keep old resource
    }
  }

  /**
   * Suitable for self::rel()
   * @param boolean $is_rev default to false
   * @param int $contextual step of contextual resource. default to 0 - for none. Test performance and graph for highter values
   * @TODO negative number toggle direction of contextual
   * @TODO use UI scroll to fluent increase $contextual
   * @TODO $contextual only for first, last, eq(i) with checkbox 'check all'
   */
  public function walkedToSparql($is_rev = false, $contextual = 0) {
//     $sparql = 'select ?s ?p ?o WHERE {';
    $sparql = '';
    foreach ($this->triples as $i => $grp_gph) {

      // Only for first and last triple. Every next $grp_gph is supposed to be adjacent(by rel or rev) to the previous
      if(!$i) {

//         $triples = reset($grp_gph);
//         $sparql .= !empty($sparql) ? "\n" . 'UNION ' : '';
//         $sparql .= '{';
//         $sparql .= "\n" . '?s ?p ?o .' .
//             "\n" . 'FILTER str(?s) = str(' . $triples[0] . ') and str(?p) = str(' . $triples[1] . ')' .
//             "\n" . '}';
      }



      if(
//           count($grp_gph) > 1 &&
          $contextual
        ) {

        // contextual ?s ?p ?o for every step
        // @TODO make optional intermediates levels ?s ?p ?o union -.
        // First and last are mandatory
        for($context_i = 0; $context_i <
//             count($grp_gph)-1;
            count($grp_gph);
            $context_i++
          ) {


          $sparql .= !empty($sparql) ? "\n" . 'UNION ' : '';
          $sparql .= '{' . "\n";

          foreach ($grp_gph as $ii => $triples) {

            if($ii <= $context_i) {
              if($context_i == $ii) {
                if($is_rev) {
                  $triples[0] = preg_replace('/\?(b[0-9]+|s)/', '?o', $triples[0]);
                }
                else {
                  $triples[2] = preg_replace('/\?(b[0-9]+|o)/', $contextual > 0? '?s' : '?o', $triples[2]);
                }
              }

              $sparql .= "\n" . implode(' ', $triples) . ' .';
            }

          }
          $sparql .= "\n" . '?s ?p ?o .' .
          "\n" . '}';
        }
      }

      $sparql .= !empty($sparql) ? "\n" . 'UNION ' : '';
      $sparql .= '{' . "\n";
      foreach ($grp_gph as $ii => $triples) {

        $sparql .= "\n" . implode(' ', $triples) . ' .';
      }

      $sparql .= "\n" . '?s ?p ?o .' .
          ($is_rev ?
              "\n" . 'FILTER str(?o) = str(' . $triples[2] . ') and str(?p) = str(' . $triples[1] . ')'
            : "\n" . 'FILTER str(?s) = str(' . $triples[0] . ') and str(?p) = str(' . $triples[1] . ')'
          ) .
      "\n" . '}';

    }
    $sparql .= "\n" . '}';

    return 'CONSTRUCT { ?s ?p ?o .} WHERE {' .
    "\n" . $sparql;
  }
}

class SwoopClass extends OntClass implements SwoopOntologyComponent {

// }
/**
 * Onother implementation will be HtmlOntologyClass (see swoop)
 * @TODO merge with  SwoopClass
 */
// class SwoopOntologyClass extends SwoopOntology{
  /**
   * rev owl:subClassOf: ?s subClassOf current
   * @var swoopClass[]
   */
  protected $subClasses = null;

  /**
   * rev owl:subClassOf: current subClassOf ?o
   * @var swoopClass[]
   */
  protected $superClasses = null;

  /**
   *
   * @var SwoopIndividual[]
   */
  protected $instances = null;
  /**
   * @var SwoopProperty[]
   */
  protected $domainOf = null;
  /**
   *
   * @var SwoopProperty[]
   */
  protected $rangeOf = null;

  protected $properties = null;

  public function __construct($uri = null) {

    parent::OntClass($uri);

  }

  /**
   *
   * @See SwoopOntology::defaults()
   * @TODO traits or assert code compare
   */
  public function defaults($prop = null) {

    if(!isset($this->defaults)) {
      $class = get_class($this);
      $this->defaults = array_intersect_key(get_class_vars($class), array_flip(get_class_methods($class)) + array('defaults' => true));
    }
    unset($this->defaults['properties']);

    if(isset($prop) && array_key_exists($prop, $this->defaults)) {
      $this->{$prop} = $this->defaults[$prop];
    }

    return $this->defaults;
  }

  /**
   * init() is doing mutable part(see light way pattern)
   */
  function init($reset = false) {
    $this->domainOf();
    $this->rangeOf();

    $this->subClasses();
    $this->superClasses();
    $this->instances();

    $this->properties();
  }

  /**
   * Only business. For performance AttrProxy is used
   */
  function superClasses($superClasses = NULL) {

    $this->superClasses = array();

    // Setting external value + control
    if(isset($superClasses) && is_array($superClasses)) {
      foreach ($superClasses as $superClass) {
        if($superClass instanceof SwoopClass) {
          $this->superClasses[] = $superClass;
        }
      }

      return $this->superClasses;
    }

    // Busyness processing
    foreach (parent::listSuperClasses() as $subClass)
    {
      $this->superClasses[] = $subClass;
    }

    return $this->superClasses;
  }

  function subClasses($subClasses = NULL) {

    $this->subClasses = array();

    // Setting external value + control
    // From aspect - this->subClasses : SwoopClass[]
    if(isset($subClasses) && is_array($subClasses)) {
      foreach ($subClasses as $subClass) {
        if($subClass instanceof SwoopClass) {
          $this->subClasses[] = $subClass;
        }
      }

      return $this->subClasses;
    }

    return $this->listSubClasses();
  }

  public function instances($instances = NULL) {
    $this->instances = array();

    // Setting external value + control
    if(isset($instances) && is_array($instances)) {
      foreach ($instances as $instance) {
        if($instance instanceof SwoopIndividual) {
          $this->instances[] = $instance;
          $this->addResourceTag($instance);
        }
      }

      return $this->instances;
    }

    $iterator = $this->listInstances();
    for ($iterator->rewind(); $iterator->valid(); $iterator->next())
    {
      $instance = $iterator->current();
      $this->instances[] = $instance; // SwoopIndividual[]
      $this->addResourceTag($instance);
    };


    usort($this->instances, function($a, $b) {
      return strcasecmp($a->getLocalName(), $b->getLocalName());
    });

    return $this->instances;
  }

  function listSubClasses($direct = true) {

    foreach (parent::listSubClasses($direct) as $i => $subClass)
    {
      $this->subClasses[$i] = $subClass;
    }

    return $this->subClasses;
  }

	function domainOf($values = NULL) {
	   $this->domainOf = array();

    // Setting external value + control
    // From aspect - this->domainOf : SwoopProperty[]
    if(isset($values) && is_array($values)) {
      foreach ($values as $value) {
        if($value instanceof SwoopProperty) {
          $this->domainOf[] = $value;
          $this->addResourceTag($value);
        }
      }

      return $this->domainOf;
    }

	  $return=array();
	  $resArray = $this->model->find(null,$this->vocabulary->DOMAIN(),$this);

	  foreach ($resArray as $statement) {
	    $value = $this->model->createOntProperty($statement->getLabelSubject());
	    $return[] = $value;
	    $this->addResourceTag($value);
	  }

	  $this->domainOf = $return;

	  return $this->domainOf;
	}

	function rangeOf() {
	  $this->rangeOf = array();
	  // Setting external value + control
    // From aspect - this->domainOf : SwoopProperty[]
    if(isset($values) && is_array($values)) {
      foreach ($values as $value) {
        if($value instanceof SwoopProperty) {
          $this->rangeOf[] = $value;
          $this->addResourceTag($value);
        }
      }

      return $this->rangeOf;
    }


	  $return=array();
	  $resArray = $this->model->find(null,$this->vocabulary->RANGE(),$this);

	  foreach ($resArray as $statement) {
	    $value = $this->model->createOntProperty($statement->getLabelSubject());
	    $return[] = $value;
	    $this->addResourceTag($value);
	  }

	  $this->rangeOf = $return;

	  return $this->rangeOf;
	}

	/**
	 * Use generic reccurse with stop condition and callback. See SimpleTestUtilityHelper::element_walk_recursive()
	 * Use config(export) to customize for this case
	 * See also iterator, iterator reccursive pattern
	 *
	 * key in current_path_arr is rdf:property - so explore|walk on rdf model (onthology) is same like recurse walk on one resource
	 *   mixe previous with sparql and triples (see also Ont* helper - listClasses, ... - work with statements)
	 *   then xpath for sparql helper (shortcut for frequently sparql queries)
	 *   shortcut or helper for already tested sparql cases
	 *
	 * What about drupal views requeteur
	 *   choose base table > choose CT > choose relations > choose fields, filters, sorts...
	 * display current search block tree (sublings for union, children for intersection)
	 * @param unknown $firstLevelClasses
	 */
  public function subClassesWalk(&$firstLevelClasses) {

    if(!empty($this->processed)) {
      return;
    }

    //$this->init();
    $this->subClasses();
    $this->superClasses();

    foreach ($this->subClasses as $i => $subClass) {


      // SwoopOntologyClass creates subClasses : OntClass[]
      $this->subClasses[$i]->subClassesWalk($firstLevelClasses);


      if(!empty($firstLevelClasses[$subClass->getUri()])) {
        unset($firstLevelClasses[$subClass->getUri()]);
      }
    }

    $this->processed = true;
  }

  public function properties() {

    foreach ($this->defaults() as $name => $default) {
      $this->properties[$name] = $this->{$name}();
    }

    return $this->properties;
  }

  public function addResourceTag($resource) {
    $tag = 'rdfxp:' . $this->model->modelId() . ':resource:' . (is_object($resource) ? $resource->getLocalName() : $resource);
    $this->tags[$tag] = $tag;
  }
}

class SwoopProperty extends OntProperty implements SwoopOntologyComponent {

  /**
   *
   * @var SwoopProperty[]
   */
  protected $subProperties = NULL;
  /**
   *
   * @var SwoopProperty[]
   */
  protected $superProperties = array();
  /**
   *
   * @var SwoopClass
   */
  protected $domain = NULL;
  /**
   *
   * @var SwoopOntologyElement
   */
  protected $range = NULL;
  protected $typeProperty = NULL;
  protected $rdfTypes = NULL;

  public function __construct($uri = null) {

    parent::OntProperty($uri);

  }

  public function init() {
    $this->superProperties();
    $this->subProperties();
    $this->domain();
    $this->range();
    $this->listRDFTypes();
    $this->typeProperty();
  }

  /**
   *
   * @See SwoopOntology::defaults()
   * @TODO traits or assert code compare
   */
  public function defaults($prop = null) {

    if(!isset($this->defaults)) {
      $class = get_class($this);
      $this->defaults = array_intersect_key(get_class_vars($class), array_flip(get_class_methods($class)) + array('defaults' => true));
    }

    if(isset($prop) && array_key_exists($prop, $this->defaults)) {
      $this->{$prop} = $this->defaults[$prop];
    }

    return $this->defaults;
  }

  public function domain() {
    $this->domain = array();

    // Setting external value + control
    // From aspect - this->domainOf : SwoopProperty[]
    if(isset($values) && is_array($values)) {
      foreach ($values as $value) {
        if($value instanceof SwoopClass) {
          $this->domain[] = $value;
        }
      }

      return $this->domain;
    }

    foreach (parent::listDomain() as $resResource)
    {


      $this->domain[] = $resResource;//->getLocalName();
    }

    return $this->domain;
  }

  public function range($values = null) {

    $this->range = array();

    // Setting external value + control
    // From aspect - this->domainOf : SwoopProperty[]
    if(isset($values) && is_array($values)) {
      foreach ($values as $value) {
        if($value instanceof SwoopOntologyElement) {
          $this->range[] = $value;
        }
      }

      return $this->range;
    }

    foreach (parent::listRange() as $resResource)
    {


      $this->range[] = $resResource;//->getLocalName();
    }

    return $this->range;
  }

  public function superProperties($values = null) {
    $this->superProperties = array();

    // Setting external value + control
    // From aspect - this->domainOf : SwoopProperty[]
    if(isset($values) && is_array($values)) {
      foreach ($values as $value) {
        if($value instanceof SwoopProperty) {
          $this->superProperties[] = $value;
        }
      }

      return $this->superProperties;
    }

    foreach (parent::listSuperProperties() as $i => $subClass)
    {
//       $subClass->setVocabulary($this->vocabulary);
//       $subClass->setAssociatedModel($this->model);
//       $subClass->setInstanceRdfType($this->model->createResource(RDF_NAMESPACE_URI.RDF_PROPERTY));
      $this->superProperties[$i] = $this->model->createOntProperty($subClass->getURI());
    }

    return $this->superProperties;
  }

  /**
   * Setter and getter. Setter is some like reset for cache
   * @param mixed $subProperties
   *   null : getter
   *   true|array : reset|setter
   *   For setting bool value to 'true' use '1' or '> 0' instead
   */
  public function subProperties($values = null) {
	   $this->subProperties = array();

    // Setting external value + control
    // From aspect - this->domainOf : SwoopProperty[]
    if(isset($values) && is_array($values)) {
      foreach ($values as $value) {
        if($value instanceof SwoopProperty) {
          $this->subProperties[] = $value;
        }
      }
// krumo($this->subProperties);
      return $this->subProperties;
    }

    //
    return $this->listSubProperties();

//     return $this->{$propName};
  }

  /**
   * Use self::subProperties()
   * @deprecated
   */
  function listSubProperties($direct = true) {

    foreach (parent::listSubProperties($direct) as $i => $subClass)
    {
//       $subClass->setVocabulary($this->vocabulary);
//       $subClass->setAssociatedModel($this->model);
//       $subClass->setInstanceRdfType($this->model->createResource(RDF_NAMESPACE_URI.RDF_PROPERTY));
      $this->subProperties[$i] = $this->model->createOntProperty($subClass->getURI());
    }

    return $this->subProperties;
  }

  public function subPropertiesWalk(&$firstLevelClasses) {

    if(!empty($this->processed)) {
      return;
    }

    $this->init();

    foreach ($this->listSubProperties() as $i => $subClass) {

//       krumo($subClass);
      // SwoopOntologyClass creates subClasses : OntClass[]
//       $this->subProperties[$i] = new SwoopOntologyProperty($subClass);
      $this->subProperties[$i]->subPropertiesWalk($firstLevelClasses);


      if(!empty($firstLevelClasses[$subClass->getUri()])) {
        unset($firstLevelClasses[$subClass->getUri()]);
      }
    }

    $this->processed = true;
  }

  function typeProperty() {

    if(isset($this->typeProperty)) {
      return $this->typeProperty;
    }

    foreach ($this->listRDFTypes() as $listRDFType) {
      if(in_array($listRDFType->uri , array(OWL_NS . 'DatatypeProperty', OWL_NS . 'ObjectProperty'))) {
        $this->typeProperty = $listRDFType->getLocalName();
      }
    }
  }


  /**
   * Answer an array of the RDF classes to which this resource belongs.
   * If $direct is true, only answer those resources that are direct types of
   * this resource, not the super-classes of the class etc.
   * @override
   *
   * @param	boolean	$direct
   * @return	array Array of SwoopClass
   * @access	public
   */
  function listRDFTypes($direct = true)
  {
    if(isset($this->rdfTypes)) {
      return $this->rdfTypes;
    }

    $this->rdfTypes = $this->listProperty($this->vocabulary->TYPE(), 'OntClass');

    return $this->rdfTypes;
  }
}

class SwoopIndividual extends Individual implements SwoopOntologyComponent {


  protected $likelyProperties = null;
  protected $objectAssertions = null;
  protected $datatypeAssertions = null;
  protected $propertyValueOf = null;

  protected $propertyValue = null;
  protected $pulledUpDependencies = null;

  public function __construct($uri = null) {


    parent::Individual($uri);

  }

  function init() {
    $this->likelyProperties();
// krumo($this->objectAssertions, $this->datatypeAssertions);exit;
    $this->propertyValueOf();

    $this->propertyValue();

  }

  /**
   *
   * @TODO V2 go to SwoopOntology.
   * V1 keep here because OntClass|OntProperty props and propsOf are ontology defined, so just for individuals
   *
   * Also for propertyValueOf: Compare ObjectProperties to deps aware => must be the same
   *  The gain there is the deps awere call.
   */
  public function propertyValue() {

    $this->propertyValue = array();
    $resArray = $this->model->find($this, null, null);

    foreach ($resArray as $statement) {

      // Property is already set somewhere
      $prop = new SwoopOntology($this->model->createOntProperty($statement->getPredicate()->getUri()));
      if(
          isset($this->datatypeAssertions[$prop->getShortName()])
          || isset($this->objectAssertions[$prop->getShortName()])
          || $prop->getShortName() == 'rdf:type'
        ) {
        continue;
      }
      $this->addResourceTag($prop);

      $object = $statement->getObject();
      // Litteral
      if($object instanceof Literal) {
        if($object->getDatatype() == RDF_NAMESPACE_URI . RDF_XMLLITERAL) {
          $ontResource = new SwoopLiteralXml($object->getLabel());
        }
        else {
          $ontResource = new SwoopLiteral($object->getLabel());
        }
        $ontResource->setAssociatedModel($this->model);
      }
      else {
        $ontResource = new SwoopResource($object->getUri());
        $ontResource->setAssociatedModel($this->model);
        $ontResource->setVocabulary($this->model->vocabulary);
        $this->addResourceTag($ontResource);
      }


      $this->propertyValue[$prop->getShortName()][] = $ontResource;

      usort($this->propertyValue[$prop->getShortName()], function($a, $b){
        return strcasecmp($a->getLocalName(), $b->getLocalName());
      });
    }

    return $this->propertyValue;
  }

  public function pulledUpDependencies() {
    return $this->pulledUpDependencies;
  }

  public function propertyValueOf() {
    $this->propertyValueOf = array();

    $rdf_type = $this->getRDFType();
    $drush_only = \Drupal::config('rdfxp.settings')->get('filesystem_index.drush_only');
    $drush_main = !$drush_only || $drush_only && function_exists('drush_main');
    $grouping_types_rev = \Drupal::config('rdfxp.settings')->get('grouping_types_rev');

    $resArray = $this->model->find(null, null, $this);

    $property_value_of = array();
    foreach ($resArray as $statement) {

      $subject = $statement->getSubject();

      $ontResource = new SwoopResource($subject->getUri());
      $ontResource->setAssociatedModel($this->model);
      $ontResource->setVocabulary($this->model->vocabulary);

      $prop = new SwoopOntology($this->model->createOntProperty($statement->getPredicate()->getUri()));

      $this->propertyValueOf[$prop->getShortName()][$ontResource->getLocalName()] = $ontResource; //->getLocalName();
      $property_value_of[$ontResource->getLocalName()] = $prop->getShortName();
      $this->addResourceTag($prop);
      $this->addResourceTag($ontResource);

      // See the comment "--rev + deps aware", a few lines bellow.
      if(!$drush_main || !$rdf_type || !in_array($rdf_type->getLocalName(), $grouping_types_rev)) {
        usort($this->propertyValueOf[$prop->getShortName()], function($a, $b){
          return strcasecmp($a->getLocalName(), $b->getLocalName());
        });
      }
    }

    if($drush_main && !$rdf_type) {
      drush_log(t('Missing rdf:type for @resource', array('@resource' => $this->getLocalName())), 'error');
    }

    // --rev + deps aware
    if(
        // On huge model this is time consuming feature, so to avoid timeout run
        // only with drush: drush cc-model files__rdf__rdfxp-config-auto
        $drush_main
        && $rdf_type
        // --rev only for grouping resources i.e.
        && in_array($rdf_type->getLocalName(), $grouping_types_rev)
      ) {

      $deps_aware = new DependencyAware($this->model);

      $localname = $this->getLocalName();
      $types = array($rdf_type->getLocalName() => array($localname => $localname));
      $prev = $deps_aware->get($types, TRUE);

      $appended = $this->pulledUpDependencies = $thisPropertyValueOf = array();
      $i = 0;
      // TODO: #major: document "appended" processing in this loop
      foreach ($deps_aware->getDependenciesAppended() as $deps_appended) {
        if($deps_appended['searched'] == $types) {
          foreach ($deps_appended['appended'] as $type => $appened_localnames) {

            if($type != $deps_appended['look_for']) {
              $type .= ':' . $deps_appended['look_for'];
            }

            foreach ($appened_localnames as $appened_localname => $appened_localname) {

              if(isset($property_value_of[$appened_localname])) {
                $prop_name = $property_value_of[$appened_localname];
                $this->propertyValueOf[$prop_name]['ns0:' . $type][] = $this->propertyValueOf[$prop_name][$appened_localname];
                unset($this->propertyValueOf[$prop_name][$appened_localname]);
              }
              else {
                $appended[$appened_localname] = $type; // . ($type != $deps_appended['look_for'] ? ':' . $deps_appended['look_for'] : '');

                $ontResource = new SwoopResource('ns0:' . $appened_localname);
                $ontResource->setAssociatedModel($this->model);
                $ontResource->setVocabulary($this->model->vocabulary);

                $types1 = explode(':', $type);
                // TODO: #major: why need to log if not set $type[1]
                $this->pulledUpDependencies['0 rev: searched for ' . $types1[1]]['ns0:' . $types1[0]][] = $ontResource;
                $this->addResourceTag($ontResource);
                $this->addResourceTag($types1[0]);
              }
            }
          }
        }
        else {
          foreach ($deps_appended as $key => $value) {
            if(!is_array($value)) {
              continue;
            }
            foreach ($value as $type => $appened_localnames) {
              foreach ($appened_localnames as $appened_localname => $appened_localname) {

                $ontResource = new SwoopResource('ns0:' . $appened_localname);
                $ontResource->setAssociatedModel($this->model);
                $ontResource->setVocabulary($this->model->vocabulary);

                if($key == 'searched') {
                  $last_searched_key = ($i+1) . ' rel: ' . $key . ' for ' . $deps_appended['look_for'];
                  $this->pulledUpDependencies[$last_searched_key]['ns0:' . $type][] = $ontResource;
                }
                else {
                  $this->pulledUpDependencies[$last_searched_key][$key]['ns0:' . $type][] = $ontResource;
                  $this->addResourceTag($ontResource);
                  $this->addResourceTag($type);
                }

              }
            }
          }
          $i++;
        }
      }
    }

    return $this->propertyValueOf;
  }

  public function objectAssertions() {
    return $this->objectAssertions;
  }

  public function datatypeAssertions() {
    return $this->datatypeAssertions;
  }

  /**
   *
   * @See SwoopOntology::defaults()
   * @TODO traits or assert code compare
   */
  public function defaults($prop = null) {

    if(!isset($this->defaults)) {
      $class = get_class($this);
      $this->defaults = array_intersect_key(get_class_vars($class), array_flip(get_class_methods($class)) + array('defaults' => true));
    }

    if(isset($prop) && array_key_exists($prop, $this->defaults)) {
      $this->{$prop} = $this->defaults[$prop];
    }

    return $this->defaults;
  }

  /**
   * Answer an array of the RDF classes to which this resource belongs.
   * If $direct is true, only answer those resources that are direct types of
   * this resource, not the super-classes of the class etc.
   * @override
   *
   * @param	boolean	$direct
   * @return	array Array of SwoopClass
   * @access	public
   */
  function listRDFTypes($direct = true)
  {
    return $this->listProperty($this->vocabulary->TYPE(), 'OntClass');
  }

  function likelyProperties() {

    $this->datatypeAssertions = array();
    $this->objectAssertions = array();
    $this->likelyProperties = array();

    foreach ($this->listRDFTypes() as $swoopClass) {
//       krumo($swoopClass);
//       $swoopClass = new SwoopOntologyClass($resResource);
      foreach ($swoopClass->domainOf() as $resResource) {
        $uri = $resResource->getUri();
        $swoopOnt = new SwoopOntology($resResource);
        $shortName = $swoopOnt->getShortName();
        $localName = $resResource->getLocalName();



        $this->likelyProperties[$shortName] = $resResource;// $localName;
        $prop = $resResource;// new SwoopOntologyProperty($this->model->createOntProperty($uri));
        $typeProp = '';

        foreach($prop->listRDFTypes() as $propType) {
          if(in_array($propType->getLocalName(), array('DatatypeProperty', 'ObjectProperty'))) {
            $typeProp = $propType->getLocalName();
            break;
          }
        }
//         krumo($this->likelyProperties);
        switch ($typeProp) {
        	case 'ObjectProperty' :
        	  $res_property = new ResProperty($uri);
        	  foreach ($this->listProperty($res_property, 'Individual') as $resProp) {
        	    $this->objectAssertions[$shortName][$resProp->getLocalName()] = $resProp;//->getLocalName();
              $property_value_of[$resProp->getLocalName()] = $swoopOnt->getShortName();

        	    if(isset($this->likelyProperties[$shortName])) {
        	      unset($this->likelyProperties[$shortName]);
        	    }
        	    $this->addResourceTag($resProp);
        	  }
        	  $this->addResourceTag($res_property);

        	  break;
      	  case 'DatatypeProperty' :
      	    $res_property = new ResProperty($uri);
      	    foreach ($this->listProperty($res_property) as $resProp) {

      	      if($resProp->getDatatype() == RDF_NAMESPACE_URI . RDF_XMLLITERAL) {
      	        $ontResource = new SwoopLiteralXml($resProp->getLabel());
      	      }
      	      else {
      	        $ontResource = new SwoopLiteral($resProp->getLabel());
      	      }
      	      $property_model = $prop->getAssociatedModel();
      	      $ontResource->setAssociatedModel($property_model);

      	      $this->datatypeAssertions[$shortName][] = $ontResource;
      	      if(isset($this->likelyProperties[$shortName])) {
      	        unset($this->likelyProperties[$shortName]);
      	      }
      	    }
      	    $this->addResourceTag($res_property);

      	    break;
        	default:{}
        }

//         krumo($this->objectAssertions, $this->datatypeAssertions);
      }

      $this->likelyProperties = array_values($this->likelyProperties);
    }

    if($drush_main && !$rdf_type) {
      drush_log(t('Missing rdf:type for @resource', array('@resource' => $this->getLocalName())), 'error');
    }

    // deps aware
    // TODO: #major: duplicate code: see self::propertyValueOf()
    $rdf_type = $this->getRDFType();
    $drush_only = \Drupal::config('rdfxp.settings')->get('filesystem_index.drush_only');
    $grouping_types_rev = \Drupal::config('rdfxp.settings')->get('grouping_types_rev');
    if(
        // On huge model this is time consuming feature, so to avoid timeout run
        // only with drush: drush cc-model files__rdf__rdfxp-config-auto
        (!$drush_only || $drush_only && function_exists('drush_main'))
        // TODO: #normal: For drush command "rdfxp-mxmi" there is a call to individual, without
        //   the need of deps aware. Find a better way
        && (!function_exists('drush_main') || function_exists('drush_main') && !in_array(reset(drush_get_arguments()), array('rdfxp-mxmi')))
        && $rdf_type
        && !in_array($rdf_type->getLocalName(), $grouping_types_rev)
      ) {

      $deps_aware = new DependencyAware($this->model);

      $localname = $this->getLocalName();
      $types = array($rdf_type->getLocalName() => array($localname => $localname));
      $prev = $deps_aware->get($types);

      $appended = $this->pulledUpDependencies = $thisPropertyValueOf = array();
      $i = 0;
      foreach ($deps_aware->getDependenciesAppended() as $deps_appended) {
        if($deps_appended['searched'] == $types) {
          foreach ($deps_appended['appended'] as $type => $appened_localnames) {

//             if($type != $deps_appended['look_for']) {
//               $type .= ':' . $deps_appended['look_for'];
//             }

            foreach ($appened_localnames as $appened_localname => $appened_localname) {

              if(isset($property_value_of[$appened_localname])) {
                $prop_name = $property_value_of[$appened_localname];
                $this->objectAssertions[$prop_name]['ns0:' . $type][] = $this->objectAssertions[$prop_name][$appened_localname];
                unset($this->objectAssertions[$prop_name][$appened_localname]);
              }
              else {
                $appended[$appened_localname] = $type; // . ($type != $deps_appended['look_for'] ? ':' . $deps_appended['look_for'] : '');

                $ontResource = new SwoopResource('ns0:' . $appened_localname);
                $ontResource->setAssociatedModel($this->model);
                $ontResource->setVocabulary($this->model->vocabulary);

                $types1 = explode(':', $type);
                // TODO: #major: why need to log if not set $type[1]
                $this->pulledUpDependencies['0 rel: searched for ' . $types1[1]]['ns0:' . $types1[0]][] = $ontResource;
                $this->addResourceTag($ontResource);
                $this->addResourceTag($types1[0]);
              }
            }
          }
        }
        else {
          foreach ($deps_appended as $key => $value) {
            if(!is_array($value)) {
              continue;
            }
            foreach ($value as $type => $appened_localnames) {
              foreach ($appened_localnames as $appened_localname => $appened_localname) {

                $ontResource = new SwoopResource('ns0:' . $appened_localname);
                $ontResource->setAssociatedModel($this->model);
                $ontResource->setVocabulary($this->model->vocabulary);

                if($key == 'searched') {
                  $last_searched_key = ($i+1) . ' rel: ' . $key . ' for ' . $deps_appended['look_for'];
                  $this->pulledUpDependencies[$last_searched_key]['ns0:' . $type][] = $ontResource;
                }
                else {
                  $this->pulledUpDependencies[$last_searched_key][$key]['ns0:' . $type][] = $ontResource;
                  $this->addResourceTag($ontResource);
                  $this->addResourceTag($type);
                }

              }
            }
          }
          $i++;
        }
      }
    }

    return $this->likelyProperties;
  }

  /**
   * @TODO V2 must go to SwoopOntology. V1 keep - see propertyValue
   * @return multitype:multitype:NULL
   */
  function listPropertyOf()
  {

    $return=array();
    $resArray = $this->model->find(null, null, $this);

    foreach ($resArray as $statement) {
      // 	    echo "\n" . $statement->getPredicate()->getLocalName();
      $return[]=array($statement->getSubject(), $statement->getPredicate());
    }

    return $return;
  }

  public function addResourceTag($resource) {
    $tag = 'rdfxp:' . $this->model->modelId() . ':resource:' . (is_object($resource) ? $resource->getLocalName() : $resource);
    $this->tags[$tag] = $tag;
  }
}