<?php
/**
 * Apply some patterns to elements using visitor pattern
 * @author alex
 *
 */

interface MethodsElement {
  public function accept(MethodsVisitor $visitor);
  public function hasVisitor($visitorName);
}

interface MethodsVisitor {
  /**
   * Retun "visit" methods
   */
  public function methods($classname);

  public function visit(MethodsElement $elt);
}

interface WalkableElement extends MethodsElement{
  /**
   * For registering
   * @param WalkVisitor $visitor
   */
  public function walker(WalkVisitor $visitor);

  /**
   * accept() and getIterator()
   */
  public function items();
}

interface WalkVisitor extends MethodsVisitor, RecursiveIterator{
  public function each($callback = null);
  public function reset();
  public function walkRecursive1($container = null);
  public function getIterator();
//   public function getIterator();
}

interface ProxyElement extends MethodsElement{
  /**
   * exposes GetterSetterReset of protected attr
   * Get defaults props and their values
   */
  public function defaults();

  /**
   * Register the visitor
   */
  public function proxy(ProxyVisitor $visitor);
}

interface ProxyVisitor extends MethodsVisitor{
  public function attr($propName, $value = NULL);
}

class MethodsVisitorConcrete {

  public function methods($className) {
    $methods = array();
    foreach (get_class_methods($className) as $method) {
      if(
      !in_array($method, array('methods', 'visit'))
      // drop magic methods
      && !preg_match('/^__[^_]+/', $method)
      ) {
        $methods[] = $method;
      }
    }

    return $methods;
  }

  public function visit(MethodsElement $elt) {
    $this->element = $elt;
  }
}

class IteratorVisitor extends MethodsVisitorConcrete implements WalkVisitor {

  protected $element = null;
  protected $methods = null;
  protected $items = null;
  protected $keys = null;
  protected $position = null;
  public $context = array();
  protected $recursionMarker;
  protected $dataKey;

  public function __construct() {

    $this->recursionMarker = uniqid('recursion');
    $this->dataKey = uniqid('data');
//     parent::__construct();
    $this->context = array(
      'currentPathItems' => array(),
//       'base' => '/toto/'
//       'base' => '/',
      //@TODO Can be the varname
      'base' => '/output',
//       'base' => '',
//       'base' => '/toto/tata/',
//       'base' => '/toto/tata',

       'hive' => array(),
    );

    // Not working for SwoopOntologyRel
    $this->context['base'] = preg_replace('%/+$%', '', $this->context['base']) . '/';

    $this->callback();

  }

  public function context() {
    $context = $this->context[preg_replace('%/+$%', '', $this->context['base'])];
    $context['#markup'] = $this->context['base'];

    $varname = str_replace('/', '__', preg_replace('%(^/|/$)%', '', $this->context['base']));
    if(empty($varname)) {
      $varname = '__';
    }

    foreach ($this->context['hive'] as $hive) {
      unset($hive->{$this->recursionMarker});
    }

    return array($varname => $context);
  }

//   public function methods() {
//     return array(
//       'rewind', 'key', 'current', 'next', 'valid',
//       'getChildren', 'hasChildren', 'walkRecursive'
//     );
//   }
  // Methods wich will be available in visited object
  public function methods($classname) {
    if(isset($this->methods)) {
      return $this->methods;
    }

    $this->methods = parent::methods(__CLASS__);

//     $this->methods = array_intersect($this->methods, array('getIterator', 'getChildren', 'hasChildren'));

    return $this->methods;
  }

  /**
   * reset by $this->items = NULL
   */
  public function reset() {
    $this->items = $this->keys = NULL;

    return $this;
  }

  protected function items($items) {
    $this->items = $items;
    $this->keys = array_keys($this->items);
    $this->position = 0;
  }

  /**
   * If container is null, do Aggregator get items to iterate over
   * @TODO replace container by items proxy pattern visitor
   */
  public function rewind() {
    if(!isset($this->items)) {
      /**
       * getIterator(this)
       */
      $this->items($this->element->getIterator());
    }

    $this->position = 0;
  }

  public function key() {
    return isset($this->keys[$this->position])? $this->keys[$this->position] : NULL;
  }

  public function current() {
    return $this->items[$this->key()];
  }

  public function next() {
    $this->position++;
  }

  /**
   * Do rewind at the end
   */
  public function valid() {

    $valid = isset($this->items) && $this->key() !== NULL;

    // Set to Null to produce Reset container + items() in next rewind
    // Finally no need to do a reset container value, just rewind the same value
    if(!$valid) {
//       $this->reset();
      $this->rewind();
    }
    return $valid;
  }

  /**
   * infered - elt has method items()
   * @TODO default will be (array)$this->element : To test __toArray() vs iterator methods rewind(), key(), ...
   */
  public function getIterator() {
    static $getIteratorCalls = 0;

    $getIteratorCalls++;

    $items = $this->element->items();

    if(!isset($items[$this->recursionMarker])) {
      $items[$this->recursionMarker] = array();
    }
    $firstMetPath = key($items[$this->recursionMarker]);

    if(isset($items[$this->recursionMarker][$firstMetPath]) && (int)$items[$this->recursionMarker][$firstMetPath] > 1) {
      return array($this->recursionMarker => $firstMetPath);
    }

    unset($items[$this->recursionMarker]);
    $this->element->items($items);

    return $items;
  }

  /**
   * Children is different for every key(), current() pair.
   * Compare to current() - is an object - having a property 'children'
   * the object - current() - has just key() - like attr key - and children are actually the items if any.
   */
  public function getChildren() {

    if(!$this->setContainer($this->current())) {
      return null;
    }

    $this->items($this->element->getIterator());

    return $this->items;
  }

  public function hasChildren() {

    return count($this->getChildren());
  }

  /**
   * Doesn't advance the internal pointer
   */
  public function eachOne($callback = null) {

    if(!isset($this->items)) {
      // get item to iterate over + rewind
      $this->rewind();
    }

    if($valid = !$this->valid()) {
      return NULL;
    }

    $key = $this->key();
    $current = $this->current();

    // Notify there is an each element
    if(is_callable($callback)) {
      call_user_func($callback, $current, $key);
    }

    return array($key => $current);
  }

  /**
   * Advance the internal pointer
   */
  public function each($callback = null) {
    $each = $this->eachOne($callback);

    $this->next();

    return $each;
  }

  /**
   * Register container: array or WalkableElement instance
   * Set items form getIterator
   */
  public function setContainer($container) {

    if(is_array($container)) {
      $container = new SwoopElement($container);
    }

    // @TODO commenting next lines, but previousely must handle reccursion by SwoopOntology::getShortName to prevent infinite loop
    // Currently ok because all properties are OntResource(orig) and not SwoopOntology(decored)
//     if($container instanceof OntResource) {
//       $container->recursionMarker++;
//       $container = new SwoopOntology($container);
//     }

    if(is_object($container) && !($container instanceof WalkableElement) && !($container instanceof OntResource)) {
      if(isset($container->{$this->recursionMarker})) {
        $firstMetPath = key($container->{$this->recursionMarker});
        $container->{$this->recursionMarker}[$firstMetPath]++;
      }
      else {
        $container->{$this->recursionMarker} = array($this->context['currentPath'] => 1);
      }
      $this->context['hive'][] = $container;

      $container = new SwoopElement(get_object_vars($container));

    }

    if($container instanceof WalkableElement) {
      $container->walker($this);


      return $container;
    }

    return false;
  }

  /**
   * @TODO template method (See also drupal point cut) - Will be in visitor.
   * @param Iterator $container Capability to walk on external object
   */
  public function walkRecursive1($container = null) {
    $context = &$this->context;

    if(isset($container)) {

      if(!$this->setContainer($container)) {
        return;
      }

      $this->items($this->element->getIterator());
    }
    // $this->element is set only after elt->accept() > visitor->visit()
    // so already registered
    else if(!isset($this->element)) {

      return;
    }

    $i = 0;
    while($each = $this->eachOne()) {
      $key = key($each);
      $item = current($each);

      $context['currentPathItems'][] = $key;

      $this->preCallback();

      $backupElement = $this->element;
      $backupItems = $this->items;
      $backupPosition = $this->position;

      if($this->hasChildren()) {
        $this->walkRecursive1();
      }
      else {
        $this->leafCallback($item, $key);
      }

      $this->element = $backupElement;
      $this->items($backupItems);
      $this->position = $backupPosition;

      $this->postCallback($item, $key);

      $poped = array_pop($context['currentPathItems']);
      if($poped != $key) {
        throw new Exception('Key: ' . $key . '; item:' . print_r($item, TRUE) . '; poped:' . $poped . 'current_path: ' . print_r($context['current_path_arr'], true));
      }

      $i++;

      $this->next();
    }
  }

  public function callback() {

    $this->context['currentPath'] = $this->context['base'] . implode('/', str_replace('/', '__', $this->context['currentPathItems']));
    $this->context['parentPath'] = substr($this->context['currentPath'], 0, (int)strrpos($this->context['currentPath'], '/'));

  }

  public function preCallback() {

    $this->callback();

  }

  public function postCallback($item, $key) {

    $this->callback();

    $context = &$this->context;

    if($parentPath = $this->context['parentPath']) {

      $return = $context[$context['currentPath']];//['#data'];
       // Some cleaning
      unset($context[$context['currentPath']]);

      // return to parent
      $context[$parentPath][$key] = $return; //$context[$context['currentPath']];
    }
  }

  public function leafCallback($item, $key) {

    $this->callback();
    $context = &$this->context;

    $context[$context['currentPath']] = $item;
  }
}

/**
 *
 * Inspired by krumo() for recursion detection
 */
class SwoopOntologyIterator extends IteratorVisitor {


  public function __construct() {
    parent::__construct();

    $this->dataKey = 'data';
  }

  public function preCallback() {
    $key = $this->key();
    $item = $this->current();

    $this->callback();

    $context = &$this->context;



    if($item instanceof SwoopOntologyComponent) {

      $namespaces = $item->getAssociatedModel()->model->parsedNamespaces;

      $url = new Drupal\Core\Url('rdfxp.resource',
        array('modelId' => $item->getAssociatedModel()->model->modelid, 'resource' => $item->getLocalName()),
        array('attributes' => array('resource' => $namespaces[$item->getNamespace()] . ':' . $item->getLocalName()))
      );

      $link = array(
          // also working
          //'#theme' => 'link_formatter_link_separate',
          //'#url_title' => $item->getLocalName(),
          '#type' => 'link',
          '#title' => $item->getLocalName(),
          // @TODO default is $item->getUri() if $item->getNamespace() in model_map then swoop/...
          '#url' => $url// 'swoop/' . $item->getAssociatedModel()->model->modelid . '/' . $item->getLocalName(),
//           '#options' => array('attributes' => array('resource' => $namespaces[$item->getNamespace()] . ':' . $item->getLocalName())),
      );
    }
//     else if($item instanceof SwoopLiteralXml) {
//         $renderable = $item->renderable();
//         $link = $renderable['data'];
//         $litteral_children = $renderable['children'];
//         $link = array(
//           '#theme' => 'item_list',
//           '#title' => 'XMLLiteral',
//           '#items' => $item->renderable(),
//         );
//         krumo($link);exit();
//         $link = render($link);
//     }
    else if($item instanceof SwoopOntologyLitteral) {
      $link = array(
          '#type' => 'markup',
          '#markup' => $item->renderable(),//$this->swoopRes->markup? $this->swoopRes->markup : (string)$this->swoopRes,
      );
    }else {

      if(is_array($item) && count($item)) {
        $current_item = current($item);

        if($current_item instanceof SwoopOntologyComponent
          || $current_item instanceof OntResource
            ) {


          $namespaces = $current_item->getAssociatedModel()->model->parsedNamespaces;

            // Key may be a short name if item is array of resources
          foreach ($namespaces as $namespace) {
            if(strpos($key, $namespace . ':') === 0) {
              $localName = substr($key, strlen($namespace . ':'));

              $url = new Drupal\Core\Url('rdfxp.resource',
                  array('modelId' => $current_item->getAssociatedModel()->model->modelid, 'resource' => $localName),
                  array('attributes' => array('resource' => $key))
              );

              $link = array(
                  '#type' => 'link',
                  '#title' => $key,//$localName,
                  // @TODO default is $item->getUri() if $item->getNamespace() in model_map then swoop/...
                  '#url' => $url,//'swoop/' . $current_item->getAssociatedModel()->model->modelid . '/' . $localName,
//                   '#options' => array('attributes' => array('resource' => $key)),
              );
            }
          }
        }
      }

      if(
          strpos($key, 'rel: searched for ') !== FALSE) {
        $key = '<div class="rel-searched-for">' . $key . '</div>';
      }

      if(!isset($link)) {
        $link = array(
            '#type' => 'markup',
            '#markup' => $key,//$this->swoopRes->markup? $this->swoopRes->markup : (string)$this->swoopRes,
        );
      }
    }

    $context[$context['currentPath']] = array($this->dataKey => $link);

  }

  /**
   * @TODO general recurse return. Then subclass for this case - #theme = item_list
   * @param unknown $item
   * @param unknown $key
   * @param unknown $context
   */
  public function postCallback($item, $key) {
    parent::postCallback($item, $key);

    $context = &$this->context;

    if($parentPath = $this->context['parentPath']) {

      $context[$parentPath]['children'][] = $context[$parentPath][$key];
      unset($context[$parentPath][$key]);
    }
  }

  public function leafCallback($item, $key) {
    $this->callback();
    $context = &$this->context;

    if(!($item instanceof SwoopOntologyElement)) {

      $link = array(
          '#type' => 'markup',
          '#markup' => $key . ' => ' . var_export($item, true),//$this->swoopRes->markup? $this->swoopRes->markup : (string)$this->swoopRes,
      );

      $context[$context['currentPath']] = array($this->dataKey => $link);
    }


  }
}

//class SwoopOntologyToArrayIterator extends SwoopOntologyIterator {}

class SwoopOntologyConditionsIterator extends IteratorVisitor {

  public function postCallback($item, $key) {
    parent::postCallback($item, $key);

    if($parentPath = $this->context['parentPath']) {

      $return = &$this->context[$parentPath][$key];

    // first time : all array elements ar conditions
      if($key === 'children' || $key === 'items') {
        $levelBool = NULL;
        $levelOutput = '(';
        foreach ($return as $element) {

          if($levelBool === NULL) {

            $levelOutput .= ' ' . $element['condition'];
            $levelBool = $element['condition'];
          }
          else {

            $levelBool = $levelBool || $element['condition'];
            $levelOutput .= ' || ' . $element['condition'];

          }
        }

//         $processedItem = $levelBool;
        $return = $levelOutput . ' )';

      }
      else if($key !== 'condition') {

//         $processedItem = array('condition' => $processedItem['condition']
//           && (!isset($processedItem['children']) || isset($processedItem['children']) && $processedItem['children']));

        if(isset($return['children'])) {

//           if(is_array($processedItem['condition'])) {
//             krumo($processedItem['condition']);
//             $elementCondition = '';
//             foreach ($processedItem['condition'] as $var => $val) {
//               $elementCondition .= $var . ' = <' . $val['uri'] . '>';
//             }
//             $processedItem['condition'] = $elementCondition;
//           }

          $return = array('condition' => '( ' . $return['condition'] . ' && ' . $return['children'] . ')');
        }
      }
    }
  }

  public function context() {
    $context = parent::context();

    $context = $context['output']['items'];

    return $context;
  }
}

class SwoopOntologyPropertyIterator extends SwoopOntologyIterator {
  /**
   * property Iteration - infered - elt has method properties()
   */
  public function getIterator() {
    if($this->element instanceof SwoopOntologyComponent) {
      $this->element->items($this->element->properties());
    }

    return parent::getIterator();
  }
}

class SwoopOntologyRelToXmiIterator extends IteratorVisitor {

  public function getChildren() {

      $key = $this->key();
      $item = $this->current();

      if(is_array($item) && $item['current'] instanceof SwoopOntologyComponent) {

      $keys = array_keys($item);
      // drop 'current', 'conditions' keys
      $keys = array_diff($keys, array('current', 'conditions'));

      foreach ($keys as $axe_step) {
//         $axe_step = $keys[1];
        $axe_step_arr = explode('::', $axe_step);
//         krumo($axe_step_arr);
        if(count($axe_step_arr) == 2) {
          $step = $axe_step_arr[1];
          $rel = $axe_step_arr[0];
        }
        else {
          $step = $axe_step_arr[0];
          $rel = 'rel';
        }
        if($step == '*') {
          $prop = null;
          $step = '?p';
        }
        else {
          list($prefix, $localName) = explode(':', $step);

          $namespace = array_search($prefix, $namespaces = $item['current']->getAssociatedModel()->model->parsedNamespaces);

          $prop = new ResProperty($namespace . $localName);
        }

        $statements = array();

        if($rel == 'rev') {
          if(isset($item['conditions'])) {
            $statements = $item['current']->getAssociatedModel()->rdqlFind(null, $prop, $item['current'], $item['conditions']);
          }
          else {
            $statements = $item['current']->getAssociatedModel()->find(null, $prop, $item['current']);
          }
        }
        else {
          if(isset($item['conditions'])) {
//             krumo($item['current'], $prop, null, $item['conditions']);
            $conditions = array(
              'items' => array(
                array(
                  'condition' => '?s = <' . $item['current']->getUri() . '>',
                  'children' => array(
                    array(
                      'condition' => '?p = <' . $prop->getUri() . '>',
                      'children' => $item['conditions']['items'],
                    ),
                  )
                ),
              ),
            );

            $statements = $item['current']->getAssociatedModel()->rdqlFind($item['current'], $prop, null, $item['conditions']);
          }
          else {
            $statements = $item['current']->getAssociatedModel()->find($item['current'], $prop, null);
          }
        }

        $results = array();
        foreach ($statements as $i => $statement)
        {
          if($rel == 'rev') {
            if(isset($item['conditions'])) {
              $ontResource = $statement[0];
            }
            else {
              $objectLabel=$statement->getLabelSubject();
            }
          }
          else {
            if(isset($item['conditions'])) {
              $ontResource = $statement[2];
            }
            else {
              $objectLabel=$statement->getLabelObject();
            }
          }

          if(!isset($item['conditions'])) {
            $ontResource = new SwoopResource($objectLabel);
            $ontResource->setAssociatedModel($item['current']->getAssociatedModel());
            $ontResource->setVocabulary($item['current']->getAssociatedModel()->vocabulary);
          }

          $results[$i] = array('current' => $ontResource);


          foreach ($item[$axe_step] as $key1 => $value) {
            $results[$i][$key1] = $value;
          }
        }

        if(count($results)) {
          $item[$axe_step] = $results;

        }
      }

//       krumo($key, $item);
    }

    //====== parent
    if(!$this->setContainer($item)) {
      return null;
    }

    $this->items($this->element->getIterator());

    return $this->items;
  }

  public function preCallback() {
    parent::preCallback();

    $key = $this->key();
    $item = $this->current();

    if(!(is_array($item) && $item['current'] instanceof SwoopOntologyComponent)) {
      return ;
    }

    extract(rdfxp_xmi_paths());

    // Type Interface
    $subPath = '' . $model_prefix . ':Interface/rev::rdf:type/' . $key;
    // ends with: strrpos faster than preg_match
    $isTypeInterface = strrpos($this->context['currentPath'], $subPath) + strlen($subPath) == strlen($this->context['currentPath']);
    // Type Class
    $subPath = '' . $model_prefix . ':Class/rev::rdf:type/' . $key;
    // ends with: strrpos faster than preg_match
    $isTypeClass = strrpos($this->context['currentPath'], $subPath) + strlen($subPath) == strlen($this->context['currentPath']);
    // Type Trait
    $subPath = '' . $model_prefix . ':Trait/rev::rdf:type/' . $key;
    // ends with: strrpos faster than preg_match
    $isTypeTrait = strrpos($this->context['currentPath'], $subPath) + strlen($subPath) == strlen($this->context['currentPath']);


    if($isTypeInterface) {
      $currentClassifierPath = $interfacePath . '/' . $key;
    }
    else if($isTypeClass || $isTypeTrait){
      $currentClassifierPath = $classPath . '/' . ($this->context[$classPath]++);
    }

    // Classifier
    if(isset($currentClassifierPath)) {

      $classifierName = $item['current']->getLocalName();

      $classifierDisplayName = implode(array_map('ucfirst', explode('-', $classifierName)));

      $this->context['struct'][$currentClassifierPath . '/@name'] = $classifierDisplayName;
      $this->context['struct'][$currentClassifierPath . '/@xmi.id'] = $uuid_like_prefix . $classifierDisplayName;

      $this->context['currentClassifierPath'] = $currentClassifierPath;

    }

    // Extends
    $subPath = '/' . $model_prefix . ':extends/' . $key;
    $extends = strrpos($this->context['currentPath'], $subPath) + strlen($subPath) == strlen($this->context['currentPath']);
    $subPath = '/rev::' . $model_prefix . ':extends/' . $key;
    $revExtends = strrpos($this->context['currentPath'], $subPath) + strlen($subPath) == strlen($this->context['currentPath']);
    // ends with: strrpos faster than preg_match
    if($extends || $revExtends) {

      $currentClassifierPath = $this->context['currentClassifierPath'];
      $classifierId =  $this->context['struct'][$currentClassifierPath . '/@xmi.id'];
      $classifierDisplayName =  $this->context['struct'][$currentClassifierPath . '/@name'];

      $extendsName = $item['current']->getLocalName();
      $extendsDisplayName = implode(array_map('ucfirst', explode('-', $extendsName)));
      $extendsId = $uuid_like_prefix . $extendsDisplayName;

      // swap
      if($revExtends) {
        $tmp = $classifierDisplayName;
        $classifierDisplayName = $extendsDisplayName;
        $extendsDisplayName = $tmp;

        $tmp = $extendsId;
        $extendsId = $classifierId;
        $classifierId = $tmp;
      }

      $already = array_search($uuid_like_prefix . $classifierDisplayName . ':gen:' . $extendsDisplayName, $this->context['struct']) !== FALSE;

      if(!$already) {
//       $walker->context[$generalizationPath]
        $this->context['struct'][($theGeneralizationPath = $generalizationPath . '/' . ($this->context[$generalizationPath]++)) . '/@xmi.id'] = $uuid_like_prefix . $classifierDisplayName . ':gen:' . $extendsDisplayName;
        $this->context['struct'][$theGeneralizationPath . '/UML:Generalization.child/UML:Class/@xmi.idref'] = $classifierId;
        $this->context['struct'][$theGeneralizationPath . '/UML:Generalization.parent/UML:Class/@xmi.idref'] = $extendsId;
      }
//       krumo($this->context['struct']);exit;
    }



    // Realize, use
    $subPath = '/' . $model_prefix . ':realize/' . $key;
    $isRealize = strrpos($this->context['currentPath'], $subPath) + strlen($subPath) == strlen($this->context['currentPath']);
    $subPath = '/rev::' . $model_prefix . ':realize/' . $key;
    $isRealizeRev = strrpos($this->context['currentPath'], $subPath) + strlen($subPath) == strlen($this->context['currentPath']);
    $subPath = '/' . $model_prefix . ':use/' . $key;
    $isUse = strrpos($this->context['currentPath'], $subPath) + strlen($subPath) == strlen($this->context['currentPath']);
    $subPath = '/rev::' . $model_prefix . ':use/' . $key;
    $isUseRev = strrpos($this->context['currentPath'], $subPath) + strlen($subPath) == strlen($this->context['currentPath']);
    // ends with: strrpos faster than preg_match
    if($isRealize || $isUse || $isRealizeRev || $isUseRev) {
    //if($isRealize || $isUse) {

      $currentClassifierPath = $this->context['currentClassifierPath'];
      $classifierId =  $this->context['struct'][$currentClassifierPath . '/@xmi.id'];
      $classifierDisplayName =  $this->context['struct'][$currentClassifierPath . '/@name'];

      $extendsName = $item['current']->getLocalName();
      $extendsDisplayName = implode(array_map('ucfirst', explode('-', $extendsName)));
      $extendsId = $uuid_like_prefix . $extendsDisplayName;

      // swap
      if($isRealizeRev || $isUseRev) {
        $tmp = $classifierDisplayName;
        $classifierDisplayName = $extendsDisplayName;
        $extendsDisplayName = $tmp;

        $tmp = $extendsId;
        $extendsId = $classifierId;
        $classifierId = $tmp;
      }

      $already = array_search($uuid_like_prefix . $classifierDisplayName . ':gen:' . $extendsDisplayName, $this->context['struct']) !== FALSE;

      if(!$already) {
        $this->context['struct'][($theGeneralizationPath = $abstractionPath . '/' . ($this->context[$abstractionPath]++)) . '/@xmi.id'] = $uuid_like_prefix . $classifierDisplayName . ':gen:' . $extendsDisplayName;
        $this->context['struct'][$theGeneralizationPath . '/UML:Dependency.client/UML:Class/@xmi.idref'] = $classifierId;
        if($isRealize) {
          $this->context['struct'][$theGeneralizationPath . '/UML:Dependency.supplier/UML:Interface/@xmi.idref'] = $extendsId;
          $this->context['struct'][$theGeneralizationPath . '/UML:ModelElement.stereotype/UML:Stereotype/0/@xmi.idref'] = $uuid_like_prefix . 'realize1';
        }
        else {
          $this->context['struct'][$theGeneralizationPath . '/UML:Dependency.supplier/UML:Class/@xmi.idref'] = $extendsId;
          $this->context['struct'][$theGeneralizationPath . '/UML:ModelElement.stereotype/UML:Stereotype/0/@xmi.idref'] = $uuid_like_prefix . 'use1';
        }
      }
//       krumo($this->context['struct']);exit;
    }

    // Method
    $subPath = '' . $model_prefix . ':methods/' . $key;
    // ends with: strrpos faster than preg_match
    if(strrpos($this->context['currentPath'], $subPath) + strlen($subPath) == strlen($this->context['currentPath'])) {

      $currentClassifierPath = $this->context['currentClassifierPath'];
      $classifierDisplayName =  $this->context['struct'][$currentClassifierPath . '/@name'];

      $methodDisplayName = implode(array_map('ucfirst', explode('-', $methodName = $item['current']->getLocalName())));
      $methodDisplayName = lcfirst(preg_replace('/^' . $classifierDisplayName . '/', '', $methodDisplayName));


      $this->context['struct'][($methodPath = $currentClassifierPath . '/' . $operationSubath . '/' . $key) . '/@name'] = $methodDisplayName;
      $this->context['struct'][$methodPath . '/@xmi.id'] = $uuid_like_prefix . $methodName;

      $this->context['currentMethodPath'] = $methodPath;
    }

    // Return
    $subPath = '' . $model_prefix . ':return/' . $key;
    // ends with: strrpos faster than preg_match
    if(strrpos($this->context['currentPath'], $subPath) + strlen($subPath) == strlen($this->context['currentPath'])) {

      $methodPath = $this->context['currentMethodPath'];
      $methodDisplayName =  $this->context['struct'][$methodPath . '/@name'];

      $this->context['struct'][($returnPath = $methodPath . '/' . $parameterSubath . '/' . $key) . '/@name'] = 'return';//$return
      $this->context['struct'][$returnPath . '/@xmi.id'] = $uuid_like_prefix . $methodDisplayName . ':return';
      if(in_array($item['current']->getLocalName(), $this->context['primitives'])) {
        $this->context['struct'][$returnPath . '/UML:Parameter.type/UML:DataType/@xmi.idref'] = $uuid_like_prefix . $item['current']->getLocalName();
      }
      else {
        $this->context['struct'][$returnPath . '/UML:Parameter.type/UML:Class/@xmi.idref'] = $uuid_like_prefix . $item['current']->getLocalName();
        $this->context['struct'][$returnPath . '/UML:Parameter.type/UML:Interface/@xmi.idref'] = $uuid_like_prefix . $item['current']->getLocalName();
      }
    }

    // References
    $subPath = '' . $model_prefix . ':reference/' . $key;
    // ends with: strrpos faster than preg_match
    if(strrpos($this->context['currentPath'], $subPath) + strlen($subPath) == strlen($this->context['currentPath'])) {
      $currentClassifierPath = $this->context['currentClassifierPath'];
      $classifierId =  $this->context['struct'][$currentClassifierPath . '/@xmi.id'];
      $classifierDisplayName =  $this->context['struct'][$currentClassifierPath . '/@name'];

      $extendsName = $item['current']->getLocalName();
      $extendsDisplayName = implode(array_map('ucfirst', explode('-', $extendsName)));

      $currentAssociationPath = $associationPath . '/' . ($this->context[$associationPath]++);
      $this->context['currentAssociationPath'] = $currentAssociationPath;
      $refPath = $currentAssociationPath . '/UML:Association.connection/UML:AssociationEnd/0';
      $attrPath = $currentAssociationPath . '/UML:Association.connection/UML:AssociationEnd/1';

      $this->context['struct'][$refPath . '/@xmi.id'] = $uuid_like_prefix . $classifierDisplayName . ':ref:' . $extendsDisplayName;
      $this->context['struct'][$refPath . '/@name'] = '  ' . lcfirst(preg_replace('/^' . $classifierDisplayName . '/', '', $extendsDisplayName));

      $this->context['struct'][$attrPath . '/@xmi.id'] = $uuid_like_prefix . $classifierDisplayName . ':attr:' . $extendsDisplayName;
      $this->context['struct'][$attrPath . '/UML:AssociationEnd.participant/UML:Class/@xmi.idref'] = $classifierId;

    }

    // Attributes
    $subPath = '' . $model_prefix . ':attribute/' . $key;
    // ends with: strrpos faster than preg_match
    if(strrpos($this->context['currentPath'], $subPath) + strlen($subPath) == strlen($this->context['currentPath'])) {

      $currentClassifierPath = $this->context['currentClassifierPath'];
      $classifierDisplayName =  $this->context['struct'][$currentClassifierPath . '/@name'];

      $methodDisplayName = implode(array_map('ucfirst', explode('-', $methodName = $item['current']->getLocalName())));
      $methodDisplayName = lcfirst(preg_replace('/^' . $classifierDisplayName . '/', '', $methodDisplayName));

      $this->context['struct'][($methodPath = $currentClassifierPath . '/' . $attributeSubath . '/' . $key) . '/@name'] = $methodDisplayName;
      $this->context['struct'][$methodPath . '/@xmi.id'] = $uuid_like_prefix . $methodName;

    }
  }

  public function postCallback($item, $key) {
    parent::postCallback($item, $key);

    $key = $this->key();
    $item = $this->current();

    if(!(is_array($item) && $item['current'] instanceof SwoopOntologyComponent)) {
      return ;
    }

    extract(rdfxp_xmi_paths());

    // Additional infos for references
    $subPath = $model_prefix . ':reference/' . $key;
    // ends with: strrpos faster than preg_match
    if(strrpos($this->context['currentPath'], $subPath) + strlen($subPath) == strlen($this->context['currentPath'])) {
      $currentAssociationPath = $this->context['currentAssociationPath'];
      $refPath = $currentAssociationPath . '/UML:Association.connection/UML:AssociationEnd/0';
      $attrPath = $currentAssociationPath . '/UML:Association.connection/UML:AssociationEnd/1';
      $refPaths = $attrPaths = array();
      $refPaths[] = $refPath;
      $attrPaths[] = $attrPath;

      if(($pos = strrpos($this->context['currentPath'], '/')) !== false) {

        $parentPath = substr($this->context['currentPath'], 0, $pos);
        $processedItem = $this->context[$parentPath][$key];
        // reference type
        foreach ($processedItem[$model_prefix . ':data-type'] as $dataType) {
          $dataTypeName = $dataType['current']->getLocalName();
          $dataTypeDisplayName = implode(array_map('ucfirst', explode('-', $dataTypeName)));
          // Additional $refPath for every entry of multivalued data-type
          if(isset($this->context['struct'][$refPath . '/UML:AssociationEnd.participant/UML:Class/@xmi.idref'])) {

            $currentAssociationPath1 = $associationPath . '/' . ($this->context[$associationPath]++);
            $refPath1 = $currentAssociationPath1 . '/UML:Association.connection/UML:AssociationEnd/0';
            $attrPath1 = $currentAssociationPath1 . '/UML:Association.connection/UML:AssociationEnd/1';
            $refPaths[] = $refPath1;
            $attrPaths[] = $attrPath1;

            $this->context['struct'][$refPath1 . '/@name'] = $this->context['struct'][$refPath . '/@name'];
            $this->context['struct'][$refPath1 . '/@xmi.id'] = $this->context['struct'][$refPath . '/@xmi.id'];
            $this->context['struct'][$refPath1 . '/UML:AssociationEnd.participant/UML:Class/@xmi.idref'] = $uuid_like_prefix . $dataTypeDisplayName;

            $this->context['struct'][$attrPath1 . '/@xmi.id'] = $this->context['struct'][$attrPath . '/@xmi.id'];
            $this->context['struct'][$attrPath1 . '/UML:AssociationEnd.participant/UML:Class/@xmi.idref'] = $this->context['struct'][$attrPath . '/UML:AssociationEnd.participant/UML:Class/@xmi.idref'];

          }
          else {
            $this->context['struct'][$refPath . '/UML:AssociationEnd.participant/UML:Class/@xmi.idref'] = $uuid_like_prefix . $dataTypeDisplayName;
          }
        }

        // Drop entity data-type if there is at least one bundle data-type
        if(count($refPaths) > 1) {
          foreach ($refPaths as $i => $refPath) {
            if(stripos($this->context['struct'][$refPath . '/UML:AssociationEnd.participant/UML:Class/@xmi.idref'], 'entity.') === 0) {
              unset($refPaths[$i]);
              unset($this->context['struct'][$refPath . '/@name']);
              unset($this->context['struct'][$refPath . '/@xmi.id']);
              unset($this->context['struct'][$refPath . '/UML:AssociationEnd.participant/UML:Class/@xmi.idref']);

              $attrPath = $attrPaths[$i];
              unset($attrPaths[$i]);
              unset($this->context['struct'][$attrPath . '/@xmi.id']);
              unset($this->context['struct'][$attrPath . '/UML:AssociationEnd.participant/UML:Class/@xmi.idref']);
            }
          }
        }

        // aggregation type if any on $attrPath
        foreach (array('aggregation') as $refProp) {
          foreach ($processedItem['' . $model_prefix . ':' . $refProp] as $aggregation) {
            foreach ($attrPaths as $attrPath) {
              $this->context['struct'][$attrPath . '/@' . $refProp] = $aggregation['current']->getLocalName();
            }
          }
        }
        // other infos on $refPath. ordering, isNavigable
        foreach (array('visibility') as $refProp) {
          foreach ($processedItem['' . $model_prefix . ':' . $refProp] as $aggregation) {
            foreach ($refPaths as $refPath) {
              $this->context['struct'][$refPath . '/@' . $refProp] = $aggregation['current']->getLocalName();
            }
          }
        }

        // Label overrides Local Name
        $refProp = 'rdfs:label';
        if(isset($processedItem[$refProp])) {
          foreach ($processedItem[$refProp] as $aggregation) {
            foreach ($refPaths as $refPath) {
              $this->context['struct'][$refPath . '/@name'] = '  ' . $aggregation['current']->getLocalName();
            }
          }
        }

        // Required
        $isRequired = FALSE;
        $refProp = $model_prefix . ':required-association-end';
        if(isset($processedItem[$refProp])) {
          foreach ($processedItem[$refProp] as $aggregation) {
            $value = $aggregation['current']->getLocalName();
            if($value && $value != 'false') {
              foreach ($refPaths as $refPath) {
                $this->context['struct'][$refPath . '/@name'] .= ' *';
              }
              $isRequired = TRUE;
            }
          }
        }

        // Type
        $refProp = 'rdf:type';
        $mapping = rdfxp_graphviz_field_type_mapping();
        $isStorage = FALSE;
        $isField = FALSE;
        $isImage = FALSE;
        if(isset($processedItem[$refProp])) {
          foreach ($processedItem[$refProp] as $aggregation) {
            if(!in_array($aggregation['current']->getLocalName(), array('FieldStorage', 'FieldField', 'FieldBase'))) {
              if($aggregation['current']->getLocalName() == 'ImageReference') {
                $isImage = TRUE;
              }
              foreach ($refPaths as $refPath) {
                $this->context['struct'][$refPath . '/@name'] .= ' : ' . str_replace(array_keys($mapping), $mapping, $aggregation['current']->getLocalName());
              }
            }

            if($aggregation['current']->getLocalName() == 'FieldStorage') {
              $isStorage = TRUE;
            }
            else if($aggregation['current']->getLocalName() == 'FieldField') {
              $isField = TRUE;
            }
          }
        }

        // Field name
        $refProp = $model_prefix . ':id';
        $field_name = '';
        if(isset($processedItem[$refProp])) {
          foreach ($processedItem[$refProp] as $aggregation) {
            if($isStorage) {
              // For info: we already have the entity info by: ./(rev::reference|rev::attribute)
              list($entity, $field_name) = explode('.', $aggregation['current']->getLocalName());
            }
            else if($isField) {
              // For info: we already have the bundle and entity info by: ./(rev::reference|rev::attribute)/extends
              list($entity, $bundle, $field_name) = explode('.', $aggregation['current']->getLocalName());
            }
          }
        }
        if($field_name) {
          foreach ($refPaths as $refPath) {
            $this->context['struct'][$refPath . '/@name'] .= '\l  (' . $field_name;
          }
        }

        // Translatable
        $refProp = 'translatable';
        $translatable = FALSE;
        foreach ($processedItem['' . $model_prefix . ':' . $refProp] as $aggregation) {
          foreach ($refPaths as $refPath) {
            $value = $aggregation['current']->getLocalName();
            if($value && $value != 'false') {
              $this->context['struct'][$refPath . '/@name'] .= ', trans';
              $translatable = TRUE;
            }
            else {
              $this->context['struct'][$refPath . '/@name'] .= ', und';
            }
          }
        }


        $refProp = 'image_settings';
        $alt_field = $alt_required = $alt_trans = NULL;
        if(isset($processedItem['' . $model_prefix . ':' . $refProp])) {
          foreach ($processedItem['' . $model_prefix . ':' . $refProp] as $image_alt_settings) {
            $xmlElement = simplexml_load_string($image_alt_settings['current']->uri);
            $pattern = '/(^|\s+)([^\s]+)::__set_state\(array\(/';
            $replace = '\1(array(\'object\' => \'\2\','; //(object)
            eval('$settings = ' . preg_replace($pattern, $replace, var_export($xmlElement, true)) . ';');

            // When empty <alt_field></alt_field> then 'alt_field' => array('object' => 'SimpleXMLElement')
            $alt_field = isset($settings['alt_field']) && !is_array($settings['alt_field']) && $settings['alt_field'] && $settings['alt_field'] != 'false';
            $alt_required = isset($settings['alt_field_required']) && !is_array($settings['alt_field_required']) && $settings['alt_field_required'] && $settings['alt_field_required'] != 'false';
            $alt_trans = isset($settings['alt']) && !is_array($settings['alt']) && $settings['alt'] == 'alt';

            $title_field = isset($settings['title_field']) && !is_array($settings['title_field']) && $settings['title_field'] && $settings['title_field'] != 'false';
            $title_required = isset($settings['title_field_required']) && !is_array($settings['title_field_required']) && $settings['title_field_required'] && $settings['title_field_required'] != 'false';
            $title_trans = isset($settings['title']) && !is_array($settings['title']) && $settings['title'] == 'title';

            $file_trans = isset($settings['file']) && !is_array($settings['file']) && $settings['file'] == 'file';
          }
        }


        if($field_name) {
          foreach ($refPaths as $refPath) {
            // "field_image trans, alt* [, file und]"
            if($isImage) {
              $this->context['struct'][$refPath . '/@name'] .=
                ($alt_field? ', alt' : '')
                . ($alt_required? '*' : '')
                . ($translatable && $alt_field ? (!$alt_trans? ' und' : '') : '')
                . ($title_field? ', title' : '')
                . ($title_required? '*' : '')
                . ($translatable && $title_field ? (!$title_trans? ' und' : '') : '')
                . ($translatable ? (!$file_trans? ', file und' : '') : '');
            }
            $this->context['struct'][$refPath . '/@name'] .= ')';
          }
        }

        // Cardinality
        $refProp = $model_prefix . ':cardinality-association-end';
        if(isset($processedItem[$refProp])) {
          foreach ($refPaths as $refPath) {
            $multiPath = $refPath . '/UML:AssociationEnd.multiplicity/UML:Multiplicity';
            $this->context['struct'][($multiPath = $refPath . '/UML:AssociationEnd.multiplicity/UML:Multiplicity') . '/@xmi.id'] = $uuid_like_prefix . $classifierDisplayName . ':ref:range:' . $extendsDisplayName;
            $this->context['struct'][$multiPath . '/UML:Multiplicity.range/UML:MultiplicityRange/@xmi.id'] = $this->context['struct'][$multiPath . '/@xmi.id'] . ':range';

            // TODO: #normal: To verify: if !$isRequired => multiplicity @lower is 0
            if(!$isRequired) {
              $this->context['struct'][$multiPath . '/UML:Multiplicity.range/UML:MultiplicityRange/@lower'] = '0';
            }
            foreach ($processedItem[$refProp] as $aggregation) {
              $this->context['struct'][$multiPath . '/UML:Multiplicity.range/UML:MultiplicityRange/@upper'] = $aggregation['current']->getLocalName();
            }
          }
        }
        // For bundle diagram, cardinality comes from "fieldStorage" property
        else {
          $refProp = 'fieldStorage';
          if(isset($processedItem['' . $model_prefix . ':' . $refProp])) {
            foreach ($processedItem['' . $model_prefix . ':' . $refProp] as $field_storage) {
              $refProp1 = 'cardinality-association-end';
              if(isset($field_storage['' . $model_prefix . ':' . $refProp1])) {
                foreach ($refPaths as $refPath) {

                  $multiPath = $refPath . '/UML:AssociationEnd.multiplicity/UML:Multiplicity';
                  //var_dump($multiPath);
                  $this->context['struct'][$multiPath . '/@xmi.id'] = $uuid_like_prefix . $classifierDisplayName . ':ref:range:' . $extendsDisplayName;
                  $this->context['struct'][$multiPath . '/UML:Multiplicity.range/UML:MultiplicityRange/@xmi.id'] = $this->context['struct'][$multiPath . '/@xmi.id'] . ':range';

                  // TODO: #normal: To verify: if !$isRequired => multiplicity @lower is 0
                  if(!$isRequired) {
                    $this->context['struct'][$multiPath . '/UML:Multiplicity.range/UML:MultiplicityRange/@lower'] = '0';
                    // TODO: #normal: Find out why next line causes $model_prefix . ':extends' to bug
                    // $this->context['struct'][$multiPath . '/UML:Multiplicity.range/UML:MultiplicityRange/@lower'] = 0;
                  }

                  foreach ($field_storage['' . $model_prefix . ':' . $refProp1] as $cardinality) {
                    $this->context['struct'][$multiPath . '/UML:Multiplicity.range/UML:MultiplicityRange/@upper'] = $cardinality['current']->getLocalName();
                  }
                }
              }
            }
          }
        }
      }
      unset($this->context['currentAssociationPath']);
    }

    // Attributes
    // TODO: #major: This is the same portion of code than for #reference, so need refactoring.
    $subPath = '' . $model_prefix . ':attribute/' . $key;
    // ends with: strrpos faster than preg_match
    if(strrpos($this->context['currentPath'], $subPath) + strlen($subPath) == strlen($this->context['currentPath'])) {

      $currentClassifierPath = $this->context['currentClassifierPath'];
      $attrPath = $currentClassifierPath . '/' . $attributeSubath . '/' . $key;

      if(($pos = strrpos($this->context['currentPath'], '/')) !== false) {

        $parentPath = substr($this->context['currentPath'], 0, $pos);
        $processedItem = $this->context[$parentPath][$key];

        // Label overrides Local Name
        $refProp = 'rdfs:label';
        if(isset($processedItem[$refProp])) {
          foreach ($processedItem[$refProp] as $aggregation) {
            $this->context['struct'][$attrPath . '/@name'] = '  ' . $aggregation['current']->getLocalName();
          }
        }

        // Required
        $isRequired = FALSE;
        $refProp = $model_prefix . ':required-association-end';
        if(isset($processedItem[$refProp])) {
          foreach ($processedItem[$refProp] as $aggregation) {
            $value = $aggregation['current']->getLocalName();
            if($value && $value != 'false') {
              $this->context['struct'][$attrPath . '/@name'] .= ' *';
              $isRequired = TRUE;
            }
          }
        }

        // Type
        $refProp = 'rdf:type';
        $mapping = rdfxp_graphviz_field_type_mapping();
        $isStorage = FALSE;
        $isField = FALSE;
        if(isset($processedItem[$refProp])) {
          foreach ($processedItem[$refProp] as $aggregation) {
            if(!in_array($aggregation['current']->getLocalName(), array('FieldStorage', 'FieldField' , 'FieldBase'))) {
              $this->context['struct'][$attrPath . '/@name'] .= ' : ' . str_replace(array_keys($mapping), $mapping, $aggregation['current']->getLocalName());
            }

            if($aggregation['current']->getLocalName() == 'FieldStorage') {
              $isStorage = TRUE;
            }
            else if($aggregation['current']->getLocalName() == 'FieldField') {
              $isField = TRUE;
            }
          }
        }

        // Field name
        $refProp = $model_prefix . ':id';
        $field_name = '';
        if(isset($processedItem[$refProp])) {
          foreach ($processedItem[$refProp] as $aggregation) {
            if($isStorage) {
              // For info: we already have the entity info by: ./(rev::reference|rev::attribute)
              list($entity, $field_name) = explode('.', $aggregation['current']->getLocalName());
            }
            else if($isField) {
              // For info: we already have the bundle and entity info by: ./(rev::reference|rev::attribute)/extends
              list($entity, $bundle, $field_name) = explode('.', $aggregation['current']->getLocalName());
            }
          }
        }
        if($field_name) {
          $this->context['struct'][$attrPath . '/@name'] .= '\l  (' . $field_name;
        }

        $refProp = 'translatable';
        foreach ($processedItem['' . $model_prefix . ':' . $refProp] as $aggregation) {
          $value = $aggregation['current']->getLocalName();
          $this->context['struct'][$attrPath . '/@name'] .= $value && $value != 'false'? ', trans' : ', und';
        }

        if($field_name) {
          $this->context['struct'][$attrPath . '/@name'] .= ')';
        }
      }
    }
  }
}

class SwoopOntologyPropertyIframeIterator extends SwoopOntologyPropertyIterator {
  public function preCallback() {
    parent::preCallback();

    $context = &$this->context;

    $context[$context['currentPath']]['data']['#options']['attributes']['target'] = 'swoop';
    $context[$context['currentPath']]['data']['#options']['query'] = array('iframe' => 1);
  }
}

abstract class SwoopOntologySamePropertyIterator extends SwoopOntologyIterator {
  /**
   *
   */
  public function getIterator() {

    if($this->element instanceof SwoopOntologyComponent) {
      $this->element->items(array($this->element));
    }

    return parent::getIterator();
  }
}

class SwoopOntologySubClassesIterator extends SwoopOntologySamePropertyIterator {

  public function getChildren() {

    $current = $this->current();
    if($current instanceof OntResource) {
      $current = new SwoopOntology($current);
    }

    if(!$this->setContainer($current->resource->listSubClasses())) {
      return null;
    }

    $this->items($this->element->getIterator());

    return $this->items;
  }
}

class SwoopOntologySubPropertiesIterator extends SwoopOntologySamePropertyIterator {
  public function getChildren() {

    $current = $this->current();
    if($current instanceof OntResource) {
      $current = new SwoopOntology($current);
    }

    //@TODO $current->resource->subProperties() doesn't work > need proxy visitor
    // Maybe subProperties($values) must be private and subProperties() public
    if(!$this->setContainer($current->resource->listSubProperties())) {
      return null;
    }

    $this->items($this->element->getIterator());

    return $this->items;
  }
}

class SwoopOntologySubClassesIframeIterator extends SwoopOntologySubClassesIterator {
  public function preCallback() {
    parent::preCallback();

    $context = &$this->context;

    $context[$context['currentPath']][$this->dataKey]['#options']['attributes']['target'] = 'swoop';
    $context[$context['currentPath']][$this->dataKey]['#options']['query'] = array('iframe' => 1);
  }
}

class SwoopOntologySubPropertiesIframeIterator extends SwoopOntologySubPropertiesIterator {
  public function preCallback() {
    parent::preCallback();

    $context = &$this->context;

    $context[$context['currentPath']][$this->dataKey]['#options']['attributes']['target'] = 'swoop';
    $context[$context['currentPath']][$this->dataKey]['#options']['query'] = array('iframe' => 1);
  }
}

/**
 *
 * See also rdfxp_model_explore
 * When recursion is detected put the currentPath for the original (first met value).
 * Then on js-open > js-replace with 'first met value'
 */
class KrumoLikeIterator extends SwoopOntologyIterator {

}


class ArrayToSimpleXml extends SimpleXMLElement{

  public static function fromPhpCode($configPhpCode, $root_element = 'root', $config_varname = 'config') {

    $config = preg_replace('/array \(\s+\),/', '\'array()\',', $configPhpCode);
    // false
    $config = preg_replace('/false,/', '\'false\',', $config);
    // start
    $config = preg_replace('/array \(/', 'ArrayToSimpleXml::__set_state(array(', $config);
    // end
    $config = preg_replace('/(\s+)(\),)(\R)/', '\\1)\\2\\3', $config);
    // in drupal context : replace stdClass, panels_display, ...
    $config = preg_replace('/(stdClass|panels_display)::__set_state/', 'ConfigToSimpleXml::__set_state', $config);
    //   echo "\n\$config: " . $config;
    //   $eval_str = '$config = ' . $config . ');';

    $eval_str = $config;
    $eval_str = preg_replace('/^<\?php/m', '', $eval_str);
    $eval_str = preg_replace('/\);$/m', '));', $eval_str);
//     krumo($eval_str);
    eval ($eval_str);

    $configXml = (isset($root_element)? '<' . $root_element . '>' : '') . ${$config_varname} . (isset($root_element)? '</' . $root_element . '>' : '');

    return $configXml;
  }

  public static function __set_state($array) {
    $output = '';
    $numkey = $hasAttr = false;
    $tags = array();
    if(count($array)) {
      foreach ($array as $key => $value) {
        $key = str_replace(' ', '___', $key);

        // views_view
        $key = str_replace('***', '____', $key);
        // menu
        $key = str_replace('%', '_____', $key);

        // Array with numeric indexes.
        if(is_numeric($key)) {
//           $key = '_' . $key;
          $numkey = true;
        }

        // attr and regular item can be together
        if($key[0] == '@' || $key[0] == '.') {
//           $key = '_' . $key;
          $hasAttr = true;
        }
        // store all regular
        else {
          $tags[] = $key;
        }

        // Prefix, Suffix can have open html tags.
        // body for panes. other possible body key?
        if(in_array($key, array('prefix', 'suffix', 'body'))) {
          $value = '<![CDATA[' . $value . ']]>';
        }

        // b/., b/@name
        $attrs = '';
        if(is_array($value)) {

          $tmpkey = key($value);
          if($tmpkey[0] == '@' || $tmpkey[0] == '.') {
            $textvalue = $value['.'];
            unset($value['.']);

            foreach ($value as $name => $attr) {
              $attrs .= ' ' . substr($name, 1) . '="' . $attr . '"';
            }

            $value = $textvalue;
          }
        }

        $values = !is_array($value)? array($value) : $value;

        $mixed = false;
        foreach ($values as $value) {

          // d/0/., d/0/@name
          // e/0/., e/0/@name
          if(is_array($value)) {
            $attrs = '';
            $tmpkey = key($value);
            if($tmpkey[0] == '@' || $tmpkey[0] == '.') {
              $textvalue = $value['.'];
              unset($value['.']);

              foreach ($value as $name => $attr) {
                $attrs .= ' ' . substr($name, 1) . '="' . $attr . '"';
              }

              $value = $textvalue;
            }

//             foreach ($value as $key1 => $value1) {
//               if($key1 != '@' && $key1 != '.') {
//                 $mixed = true;
//               }
//             }
//             else {
//               $mixed = true;
//             }
          }
          if($mixed) {
            $attrs = '';
          }

          if($key[0] != '@' && $key[0] != '.') {
            $output .= '<' . $key . $attrs . '>' . $value . '</' . $key . '>' . "\n";
          }
        }
      }
    }
    else {

    }

    // regular tags with attr put output in "."
    if($hasAttr && count($tags)) {
      $array['.'] .= $output;
      foreach ($tags as $key) {
        unset($array[$key]);
      }
    }

    return $numkey || $hasAttr? $array : $output;
  }
}

/**
 * The visitor is the proxy part (inside the real object). Element is visiting by proxy and has "proxy method of their real attrs"
 *
 */
class AttrVisitor extends MethodsVisitorConcrete implements ProxyVisitor{

  protected $valuesProxy = array();

  public function methods($classname) {
    if(isset($this->methods)) {
      return $this->methods;
    }

    $this->methods = parent::methods(__CLASS__);

    return $this->methods;
  }


  public function reset() {
    $this->valuesProxy = $this->element->defaults();
  }


  /**
   * Setter[external or internal] and getter. Setter is some like reset for cache
   *
   * @param mixed $subProperties
   *   null : getter
   *   true|false : business init|default
   *   For setting bool value to use '1' or '0' instead
   * @see attr()
   * @see double dispatch (visitor)
   */
  public function attr($propName, $value = NULL){
// krumo($propName, $value);

    if($value === NULL) {
      // Proxy getter
      if(array_key_exists($propName, $this->valuesProxy)) {
        return $this->valuesProxy[$propName];
      }
      // Real init
      else {
        $this->valuesProxy[$propName] = $this->element->{$propName}();
      }
    }
    // false|true : reset to default|reset to default + init
    // Real
    else if($value === TRUE) {
      $this->valuesProxy[$propName] = $this->element->{$propName}();
    }
    // Real with external
    else if($value !== FALSE) {
      $this->valuesProxy[$propName] = $this->element->{$propName}($value);
    }
    else {
      $defaults = $this->element->defaults($propName);
      $this->valuesProxy[$propName] = $defaults[$propName];
    }

//     if($value === NULL) {
//       $return = $this->element->{$propName}();
//       if(isset($return)) {
//         return $return;
//       }
//     }
    // Doing business init
//     else
//     if($value === TRUE) {
//       $return = $this->element->{$propName}();
//     }
//     // Getter
//     else

//     else {
//       $return = $this->element->{$propName}($value);
//     }
//     // Doing busyness init if any
//     if($value === TRUE) {
//       $this->element->{$propName}();
//     }

    // Getter Setter
//     $return = isset($value)? $this->element->{$propName}($value) : $this->element->{$propName}();

    return $this->valuesProxy[$propName];
  }
}