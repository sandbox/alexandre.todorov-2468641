<?php
/**
 * @file
 * Contains \Drupal\rdfxp_arc2\ConfigRdfComponent.
 */

namespace Drupal\rdfxp_arc2;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Cache\CacheBackendInterface;

require_once drupal_get_path('module', 'rdfxp_arc2') . '/rdfxp_arc2.drush.inc';

class ConfigRdfComponent implements \Iterator, \Countable {

  public $componentType = 'Component';


  /**
   * @var \Drupal\Core\Config\Entity\ConfigEntityBase[]
   *
   * TODO: #normal: Use proxy(lazy loading) pattern: load only when needed.
   */
  public $coreConfigEntity = array();

  /**
   * Drupal config from yml files.
   *
   * @var mixed \Drupal\Core\Config\ImmutableConfig[]|array
   */
  public $coreConfig = array();

  /**
   * Additional custom config.
   *
   * @var array
   */
  public $config = array();

  /**
   * Prefix for yml config files.
   *
   * @var string
   */
  public $configPrefix = NULL;

  /**
   * Key of current config element in the set of config elements.
   *
   * @var string
   */
  public $currentConfigId = NULL;

  /**
   * Relative path to config files to use.
   * @var string
   */
  public $source = NULL;

  /**
   * @todo #normal: what about array object instead of $config_rdf->select($config_id)
   */
  public function __construct($options = array()) {

    // TODO: #normal: add mandatory typed date validation for subset of properties or get those properties from config:
    //   ex. $configPrefix in ('core.entity_view_mode.', 'field.storage.', ...), i.e. available configs for rdf processing
    foreach ($options as $prop_name => $option) {
      $this->{$prop_name} = $option;
    }

    if(!isset($this->findOptions)) {
      $this->findOptions = array(
        'name' => $this->configName('*') . '.yml',
      );
    }

    $source_dir = isset($this->source) ? $this->source : ConfigRdfManager::getSyncDir();
    // foreach entities; store $entity.
    $file_sub_pattern = '^' . $source_dir . '(.*)/config/(install|optional)/([^/]+)';

    // TODO: Use \Drupal\Core\Config\FileStorage::lisAll($prefix) when not external config dir.
    //   See ConfigRdfManager::getSyncDir().
    $output = array();
    $cmd = 'cd ' . \Drupal::root() . '; find ' . $source_dir;
    foreach ($this->findOptions as $name => $value) {
      $cmd .= ' -' . $name . ' ' . $value;
    }
    exec($cmd, $output);
    foreach ($output as $line) {
      $matches = array();
      // If match ($module_id) then we are dealing about features
      $module_id = NULL;
      if(preg_match('%' . $file_sub_pattern . '\.yml$%', $line, $matches)) {
        $matches_1 = $matches[1]? $matches[1] : $source_dir;
        $module_id = substr($matches_1, strrpos($matches_1, '/')+1);
        $config_filename = $source_dir. ($matches[1]? '/' . $matches[1] : '' ) . '/config/' . $matches[2] . '/' . $matches[3] . '.yml';
        $config_name = $matches[3];
      }
      // TODO: take into account collection, overrides. Ex. "./fr/$config_name.yml"
      //   $line must equals ConfigRdfManager::getSyncDir() . '/' . $config_name . '.yml'
      else {
        $config_filename = $line;
        $last_slash = strrpos($line, '/');
        $config_name = substr($line, $last_slash+1, strrpos($line, '.yml')-$last_slash-1);
      }

      $config_id = $this->configId($config_name);

      $this->currentConfigId = $config_id;


      // TODO: #minor: option for what config to prefer: file or DB (or override?). Currently only config files.
      //   Show message somewhere: Do "drush cex" for get up to date to DB.
      $this->coreConfig[$config_id] = \Symfony\Component\Yaml\Yaml::parse(file_get_contents($config_filename));

      // dealing with features
      if($module_id) {
        // TODO: #minor: log if "module" key exists in coreConfig
        $this->config[$config_id]['module'] = $module_id;
      }
    }

    // TODO: #review(See iterator): add case when there is no this type of config in this cex ($this->coreConfig = array()).
    $this->rewind();
  }

  public function rewind() {
    if($this->coreConfig) {
      reset($this->coreConfig);
      $this->currentConfigId = key($this->coreConfig);
    }
    else {
      $this->currentConfigId = NULL;
    }
  }
  public function current() {
    return $this->coreConfig[$this->currentConfigId];
  }
  public function key() {
    return $this->currentConfigId;
  }
  public function next() {
    if(next($this->coreConfig) === FALSE) {
      $this->currentConfigId = NULL;
    }
    else {
      $this->currentConfigId = key($this->coreConfig);
    }
  }
  public function valid() {
    return isset($this->currentConfigId);
  }
  public function count() {
    return count($this->coreConfig);
  }

  public function configId($config_name) {
    $mode_parts = explode('.', $config_name, 3);
    return $mode_parts[2];
  }

  public function configName($config_id = NULL) {
    if(!isset($config_id)) {
      $config_id = $this->currentConfigId;
    }
    if(!isset($this->configPrefix)) {
      throw new \Exception('Must set configPrefix property or implement configName() methos');
    }

    return $this->configPrefix. $config_id;
  }

  /**
   * @todo #major: add dependencies.theme
   */
  public function coreConfigGetDependencyName() {
    $deps = array();
    if($configs = $this->coreConfigGet('dependencies.config')) {
      foreach ($configs as $config_name) {
        $deps[$config_name] = $config_name;
      }
    }

    if($configs = $this->coreConfigGet('dependencies.module')) {
      foreach ($configs as $config_name) {
        $deps['module.' . $config_name] = 'module.' . $config_name;
      }
    }

    return $deps;
  }

  public function select($config_id) {
    if(!isset($this->coreConfigEntity[$config_id])) {
      // TODO: #minor: log/msg if wrong $config_id?
      return;
    }
    $this->currentConfigId = $config_id;
  }

  public function addToTriples(&$triples) {

    $config_name = $this->configName();

    // Add component type.
    $triples[$config_name]['rdf:type'][$this->componentType] = $this->componentType;

    // ====== Module, Entity info, Dependency ======
    if($module = $this->coreConfigGetModuleName()) {
      $triples[$config_name]['module'][$module] = $module;
      if(!isset($triples[$module])) {
        $triples[$module]['rdf:type']['Module'] = 'Module';
      }
    }
    // If missing then it is an optional property for that type.
    else {
      $triples['optional'][$this->componentType]['module'] = 'module';
    }

    if($entity_id = $this->coreConfigGetEntityName()) {
      $triples[$config_name]['entity'][$entity_id] = $entity_id;
      if(!isset($triples[$entity_id])) {
        $triples[$entity_id]['rdf:type']['Entity'] = 'Entity';
      }
    }
    // If missing then it is an optional property for that type.
    else {
      $triples['optional'][$this->componentType]['entity'] = 'entity';
    }

    // TODO: #normal: add vocab_uri constant: ex. ConfigRdfManager::VOCABULARY_URI = 'public://files/rdf/rdfxp-bundle-deps-vocab.rdf'
    $ns = 'public://files/rdf/rdfxp-bundle-deps-vocab.rdf';
    list($store, $deps) = rdfxp_arc2_bundle_deps_store();

    // ====== Datatype properties "as they are" ======
    // TODO: #normal(currently only 1 level of subclasses): test if it works for greater than 2 levels of subclasses.
    $q = 'SELECT ?prop WHERE {
      {
        ?prop rdfs:domain <' . $ns . '#' . $this->componentType . '> .
        ?prop rdf:type owl:DatatypeProperty .
      }';
    // TODO: #normal: ComponentPart Interface
    if(get_class($this) != 'Drupal\rdfxp_arc2\ConfigRdfComponentPart') {
      $q .= 'UNION {
        ?prop rdfs:domain ?superClass .
        ?prop rdf:type owl:DatatypeProperty .
        <' . $ns . '#' . $this->componentType . '> rdfs:subClassOf ?superClass .
      }
    }';
    }
    else {
      $q .= '}';
    }

    if ($rows = $store->query($q, 'rows')) {
      foreach ($rows as $row) {

        // TODO: needs comments
        $prop_localname = $prop_configname = substr($row['prop'], strrpos($row['prop'], '#')+1);
        if(isset($this->dataPropertyMap[$prop_localname])) {
          $prop_configname = $this->dataPropertyMap[$prop_localname];
        }
        if(is_array($prop_configname)) {
          $prop_configvalue = array();
          foreach ($prop_configname as $a_prop_configname) {
            $prop_configvalue_for_key = $this->coreConfigGet($a_prop_configname);
            if($prop_configvalue_for_key) {
              $explode = explode('.', $a_prop_configname);
              $prop_configname_key = end($explode);
              $prop_configvalue[$prop_configname_key] = $prop_configvalue_for_key;
            }
          }
        }
        else {
          $prop_configvalue = $this->coreConfigGet($prop_configname);
        }
        if($prop_configvalue) {
          $triples[$config_name][$prop_localname][] = $prop_configvalue;
        }
      }
    }
    // TODO: #major: Rdfs annotation properties
    foreach (array('rdfs:label') as $prop_localname) {
      if(isset($this->dataPropertyMap[$prop_localname])) {
        $prop_configname = $this->dataPropertyMap[$prop_localname];
        $triples[$config_name][$prop_localname][] = $this->coreConfigGet($prop_configname);
      }
    }

    // ====== Components ======
    // TODO: #review: Add union ?component rdfs:subClassOf <' . $ns . '#ComponentPart> .
    $q = 'SELECT ?prop ?component ?componentType WHERE {
      {
        ?prop rdfs:domain <' . $ns . '#' . $this->componentType . '> .
        ?prop rdf:type owl:ObjectProperty .
        ?component rdfs:subClassOf <' . $ns . '#Component> .
        ?prop rdfs:range ?component .
        ?component rdfs:subClassOf ?componentType .
      }
      UNION {
        ?prop rdfs:domain <' . $ns . '#' . $this->componentType . '> .
        ?prop rdf:type owl:ObjectProperty .
        ?component rdfs:subClassOf <' . $ns . '#ComponentPart> .
        ?prop rdfs:range ?component .
        ?component rdfs:subClassOf ?componentType .
      }
    }';

    if ($rows = $store->query($q, 'rows')) {
      foreach ($rows as $row) {

        $prop_localname = substr($row['prop'], strrpos($row['prop'], '#')+1);
        $cmpnt_localname = substr($row['component'], strrpos($row['component'], '#')+1);

        // Get from service
        $configRdf = ConfigRdfManager::getManager();

        // TODO: needs comments
        // See Drupal\rdfxp_arc2\ConfigRdfFieldField::coreConfigGetTargetBundleName()
        $method = 'coreConfigGet' . ucfirst($prop_localname) . 'Name';
        $method1 = 'get' . $cmpnt_localname;
        $strict = TRUE;
        if(!$strict && !is_callable(array($configRdf, $method1))) {
          continue;
        }
        $bundle = $configRdf->$method1();

        if($obj_prop_config_name = $this->{$method}($bundle)) {
          if(!is_array($obj_prop_config_name)) {
            $obj_prop_config_name = array($obj_prop_config_name => $obj_prop_config_name);
          }

          // If at least one component of a given Type doesn't have a property value
          // and report as optional "isset($obj_prop_config_name['optional'][$this->componentType]"
          // (ex. targetBundle for field.field.node.page.body FieldField, then this
          // property becomes optional (for sparql queries) for all configs of that type.
          //
          // If this property must not be optional for a component, then override addTotriples,
          // call parent, then unset $triples['optional'][$this->componentType][$optional_prop]
          if(isset($obj_prop_config_name['optional'][$this->componentType])) {
            foreach($obj_prop_config_name['optional'][$this->componentType] as $optional_prop ) {
              $triples['optional'][$this->componentType][$optional_prop] = $optional_prop;
            }
          }
          else {
            if(!isset($triples[$config_name][$prop_localname])) {
              $triples[$config_name][$prop_localname] = array();
            }
            $triples[$config_name][$prop_localname] += $obj_prop_config_name;
          }
        }

      }
    }
  }

  /**
   * @todo #normal: move cache handling to addToTriples() method
   * @param unknown $triples
   */
  public function allToTriples(&$triples) {
    $component = $this;
    echo t('Processing :count item(s) for :type component', array(
      ':count' => count($component),
      ':type' => $component->componentType,
    )) . "\n";

    $from_cache = $echo = 0;
    foreach ($component as $config_id => $config) {
      $config_name = $component->configName();
      $cid = 'rdfxp_arc2.config_rdf_resource:' . $config_name;
      $data = NULL;
      if ($cache = \Drupal::cache()->get($cid)) {
        $data = $cache->data;
        $triples[$config_name] = $data;
        // Add "module" type if data from cache.
        if(isset($triples[$config_name]['module'])) {
          $module = key($triples[$config_name]['module']);
          if(!isset($triples[$module])) {
            $triples[$module]['rdf:type']['Module'] = 'Module';
          }
        }
        // Add "entity" type if data from cache.
        if(isset($triples[$config_name]['entity'])) {
          $module = key($triples[$config_name]['entity']);
          if(!isset($triples[$module])) {
            $triples[$module]['rdf:type']['Entity'] = 'Entity';
          }
        }
        // Add "field_type" type if data from cache.
        if(isset($triples[$config_name]['field_type'])) {
          $module = key($triples[$config_name]['field_type']);
          if(!isset($triples[$module])) {
            $triples[$module]['rdf:type']['FieldType'] = 'FieldType';
          }
        }
        $from_cache++;
      }
      else {
        echo '.';
        $echo++;
        $component->addToTriples($triples);

        if($deps_name = $this->coreConfigGetDependencyName()) {

          if(!is_array($deps_name)) {
            $deps_name = array($deps_name => $deps_name);
          }
          // TODO: #major: test perf here
          foreach ($deps_name as $dep_name => $no_matter) {
            $already_dep = FALSE;
            foreach ($triples[$config_name] as $property => $dep) {
              if(isset($triples[$config_name][$property][$dep_name])) {
                $already_dep = TRUE;
              }
            }

            if(!$already_dep) {
              $triples[$config_name]['dependencies'][$dep_name] = $dep_name;
            }
          }
        }
        $triples['optional'][$this->componentType]['dependencies'] = 'dependencies';

        $data = $triples[$config_name];
        \Drupal::cache()->set($cid, $data, CacheBackendInterface::CACHE_PERMANENT, [
          'config_rdf_type:' . $component->componentType,
          'config_rdf_resource:' . $config_name,
        ]);
      }
    }

    if($echo) {
      echo "\n" . t(':count item(s) computed', array(
        ':count' => $echo,
      )) . "\n";
    }
    if($from_cache) {
      echo t(':count item(s) loaded from cache', array(
        ':count' => $from_cache,
      )) . "\n";
    }
  }

  public function coreConfigGetModuleName() {
    $return = NULL;
    if($module = $this->configGet('module')) {
      $return = 'module.' . $module;
    }

    return $return;
  }

  public function coreConfigGetEntityName() {
    // TODO: #review: To be verified. It is ok for storage, bundle, field, ...
    $return = $this->coreConfigGet('entity_type');
    if(isset($return)) {
      $return = 'entity.' . $return;
    }

    return $return;
  }

  /**
   * When a config ($this) is composed by onother config ($component)
   * @param Drupal\rdfxp_arc2\ConfigRdfComponent $component
   */
  public function coreConfigGetConfigName(ConfigRdfComponent $component) {

    $components = array();
    foreach ($component as $config_id => $config) {
      $components[$component->configName()] = $component->configName();
    }
    return $components;
  }

  public function coreConfigGet($name = NULL) {
    if(!isset($this->coreConfig[$this->currentConfigId])) {
      return NULL;
    }
    if (is_array($this->coreConfig[$this->currentConfigId])) {
      $parents = $name? explode('.', $name) : array();
      return NestedArray::getValue($this->coreConfig[$this->currentConfigId], $parents);
    }
    return $this->coreConfig[$this->currentConfigId]->get($name);
  }

  public function configGet($name = NULL) {
    if(isset($this->config[$this->currentConfigId])) {
      $parents = $name? explode('.', $name) : array();
      return NestedArray::getValue($this->config[$this->currentConfigId], $parents);
    }
    else {
      return NULL;
    }
  }

}