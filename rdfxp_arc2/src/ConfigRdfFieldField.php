<?php
/**
 * @file
 * Contains \Drupal\rdfxp_arc2\ConfigRdfFieldField.
 */

namespace Drupal\rdfxp_arc2;


class ConfigRdfFieldField extends ConfigRdfComponent {

  public $componentType = 'FieldField';

  public $configPrefix = 'field.field.';

  public $bundle = array();
  public $bundleName = array();

  // TODO: #major: change in ConfigRdfComponent.php so that it is possible to write
  //   'alt_trans' => 'third_party_settings.content_translation.translation_sync.alt'
  public $dataPropertyMap = array(
    'rdfs:label' => 'label',
    'image_settings' => array(
      'settings.alt_field',
      'settings.alt_field_required',
      'third_party_settings.content_translation.translation_sync.alt',
      'settings.title_field',
      'settings.title_field_required',
      'third_party_settings.content_translation.translation_sync.title',

      'settings.file_directory',
      'settings.file_extensions',
      'third_party_settings.content_translation.translation_sync.file',
    ),
  );

  // ======== dependencies

  public function coreConfigGetBundleName($bundle) {
    $this->bundleName[$this->currentConfigId] = $bundle->configName($this->coreConfigGet('entity_type') . '.' . $this->coreConfigGet('bundle'));
    return $this->bundleName[$this->currentConfigId];
  }

  public function coreConfigGetFieldStorageName($storage) {
    return $storage->configName($this->coreConfigGet('entity_type') . '.' . $this->coreConfigGet('field_name'));
  }

  public function coreConfigGetTargetBundleName($bundle) {

    $this->bundle[$this->currentConfigId] = $bundle;
    $config_name = $this->configName();

    $return = array();
    if(($settings_handler = $this->coreConfigGet('settings.handler')) && $this->coreConfigGet('settings.handler_settings.target_bundles')) {
      foreach ($this->coreConfigGet('settings.handler_settings.target_bundles') as $entity_bundle) {

        $config_id = explode(':', $settings_handler)[1] . '.' . $entity_bundle;
        $obj_prop_config_name = $bundle->configName($config_id);
        $return[$obj_prop_config_name] = $obj_prop_config_name;
      }
    }
    else {
      $return['optional'][$this->componentType]['targetBundle'] = 'targetBundle';
    }

    return $return;
  }

  /**
   * {@inheritDoc}
   * @see \Drupal\rdfxp_arc2\ConfigRdfComponent::addToTriples()
   */
  public function addToTriples(&$triples) {
    parent::addToTriples($triples);

    $config_name = $this->configName();

    // For entity reference fields
    $entity_id = NULL;
    if($settings_handler = $this->coreConfigGet('settings.handler')) {
      $entity_id = 'entity.' . explode(':', $settings_handler)[1];
      $triples[$config_name]['target_entity'][$entity_id] = $entity_id;
      if(!isset($triples[$entity_id])) {
        $triples[$entity_id]['rdf:type']['Entity'] = 'Entity';
      }
    }
    // If missing then it is an optional property for that type.
    else {
      $triples['optional'][$this->componentType]['target_entity'] = 'target_entity';
    }

    // field type
    // TODO: Automate like "Datatype properties "as they are"". See Drupal\rdfxp_arc2\ConfigRdfComponent
    $field_type = 'field_type.' . $this->coreConfigGet('field_type');
    if(!isset($triples[$field_type])) {
      $triples[$field_type]['rdf:type']['FieldType'] = 'FieldType';
    }
    $triples[$config_name]['field_type'][$field_type] = $field_type;


    // reference or attrubute
    $compnent_type = implode('', array_map('ucfirst', explode('_', $this->coreConfigGet('field_type'))));
    if(in_array($compnent_type, array('Image', 'File'))) {
      $compnent_type .= 'Reference';
    }
    $triples[$config_name]['rdf:type'][$compnent_type] = $compnent_type;
    $bundle_id = $this->bundleName[$this->currentConfigId];
    if(in_array($compnent_type, array('EntityReference', 'FileReference', 'ImageReference', 'EntityReferenceRevisions'))) {
      $triples[$bundle_id]['reference'][$config_name] = $config_name;
    }
    else {
      $triples[$bundle_id]['attribute'][$config_name] = $config_name;
    }
    if($compnent_type == 'EntityReferenceRevisions') {
      $triples[$config_name]['aggregation'][] = 'composite';
    }

    // reference data-type
    if($entity_id) {
      $triples[$config_name]['data-type'][$entity_id] = $entity_id;
    }

    $target_bundles = $this->coreConfigGetTargetBundleName($this->bundle[$this->currentConfigId]);
    if(!isset($target_bundles['optional'])) {
      if(!isset($triples[$config_name]['data-type'])) {
        $triples[$config_name]['data-type'] = array();
      }
      $triples[$config_name]['data-type'] += $target_bundles;
    }

    // Missing from vocab mappings
    $required_association_end = 'required-association-end';
    $triples[$config_name][$required_association_end][] = $this->coreConfigGet('required');
  }
}