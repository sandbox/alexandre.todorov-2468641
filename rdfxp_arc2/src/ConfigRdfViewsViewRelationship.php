<?php
/**
 * @file
 * Contains \Drupal\rdfxp_arc2\ConfigRdfViewsViewRelationship.
 */

namespace Drupal\rdfxp_arc2;


/**
 * @todo Currently there is for some views relationships an auto detected entity property from
 *   "entity_type: node". Ex. in views.view.content_recent.display.default.display_options.relationships.uid
 *   But is missing in views.view.content.display.default.display_options.relationships.uid
 *   Anyway entity property must exists for ViewsView. When done remove that property
 *   for ViewsViewRelationship
 */
class ConfigRdfViewsViewRelationship extends ConfigRdfComponentPart {

  public $componentType = 'ViewsViewRelationship';

  public $path = 'display_options.relationships';

  public $dataPropertyMap = array(
    'plugin' => 'plugin_id',
  );

  public function coreConfigGetDependencyName() {
    if($this->coreConfigGet('plugin_id') == 'entity_queue') {
      return 'entityqueue.entity_queue.' . $this->coreConfigGet('limit_queue');
    }
  }
}