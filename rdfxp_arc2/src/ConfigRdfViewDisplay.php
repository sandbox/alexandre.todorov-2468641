<?php
/**
 * @file
 * Contains \Drupal\rdfxp_arc2\ConfigRdfViewDisplay.
 */

namespace Drupal\rdfxp_arc2;

class ConfigRdfViewDisplay extends ConfigRdfModeDisplay {

  public $componentType = 'ViewDisplay';

  public $configPrefix = 'core.entity_view_display.';

  public function coreConfigGetViewModeName($viewMode) {
    return $viewMode->configName($this->coreConfigGet('targetEntityType') . '.' . $this->coreConfigGet('mode'));
  }

  /**
   * {@inheritDoc}
   * @see \Drupal\rdfxp_arc2\ConfigRdfComponent::addToTriples()
   */
  public function addToTriples(&$triples) {
    parent::addToTriples($triples);

    // View modes "default" are missing in config export
    // ex. core.entity_view_mode.node.default
    if($this->coreConfigGet('mode') == 'default') {

      $configRdf = ConfigRdfManager::getManager();
      $viewMode = $configRdf->getViewMode();
      $config_name = $this->coreConfigGetViewModeName($viewMode);

      $triples[$config_name]['rdf:type']['ViewMode'] = 'ViewMode';
      $triples[$config_name]['entity'][$this->coreConfigGet('targetEntityType')] = $this->coreConfigGet('targetEntityType');
    }
  }
}