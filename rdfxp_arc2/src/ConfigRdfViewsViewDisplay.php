<?php
/**
 * @file
 * Contains \Drupal\rdfxp_arc2\ConfigRdfViewsViewDisplay.
 */

namespace Drupal\rdfxp_arc2;


class ConfigRdfViewsViewDisplay extends ConfigRdfComponentPart {

  public $componentType = 'ViewsViewDisplay';

  public $path = 'display';

  public $dataPropertyMap = array(
    'rdfs:label' => 'display_title',
    'plugin' => 'display_plugin',
    'access' => 'display_options.access',
    'options' => array(
      'display_options.cache',
      'display_options.query',
    ),
  );

  /**
   *
   * @var \Drupal\rdfxp_arc2\ConfigRdfViewsViewRelationship[]
   */
  public $relationship = NULL;

  public function coreConfigGetViewModeName($view_mode) {
    $return = array();
    // viewMode property is optional for ViewsViewDisplay
    $row_type = explode(':', $this->coreConfigGet('display_options.row.type'));
    if(isset($row_type[1]) && $row_type[0] == 'entity') {
      $return = $view_mode->configName($row_type[1] . '.' . $this->coreConfigGet('display_options.row.options.view_mode'));
    }
    else {
      $return['optional'][$this->componentType]['viewMode'] = 'viewMode';
    }

    return $return;
  }

  /**
   * Relationship is a component part of $this so coreConfigGetRelationship() is needed
   * ViewsViewRelationship decores $this
   *
   * @return \Drupal\rdfxp_arc2\ConfigRdfViewsViewRelationship[]
   */
  public function coreConfigGetRelationship() {
    if(!isset($this->relationship[$this->currentConfigId])) {
      $this->relationship[$this->currentConfigId] = new ConfigRdfViewsViewRelationship($this);
    }
    return $this->relationship[$this->currentConfigId];
  }

  public function coreConfigGetViewsViewRelationshipName($relationship) {
    return parent::coreConfigGetConfigName($relationship);
  }
}