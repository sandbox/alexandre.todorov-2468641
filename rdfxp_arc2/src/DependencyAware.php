<?php

namespace Drupal\rdfxp_arc2;

class DependencyAware {


  protected $modelId;
  protected $model;
  /**
   * @var array $deps component dependencies: domainOf with range Types.
   */
  protected $dependenciesFromVocab;
  protected $dependenciesAppended = array();

  public function __construct($model) {
    $this->setModel($model);
  }

  public function reset() {
    $this->dependenciesAppended = array();
  }

  /**
   * @todo #major: Throws an exception if any
   *   of valid string or \ARC2_Store or \SwoopModel.
   *
   * For queries with "no optional" dbModel is much faster. For query with optionals
   * only memModel seems to output correct result, so use both: dbModel for check
   * if there is a result without optionals, then memModel if optional must be added
   *
   * @param mixed string|\ARC2_Store|\SwoopModel $model
   */
  public function setModel($model) {

    if($model instanceof \ARC2_Store) {
      $this->store = $this->model = $model;
      // In this case modelId is the base ns0
      if(isset($model->rdfxp_ns0)) {
        $this->storeId = $this->modelId = $model->rdfxp_ns0;
      }
    }
    else if($model instanceof \SwoopModel) {
      $this->model = $model;
      $this->modelId = $model->model->modelid;
      $this->store = $this->model->dbModel;
    }
    else {
      $this->modelId = $model;
      $this->model = rdfxp_get_model($this->modelId);
      $this->store = $this->model->dbModel;
    }

    // $deps is coming from the same model than $modelId, but without individuals
    // TODO: #critical: Execute the "deps" sparql against $modelId and verify the result is correct
    require_once drupal_get_path('module', 'rdfxp_arc2') . '/rdfxp_arc2.drush.inc';
    list($deps_store, $deps) = rdfxp_arc2_bundle_deps_store();
    $this->dependenciesFromVocab = $deps;

    $is_db_model = $this->store instanceof \DbModel;

    // Get optionals from the model
    $q = 'SELECT ?optional ?class ?property WHERE {
      ?optional rdf:type <#Optional> .
      ?optional ?class ?property .
    }';

    if($is_db_model) {
      $q = str_replace(array('<#', '> '), array('ns0:', ' '), $q);
      $prefixes = '';
      foreach ($this->store->getParsedNamespaces() as $uri => $prefix) {
        $prefixes .= 'PREFIX ' . $prefix . ': <' . $uri . '>' . "\n";
      }
      $q = $prefixes . $q;

      $rows = $this->store->sparqlQuery($q);
    }
    // ARC2 store
    else {
      if(isset($this->storeId)) {
        $ns0_uri = $this->storeId;
        $q = str_replace('<#', '<' . $ns0_uri, $q);
      }
      $rows = $this->store->query($q, 'rows');
    }

    $optional = array();
    if ($rows) {
      foreach ($rows as $row) {
        $var = 'class';
        $var_key = $is_db_model ? '?' . $var : $var;
        $uri = $is_db_model ? $row[$var_key]->uri : $row[$var_key];
        $class_localname = substr($uri, strrpos($uri, '#')+1);

        $var = 'property';
        $var_key = $is_db_model ? '?' . $var : $var;
        $uri = $is_db_model ? $row[$var_key]->uri : $row[$var_key];
        $property_localname = substr($uri, strrpos($uri, '#')+1);


        if(!($class_localname == 'type' && $property_localname == 'Optional')) {
          $optional[$class_localname][$property_localname] = $property_localname;
        }
      }
    }

    $cid = 'rdfxp_arc2.config_rdf_optional';
    \Drupal::cache()->set($cid, $optional);

  }

  public function getDependenciesAppended() {
    return $this->dependenciesAppended;
  }

  public function get($types, $is_rev = FALSE) {
    $prev = $types;

    if($is_rev) {

      $config_rdf_manager = ConfigRdfManager::getManager();
      $components = array_merge($config_rdf_manager->getComponentTypes(), $config_rdf_manager->getComponentParts());
      // Added by .patch for automatic testing. do patch -R
      if($extra_debug = method_exists($this, 'rdfxpItem')) {
        $drupal_static_item = & $this->rdfxpItemKey('$component_name in $components');
      }
      foreach ($components as $key => $component_name) {
        // Added by .patch for automatic testing. do patch -R
        if($extra_debug) {
          $drupal_static_copy = & $this->rdfxpPre($drupal_static_item, $call_action = $component_name);
        }

        // Get "rev" individuals for $types
        $missing = $this->getMissing($component_name, $types, $prev);

        // If there are missing (deps aware) individuals then look for all deps
        if(count($missing)) {

          $this->dependenciesAppended[] = array(
            'look_for' => $component_name,
            'searched' => $types,
            'appended' => $missing,
          );

          $this->getMissingDeep($prev, $missing);
        }

        // Added by .patch for automatic testing. do patch -R
        if($extra_debug) {
          $this->rdfxpPost($drupal_static_item, $call_action, $drupal_static_copy);
        }
      }
    }
    else {
      $this->getMissingDeep($prev, $types);
    }

    return $prev;
  }

  /**
   *  @param string $look_for type of components to look for
   *  @param array $types used for filtering
   *  @param array(ref) $prev is the total output
   *  @return array $missing
   */
  public function getMissing($look_for, $types, &$prev) {

    $missing = array();

    $output = $this->getDependencies($look_for, $types);
    foreach ($output as $type => $values) {

      // Search for missing.
      // For rel filters, $types[$look_for] are already in $prev, so do not add $types[$look_for]
      // in mising. Thus ckecking only $type != $look_for when "rel" searching (i.e. isset($types[$look_for]))
      if(!isset($types[$look_for]) || isset($types[$look_for]) && $type != $look_for) {
        if(isset($prev[$type])) {
          if($diff = array_diff_key($values, $prev[$type])) {

            if(!isset($missing[$type])) {
              $missing[$type] = array();
            }
            $missing[$type] += $diff;
          }
        }
        else {
          $missing[$type] = $values;
        }
      }
      // TODO: #major: if in $output[$look_for] they are items which are not in $types[$look_for] (already in $prev)
      //   add unit test of these cases
      else {}

      // Update prev with new entries
      if(!isset($prev[$type])) {
        $prev[$type] = array();
      }
      $prev[$type] += $values;
    }

    return $missing;
  }

  /**
   * Create sparql query frop deps of $type.
   *   - Add "REL" filter using individuals of type $type ($types[$type]). If there
   *     are other keys in $types, so add "REV" filters.
   *
   * @todo #major: Update comments and some cleaning
   *
   * @param string $type Look for dependancies of that type
   * @param array $types
   */
  public function getDependencies($type, $types) {
    // Added by .patch for automatic testing. do patch -R
    // only for debug, not for assertions => debug.patch + assert.patch
    if($extra_debug = method_exists($this, 'rdfxpItem')) {
      $drupal_static_item = & $this->rdfxpItem(__FUNCTION__);
    }

    $store_data = $this->store;
    $deps_from_vocab = $this->dependenciesFromVocab;

    $cid_optional = 'rdfxp_arc2.config_rdf_optional';
    if ($cache = \Drupal::cache()->get($cid_optional)) {
      $optional = $cache->data;
    }

    $output = $optionals = array();
    // Nothing to do (no rel nor rev) if there is no deps for "look for" $type
    if(!isset($deps_from_vocab[$type])) {
      return $output;
    }

    // Get $type deps from all deps ($deps_from_vocab)
    // Each key of $deps will be a var in the query
    $deps = $deps_from_vocab[$type];
    $deps += $deps_from_vocab['Component'];

    // Added by .patch for automatic testing. do patch -R
    if($extra_debug) {
      $drupal_static_item['get_defined_vars']['deps'] = $deps;
    }

    // For rev: If $type not in deps so no way that $type references $types
    //   else it is rel: $type must be in $types
    //
    // this line was deleted


    $rev_filters = $rel_filter = FALSE;

    $type_var = lcfirst($type);

    $select = 'SELECT ?' . $type_var;
    // type of look_for
    $triples = "\n" . '?' . $type_var . ' rdf:type <#' . $type . '> .';
    $filter = $optional_triples = $optional_vars = '';
    $optional_filter = array();
    foreach ($deps as $dep_prop => $dep_type) {
      if($is_optional = isset($optional[$type][$dep_prop])) {
        $optionals[$dep_prop][$dep_type] = $dep_type;
      }

      $tmp_var = ' ?' . $dep_prop;

      // type for deps (?prop_value)
      $tmp_triples = "\n" . ($is_optional ? 'OPTIONAL { ' : '' ) .
          ($dep_type != 'Component' ? '?' . $dep_prop . ' rdf:type <#' . $dep_type . '> ' . ' .' : '') . "\n";
      // look_for prop ?prop_value
      $tmp_triples .= "\n" . '?' . $type_var . ' <#' . $dep_prop . '> ?' . $dep_prop . ' ' . ($is_optional? '}' : '') . ' .' . "\n";

      if($is_optional) {
        $optional_vars .= $tmp_var;
        $optional_triples .= $tmp_triples;
      }
      else {
        $select .= $tmp_var;
        $triples .= $tmp_triples;
      }

      // Add filter using types
      // Ex. $type "FieldField"
      // $dep_type "Entity" that $dep_prop "references" $types['Entity'] "Entity:entity.node"
      // Optional is exluded from filtering or change the query - With swoop model only
      //   reference field are shown, ok for arc2
      //   Problem: user ref field that references node will not be shown if no filter
      //     for optional
      //   Problem: if moving optional dep filter inside optional pattern, then with
      //     swoop model target_entity is empty
      //   TODO: #major: howto / better doc comment: manual handling when optional filters are needed. Use cache id "rdfxp_arc2.config_rdf_optional"
      //     to get all optionals
      //
      // Those are "REV" filters
      if(isset($types[$dep_type])) { // && !$is_optional) {
        $values = array_keys($types[$dep_type]);

        $tmp_filter = '?' . $dep_prop . ' = <#' . implode('> || ?' . $dep_prop . ' = <#', $values) . '> ';
        if($is_optional) {
          $optional_filter[] = $tmp_filter;
        }
        else {
          $filter .= ($filter? ' || ': "\n" . 'FILTER (') . $tmp_filter;
        }

        $rev_filters = TRUE;
      }
    }
    // Add filter for individuals of the searched type
    // With arc2, all fields that ns0:target_entity, not only field tags, so prefer Swoop Model
    //   OPTIONAL { ?fieldField ns0:target_entity ?target_entity }
    // Ex. FieldField:field.field.node.article.field_tags
    if(isset($types[$type])) {
      $values = array_keys($types[$type]);
      $filter .= ($filter? ' || ': "\n" . 'FILTER (') . '?' . $type_var . ' = <#' . implode('> || ?' . $type_var . ' = <#', $values) . '> ';

      $rel_filter = TRUE;
    }

    // Added by .patch for automatic testing. do patch -R
    if($extra_debug) {
      $drupal_static_item['get_defined_vars']['optionals'] = $optionals;
    }

    //$select .= ' WHERE {' . "\n";

    if($filter) {
      $filter .= ')';
    }

    $q = $select . ' WHERE {' . "\n" . $triples . $filter . "\n" . '}';

    if(($is_swoop_model = $store_data instanceof \DbModel)) {

      $q = str_replace(array('<#', '> '), array('ns0:', ' '), $q);

      $prefixes = '';
      foreach ($store_data->getParsedNamespaces() as $uri => $prefix) {
        $prefixes .= 'PREFIX ' . $prefix . ': <' . $uri . '>' . "\n";
      }
      $q = $prefixes . $q;

      $rows = $store_data->sparqlQuery($q);
    }
    else {
      // When loaded from rdf uri, arc2 need a full uri. ns0:{short_name} doesn't
      // work, so use modelId for ns0 uri
      if(isset($this->storeId)) {
        $ns0_uri = $this->storeId;
        $q = str_replace('<#', '<' . $ns0_uri, $q);
      }
      $rows = $store_data->query($q, 'rows');
    }

    // Added by .patch for automatic testing. do patch -R
    if($extra_debug) {
      $drupal_static_item['get_defined_vars']['q'] = $q;
    }
    // TODO: #normal: dulicate code for query execution
    // Add optional triples only if not empty result. Else optional doesn't have sense.
    if($rows && $optional_triples) {
      $store_data = $this->model;

      if($optional_filter) {
        $is_filter = (bool) $filter;
        $filter .= ($is_filter? ' || ': "\n" . 'FILTER (') . implode(' || ', $optional_filter) . ($is_filter? '' : ') ');
      }

      $q = $select . $optional_vars . ' WHERE {' . "\n" . $triples . $optional_triples . $filter . "\n" . '}';

      if(($is_swoop_model = $store_data instanceof \SwoopModel)) {

        $q = str_replace(array('<#', '> '), array('ns0:', ' '), $q);
        $rows = $store_data->sparqlQuery($q);
      }
      else {
        // When created by drush, arc2 need a full uri. ns0:{short_name} doesn't work,
        // so use modelId for ns0 uri
        if(isset($this->modelId)) {
          $ns0_uri = $this->modelId;
          $q = str_replace('<#', '<' . $ns0_uri, $q);
        }
        $rows = $store_data->query($q, 'rows');
      }

      // Added by .patch for automatic testing. do patch -R
      if($extra_debug) {
        $drupal_static_item['get_defined_vars']['q'] = $q;
      }
    }

    // Nothing to do if there is neither "rev" nor "rel" filters
    // Must be before query execution, but need to dump $q
    if(!$rev_filters && !$rel_filter) {
      return $output;
    }

    $rows_uri = array();
    if($rows) {
      $vars = array_keys($deps);
      $vars[] = $type_var;
      foreach ($rows as $i => $row) {
        foreach ($vars as $var) {

          $var_key = $is_swoop_model ? '?' . $var : $var;

          // because of optionals
          if(!empty($row[$var_key])) {

            $uri = $is_swoop_model ? $row[$var_key]->uri : $row[$var_key];
            $class_localname = substr($uri, strrpos($uri, '#')+1);


            // TODO: replace "isset($deps[$var])" by "$var != $type"
            $output_type = isset($deps[$var]) ? $deps[$var] : ucfirst($var); // $vars[] = lcfirst($type);
            $output[$output_type][$class_localname] = $class_localname;

            $rows_uri[$i][$var] = $uri;
          }
        }
      }
    }
    // Added by .patch for automatic testing. do patch -R
    if($extra_debug) {
      $drupal_static_item['get_defined_vars']['rows_uri'] = $rows_uri;
    }

    $config_rdf_manager = ConfigRdfManager::getManager();
    $component_prefixes = $config_rdf_manager->getComponentTypePrefixes();
    $component_prefixes += array(
      'module.' => 'Module',
      'entity.' => 'Entity',
    );
    if(isset($output['Component'])) {
      foreach ($output['Component'] as $component => $no_matter) {
        reset($component_prefixes);
        $component_type = '';
        do {
          $type = current($component_prefixes);
          $component_prefix = key($component_prefixes);
          if(strpos($component, $component_prefix) === 0) {
            $component_type = $type;
          }
        }
        while(!$component_type && next($component_prefixes) );

        if($component_type) {
          $output[$component_type][$component] = $no_matter;
          unset($output['Component'][$component]);
        }
      }

      if(!$output['Component']) {
        unset($output['Component']);
      }
    }

    foreach ($output as $output_type => $output_instances) {
      natsort($output[$output_type]);
    }

    return $output;
  }

  public function getMissingDeep(&$prev, array $missing) {

    // Added by .patch for automatic testing. do patch -R
    if($extra_debug = method_exists($this, 'rdfxpItem')) {
      $drupal_static_item = & $this->rdfxpItemKey('$type, $values in $missing');
    }
    foreach ($missing as $type => $values) {
      // Added by .patch for automatic testing. do patch -R
      if($extra_debug) {
        $drupal_static_copy = & $this->rdfxpPre($drupal_static_item, $call_action = $type);
      }

      $types = array(
        $type => $values,
      );
      $missing_in = $this->getMissing($type, $types, $prev) ;

      if(count($missing_in)) {

          $this->dependenciesAppended[] = array(
            'look_for' => $type,
            'searched' => $types,
            'appended' => $missing_in,
          );

        $this->getMissingDeep($prev, $missing_in);
      }

      // Added by .patch for automatic testing. do patch -R
      if($extra_debug) {
        $this->rdfxpPost($drupal_static_item, $call_action, $drupal_static_copy);//, NULL, FALSE);
        $drupal_static_item[$call_action]['types'] = $types;
      }
    }
  }
}