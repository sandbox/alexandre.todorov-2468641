<?php
/**
 * @file
 * Contains \Drupal\rdfxp_arc2\ConfigRdfModeDisplay.
 */

namespace Drupal\rdfxp_arc2;


abstract class ConfigRdfModeDisplay extends ConfigRdfMode {

  // ======== dependencies
  public function coreConfigGetBundleName($bundle) {
    return $bundle->configName($this->coreConfigGet('targetEntityType') . '.' . $this->coreConfigGet('bundle'));
  }

  public function coreConfigGetFieldName($fieldField) {

    $components = array();
    // TODO: hidden fields: $this->coreConfigGet('hidden')
    foreach ($this->coreConfigGet('content') as $field_name => $field_settings) {
      $config_id = $this->coreConfigGet('targetEntityType') . '.' . $this->coreConfigGet('bundle') . '.' . $field_name;
      $components[$fieldField->configName($config_id)] = $fieldField->configName($config_id);
    }

    return $components;
  }
}