<?php
/**
 * @file
 * Contains \Drupal\rdfxp_arc2\ConfigRdfComponentPart.
 */

namespace Drupal\rdfxp_arc2;

/**
 * @todo #normal Decorator pattern.
 */
class ConfigRdfComponentPart extends ConfigRdfComponent {

  public $component = NULL;

  public function __construct(ConfigRdfComponent $component) {

    $this->component = $component;

    if($core_config = $this->component->coreConfigGet($this->path)) {
      $this->coreConfig = $core_config;
    }

    $this->rewind();
  }

  public function configName($config_id = NULL) {
    if(!isset($config_id)) {
      $config_id = $this->currentConfigId;
    }
    return $this->component->configName() . '.' . $this->path . '.' . $config_id;
  }
}