<?php
/**
 * @file
 * Contains \Drupal\rdfxp_arc2\ConfigRdfMode.
 */

namespace Drupal\rdfxp_arc2;


class ConfigRdfMode extends ConfigRdfComponent {

  // ======== dependencies

  public function coreConfigGetEntityName() {
    return 'entity.' . $this->coreConfigGet('targetEntityType');
  }
}