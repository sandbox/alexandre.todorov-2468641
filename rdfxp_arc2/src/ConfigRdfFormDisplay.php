<?php
/**
 * @file
 * Contains \Drupal\rdfxp_arc2\ConfigRdfFormDisplay.
 */

namespace Drupal\rdfxp_arc2;

class ConfigRdfFormDisplay extends ConfigRdfModeDisplay {

  public $componentType = 'FormDisplay';

  public $configPrefix = 'core.entity_form_display.';

  public function coreConfigGetFormModeName($formMode) {
    return $formMode->configName($this->coreConfigGet('targetEntityType') . '.' . $this->coreConfigGet('mode'));
  }
}