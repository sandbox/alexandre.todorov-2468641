<?php
/**
 * @file
 * Contains \Drupal\rdfxp_arc2\ConfigRdfManager.
 */

namespace Drupal\rdfxp_arc2;

/**
 * @todo #major: Add comments (devs already done): For almost all possible components use dependencies property in order to
 *   have consistant deps aware. Ex:
 *   public function getSomeComponent($source = NULL, $reset = FALSE) {
 *     $this->configRdf['SomeComponent'] = new ConfigRdfComponent(array(
 *       'componentType' => 'SomeComponent',
 *       'configPrefix' => 'come.component.',
 *       'source' => isset($source) ? $source : self::getSyncDir(),
 *     ));
 *   }
 */
class ConfigRdfManager {

  public $configRdf = array();

  public $getComponentTypePrefixes;


  public static function getSyncDir() {
    // Use core.extension config file path to get the default config directory because getCollectionDirectory() is protected
    /**
     * @var \Drupal\Core\Config\FileStorage
     */
    $config_storage = \Drupal::service('config.storage.sync');

    $config_file_path = $config_storage->getFilePath('core.extension');
    $source = substr($config_file_path, 0, strrpos($config_file_path, '/core.extension'));
    return $source;
  }

  /**
   * @return \Drupal\rdfxp_arc2\ConfigRdfComponent
   */
  public function getFieldStorage($source = NULL, $reset = FALSE) {

    if(!isset($this->configRdf['FieldStorage']) || $reset) {

      // TODO: class ConfigRdfFieldStorage::addToTriples()
      //   See Drupal\rdfxp_arc2\ConfigRdfFieldField::addToTriples($triples)
      $this->configRdf['FieldStorage'] = new ConfigRdfFieldStorage(array(
        'source' => isset($source) ? $source : self::getSyncDir(),
      ));
    }

    return $this->configRdf['FieldStorage'];
  }

  /**
   * @return \Drupal\rdfxp_arc2\ConfigRdfBundle
   */
  public function getBundle($source = NULL, $reset = FALSE) {

    if(!isset($this->configRdf['Bundle']) || $reset) {

      $this->configRdf['Bundle'] = new ConfigRdfBundle(array(
        'source' => isset($source) ? $source : self::getSyncDir(),
      ));
    }

    return $this->configRdf['Bundle'];
  }

  /**
   * @return \Drupal\rdfxp_arc2\ConfigRdfFieldField
   */
  public function getFieldField($source = NULL, $reset = FALSE) {

    if(!isset($this->configRdf['FieldField']) || $reset) {

      $this->configRdf['FieldField'] = new ConfigRdfFieldField(array(
        'source' => isset($source) ? $source : self::getSyncDir(),
      ));
    }

    return $this->configRdf['FieldField'];
  }

  /**
   * @return \Drupal\rdfxp_arc2\ConfigRdfViewsView
   */
  public function getViewsView($source = NULL, $reset = FALSE) {

    if(!isset($this->configRdf['ViewsView']) || $reset) {

      $this->configRdf['ViewsView'] = new ConfigRdfViewsView(array(
        'source' => isset($source) ? $source : self::getSyncDir(),
      ));
    }

    return $this->configRdf['ViewsView'];
  }

  /**
   * @return \Drupal\rdfxp_arc2\ConfigRdfViewsViewDisplay
   */
  public function getViewsViewDisplay() {
	return $this->configRdf['ViewsView']->coreConfigGetDsiplay();

    $this->configRdf['ViewsViewDisplay'] = $this->configRdf['ViewsView']->coreConfigGetDsiplay();

    return $this->configRdf['ViewsViewDisplay'];
  }

  /**
   * @return \Drupal\rdfxp_arc2\ConfigRdfViewsViewDisplay
   */
  public function getViewsViewRelationship() {

    // TODO: #normal use $parents for cascades
    return $this->configRdf['ViewsView']->coreConfigGetDsiplay()->coreConfigGetRelationship();
  }

  /**
   * @return \Drupal\rdfxp_arc2\ConfigRdfMode
   */
  public function getViewMode($source = NULL, $reset = FALSE) {

    if(!isset($this->configRdf['ViewMode']) || $reset) {
      $this->configRdf['ViewMode'] = new ConfigRdfMode(array(
        'componentType' => 'ViewMode',
        'configPrefix' => 'core.entity_view_mode.',
        'source' => isset($source) ? $source : self::getSyncDir(),
      ));
    }

    return $this->configRdf['ViewMode'];
  }

  /**
   * @return \Drupal\rdfxp_arc2\ConfigRdfMode
   */
  public function getFormMode($source = NULL, $reset = FALSE) {

    if(!isset($this->configRdf['FormMode']) || $reset) {
      $this->configRdf['FormMode'] = new ConfigRdfMode(array(
        'componentType' => 'FormMode',
        'configPrefix' => 'core.entity_form_mode.',
        'source' => isset($source) ? $source : self::getSyncDir(),
      ));
    }

    return $this->configRdf['FormMode'];
  }

  /**
   * @return \Drupal\rdfxp_arc2\ConfigRdfViewDisplay
   *
   * @todo: #normal There are no content field in ViewDisplay node.*.rss but they
   *   are in content hidden
   */
  public function getViewDisplay($source = NULL, $reset = FALSE) {

    if(!isset($this->configRdf['ViewDisplay']) || $reset) {

      $this->configRdf['ViewDisplay'] = new ConfigRdfViewDisplay(array(
        'source' => isset($source) ? $source : self::getSyncDir(),
      ));
    }

    return $this->configRdf['ViewDisplay'];
  }

  /**
   * @return \Drupal\rdfxp_arc2\ConfigRdfFormDisplay
   */
  public function getFormDisplay($source = NULL, $reset = FALSE) {

    if(!isset($this->configRdf['FormDisplay']) || $reset) {

      $this->configRdf['FormDisplay'] = new ConfigRdfFormDisplay(array(
        'source' => isset($source) ? $source : self::getSyncDir(),
      ));
    }
    return $this->configRdf['FormDisplay'];
  }

  /**
   * @return \Drupal\rdfxp_arc2\ConfigRdfComponent
   */
  public function getEntityQueue($source = NULL, $reset = FALSE) {

    if(!isset($this->configRdf['EntityQueue']) || $reset) {

      $this->configRdf['EntityQueue'] = new ConfigRdfComponent(array(
        'componentType' => 'EntityQueue',
        'configPrefix' => 'entityqueue.entity_queue.',
        'source' => isset($source) ? $source : self::getSyncDir(),
      ));
    }
    return $this->configRdf['EntityQueue'];
  }

  public function getImageStyle($source = NULL, $reset = FALSE) {

    if(!isset($this->configRdf['ImageStyle']) || $reset) {
      $this->configRdf['ImageStyle'] = new ConfigRdfComponent(array(
        'componentType' => 'ImageStyle',
        'configPrefix' => 'image.style.',
        'source' => isset($source) ? $source : self::getSyncDir(),
      ));
    }
    return $this->configRdf['ImageStyle'];
  }

  public function getBlockBlock($source = NULL, $reset = FALSE) {

    if(!isset($this->configRdf['BlockBlock']) || $reset) {
      $this->configRdf['BlockBlock'] = new ConfigRdfComponent(array(
        'componentType' => 'BlockBlock',
        'configPrefix' => 'block.block.',
        'source' => isset($source) ? $source : self::getSyncDir(),
      ));
    }
    return $this->configRdf['BlockBlock'];
  }

  /**
   * @return \Drupal\rdfxp_arc2\ConfigRdfManager
   */
  public static function getManager() {
    return \Drupal::service('rdfxp_arc2.config_rdf_manager');
  }

  /**
   *  @todo #normal Get getComponentTypes from rdfxp-bundle-deps-vocab.rdf => subclassOf Component
   */
  public function getComponentTypes() {
    return array(
      'FieldStorage',
      'ViewMode',
      'FormMode',
      'EntityQueue',
      'Bundle',
      'FieldField',
      'ViewDisplay',
      'FormDisplay',
      'ViewsView',
      'ImageStyle',
      'BlockBlock',
    );
  }

  public function getComponentTypePrefixes() {
    if(isset($this->getComponentTypePrefixes)) {
      return $this->getComponentTypePrefixes;
    }
    foreach ($this->getComponentTypes() as $component_type) {
    $component = $this->{'get' . $component_type}();
      if(isset($component->configPrefix)) {
        $this->getComponentTypePrefixes[$component->configPrefix] = $component_type;
      }
    }

    return $this->getComponentTypePrefixes;
  }

  /**
   *  @todo #normal Get getComponentParts from rdfxp-bundle-deps-vocab.rdf => subclassOf ComponentPart
   */
  public function getComponentParts() {
    return array(
      'ViewsViewDisplay',
      'ViewsViewRelationship',
    );
  }
}