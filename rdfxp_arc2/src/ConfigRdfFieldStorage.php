<?php
/**
 * @file
 * Contains \Drupal\rdfxp_arc2\ConfigRdfFieldStorage.
 */

namespace Drupal\rdfxp_arc2;


class ConfigRdfFieldStorage extends ConfigRdfComponent {

  public $componentType = 'FieldStorage';

  public $configPrefix = 'field.storage.';
  
  public $dataPropertyMap = array(
    'translatable' => 'translatable',
  );

  public function addToTriples(&$triples) {
    parent::addToTriples($triples);

    $config_name = $this->configName();

    // Missing from vocab mappings
    $association_end_cardinality = 'cardinality-association-end';
    $triples[$config_name][$association_end_cardinality][] = $this->coreConfigGet('cardinality');
  }
}