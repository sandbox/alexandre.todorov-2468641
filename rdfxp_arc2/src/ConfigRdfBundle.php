<?php
/**
 * @file
 * Contains \Drupal\rdfxp_arc2\ConfigRdfBundle.
 */

namespace Drupal\rdfxp_arc2;

class ConfigRdfBundle extends ConfigRdfComponent {

  public $componentType = 'Bundle';

  public $findOptions = array(
    'regextype' => 'posix-extended',
    'regex' => '\'^[^.]+\.(type|bundle|paragraphs_type|vocabulary)\.[^.]+\.yml\'',
  );

  // ========= Custom
  public function configId($config_name) {
    $bundle_parts = explode('.', $config_name);

    $entity_key = $bundle_parts[0];

    $entity_id = $entity_key == 'taxonomy'? 'taxonomy_term' : $entity_key;
    $entity_id = $entity_key == 'media_entity'? 'media' : $entity_id;
    $entity_id = $entity_key == 'paragraphs'? 'paragraph' : $entity_id;

    switch($entity_id) {
      case 'media':
        $bundle_key = 'bundle';
        break;
      case 'paragraph':
        $bundle_key = 'paragraphs_type';
        break;
      // TODO: #review: To verify $bundle_key and $entity_key for 'taxonomy_term'
      case 'taxonomy_term':
        $bundle_key = 'vocabulary';
        break;
      // 'node', 'block_content', 'file':
      default:
        $bundle_key = 'type';
    }

    $config_id = $entity_id . '.' . $bundle_parts[2];
    $this->config[$config_id]['entityId'] = $entity_id;
    $this->config[$config_id]['entityKey'] = $entity_key;
    $this->config[$config_id]['bundleKey'] = $bundle_key;
    $this->config[$config_id]['bundleName'] = $bundle_parts[2];

    return $config_id;
  }

  public function configName($config_id = NULL) {
    if(!isset($config_id)) {
      $config_id = $this->currentConfigId;
    }

    // TODO: Undefined index: custom_entity.custom_entity, user.user, file.image, file.video
    return $this->config[$config_id]['entityKey']
      . '.' . $this->config[$config_id]['bundleKey']
      . '.' . $this->config[$config_id]['bundleName'];
  }

  public function coreConfigGetEntityName() {
    return 'entity.' . $this->config[$this->currentConfigId]['entityId'];
  }

  public function addToTriples(&$triples) {
    parent::addToTriples($triples);

    $config_name = $this->configName();
    $entity_id = $this->coreConfigGetEntityName();

    $triples[$config_name]['rdf:type']['Class'] = 'Class';
    $triples[$config_name]['extends'][$entity_id] = $entity_id;
  }
}