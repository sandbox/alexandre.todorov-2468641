<?php
/**
 * @file
 * Contains \Drupal\rdfxp_arc2\ConfigRdfViewsView.
 */

namespace Drupal\rdfxp_arc2;

use Drupal\views\Entity\View;

/**
 * @todo Add entity dependency. Ex for entity.node => base_table: node_field_data
 */
class ConfigRdfViewsView extends ConfigRdfComponent {


  public $componentType = 'ViewsView';

  public $configPrefix = 'views.view.';

  /**
   *
   * @var \Drupal\rdfxp_arc2\ConfigRdfViewsViewDisplay[]
   */
  public $display;

  /**
   * ViewsViewDisplay is a component part of $this so coreConfigGetDsiplay() is needed
   * ViewsViewDisplay decores $this
   *
   * @return \Drupal\rdfxp_arc2\ConfigRdfViewsViewDisplay[]
   */
  public function coreConfigGetDsiplay() {
    if(!isset($this->display[$this->currentConfigId])) {
      $this->display[$this->currentConfigId] = new ConfigRdfViewsViewDisplay($this);
    }
    return $this->display[$this->currentConfigId];
  }

  public function coreConfigGetViewsViewDisplayName(ConfigRdfViewsViewDisplay $display) {
    return parent::coreConfigGetConfigName($display);
  }
}