<?php

use Drupal\rdfxp_arc2\ConfigRdfManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\rdfxp_arc2\DependencyAware;

function rdfxp_arc2_drush_command() {

  $items['rdfxp-fr-deps'] = array(
    'arguments' => array(
      'feature' => 'A space delimited list of features. Currently module configs only in modules/custom. When no arguments (fra) all modules in module/custom',
    ),
    'options' => array(
      'keep-triples' => "Whether to reset config triples or not. It is a little time consuming. default is false.",
      'verbose' => "",
      'bundle' => "This option assumes that we are using features. Prefixes all arguments with {--bundle}_. When no arguments (fra), limit to bundle and active modules.",
      'dryrun' => "No action is done.",
      'config-rdf' => "Collects config data and writes them to 'public://files/rdf/rdfxp-config-auto.rdf'",
      'model-rdf' => "Use Swoop model for sparql query. ex. --model-rdf=files__rdf__rdfxp-config-auto",
      'source' => "Modules config source directory relative to Drupal::root(). Default is: if --bundle then 'modules/custom' else \$config_directories[\'sync\']",
    ),
    'aliases' => array('rdfxp-fr'),
  );
  $items['rdfxp-deps-aware'] = array(
    'options' => array(
      'rev' => 'If it is rev deps aware',
      'model-rdf' => "Use Swoop model for sparql query. ex. --model-rdf=files__rdf__rdfxp-config-auto",
    ),
    'aliases' => array('rdfxp-deps'),
  );
  return $items;
}

/**
 * @todo #normal: refactor with drush_rdfxp_arc2_rdfxp_fr_deps()
 */
function drush_rdfxp_arc2_rdfxp_deps_aware() {

  $is_rev = (bool)drush_get_option('rev');

  // Parse arguments to obtain:
  // array(
  //   'Entity' => array(
  //     'entity.node' => 'entity.node',
  //   ),
  // );
  $args = func_get_args();
  $types = array();
  foreach ($args as $arg) {
    list($type, $component) = explode(':', $arg);
    $types[$type][$component] = $component;
  }
  $prev = $types;

  if($model_id = drush_get_option('model-rdf')) {

    $deps_aware = new DependencyAware($model_id);

    // Arc store set time limit to 60 so set it unlimited
    set_time_limit(0);

    $prev = $deps_aware->get($types, $is_rev);
    foreach ($deps_aware->getDependenciesAppended() as $deps_appended) {
      rdfxp_krumo($deps_appended);
    }
    rdfxp_krumo($prev);
  }
  else {

    $data_store_name = 'bundle_data_store_loaded';
    $store = rdfxp_arc2_init_store($data_store_name);
    $store->reset();
    $filename = rdfxp_arc2_file_create_url('public://files/rdf/rdfxp-config-auto.rdf');
    $store->query('LOAD <' . $filename . '>');

    $name_when_created = 'bundle_data_store';
    $store->rdfxp_ns0 = rdfxp_arc2_file_create_url($name_when_created) . '#';

    $deps_aware = new DependencyAware($store);

    // Arc store set time limit to 60 so set it unlimited
    set_time_limit(0);

    $prev = $deps_aware->get($types, $is_rev);
    foreach ($deps_aware->getDependenciesAppended() as $deps_appended) {
      rdfxp_krumo($deps_appended);
    }
    rdfxp_krumo($prev);
  }

  $end = rdfxp_microtime_float();

  rdfxp_krumo('Total time: ' . ($end - $start));

  // Create subset model keeping the same current prefix "ns0"
  $model_id = 'files__rdf__rdfxp-config-auto';
  $ont_model = rdfxp_get_model($model_id);
  $namespace_prefix = \Drupal::state()->get('rdfxp.model_prefix', 'ns0');
  $namespace = array_search($namespace_prefix, $ont_model->model->parsedNamespaces);

  $subset_model = SwoopModel::getSwoopModelForBaseModel(ModelFactory::getMemModel(),OWL_VOCABULARY);
  $subset_model->modelId('files__rdf__rdfxp-config-subset');

  foreach ($prev as $type => $names) {
    foreach ($names as $name => $no_matter) {
      $s = new ResProperty($namespace . $name);
      $statements = $ont_model->find($s, null, null);
      foreach ($statements as $statement) {
        // TODO: #critical: Use $statement->getPredicate()->getUri(); For object handle when litteral
        // TODO: #critical: $statement->getLabelObject() is not suitable for image_settings literal
        rdfxp_add_unique_statement($subset_model, $namespace . $name, $statement->getLabelPredicate(), $statement->getLabelObject());
      }
    }
  }
  // TODO: #critical: look for <![CDATA[ => why are empty eg. node.type.article
  $filename = 'public://files/rdf/rdfxp-config-subset.rdf';
  $subset_model->saveAs($filename, "rdf");
  // Change ns from ns1 to ns0 and write to rdf directory
  // TODO: #normal: handle implicitly added namespaces => RAP add ns1, ARC2 add ns0
  file_put_contents($filename, str_replace(array('ns1:', 'ns1='), array('ns0:', 'ns0='), file_get_contents($filename)));
  drush_log(t('Subset rdf file written to @file', array('@file' => $filename)), 'success');

  $subset = rdfxp_microtime_float();
  rdfxp_krumo('Subset time: ' . ($subset - $end));

}

/**
 * @deprecated
 * @todo #normal: Refactor with drush_rdfxp_arc2_rdfxp_deps_aware()
 */
function drush_rdfxp_arc2_rdfxp_fr_deps() {
  // Check if the parent rdfxp module is enabled
  // For functions like rdfxp_krumo() or rdfxp_microtime_float()
  if(!Drupal::moduleHandler()->moduleExists('rdfxp')) {
    require_once drupal_get_path('module', 'rdfxp') . '/rdfxp.module';
  }

  $keep_triples = drush_get_option('keep-triples');
  $bundle = drush_get_option('bundle');
  $dryrun = drush_get_option('dryrun');

  $config_factory = \Drupal::config('core.extension');
  $core_extension_modules = $config_factory->get('module');

  $modules_customs = array();
  $args = func_get_args();
  // TODO: needs work: $modules_customs when dealing with features
  if($bundle && !$args) {
    foreach ($core_extension_modules as $module => $weight) {
      // Add the module (from active modules) if it belongs to the bundle argument
      if(strpos($module, $bundle . '_') === 0) {
        $modules_customs['Module']['module.' . $module] = 'enabled';
      }
    }
  }
  else {
    foreach ($args as $module) {

      if($bundle && strpos($module, $bundle . '_') !== 0) {
        $module = $bundle . '_' . $module;
      }

      if($bundle) {
        $modules_customs['Module']['module.' . $module] = isset($core_extension_modules[$module]) ? 'enabled' : $module;
      }
      else {
        list($type, $component) = explode(':', $module);
        $modules_customs[$type][$component] = $component;
      }
    }
  }

  // Some override for $_SERVER variable needed for ARC2 local resources
  // TODO: Find better way for this.
  $unset = array();
  $copy = array();
  foreach (array(
    'SERVER_PROTOCOL' => 'HTTP/1.1',
    // Actually not needed
    // 'REQUEST_SCHEME' => 'http',
  ) as $key => $value) {

    if(array_key_exists($key, $_SERVER)) {
      $copy[$key] = $_SERVER[$key];
    }
    else {
      $unset[$key] = $key;
    }
    $_SERVER[$key] = $value;
  }

  $start = rdfxp_microtime_float();

  // deps graph
  list($deps_store, $deps) = rdfxp_arc2_bundle_deps_store();
  $graph_deps_end = rdfxp_microtime_float();
  rdfxp_krumo('$graph_deps_end: ' . ($graph_deps_end - $start));

  if(!($source = drush_get_option('source'))) {
    // if --bundle then config with features is assumed
    if($bundle) {
      $source = 'modules/custom';
    }
    else {
      $source = ConfigRdfManager::getSyncDir();
    }
  }

  $cid = 'rdfxp_arc2.config_rdf_optional';
  $cache = \Drupal::cache()->get($cid);
  // option --config-rdf needs triples to work
  // TODO: needs work for --config-rdf and $triples['optional']
  if(!$keep_triples || drush_get_option('config-rdf')) {
    // config to triples
    $triples = rdfxp_arc2_bundle_config_to_triples($source);

    // Save $triples['optional'] after drush cr and without option --keep-triples
    if (!$cache) {
      \Drupal::cache()->set($cid, $triples['optional']);
    }
  }
  if ($cache) {
    $triples['optional'] = $cache->data;
  }
  $triples['optional'] = array(
    'rdf:type' => array('Optional' => 'Optional')
  ) + $triples['optional'];

  $triples_end = rdfxp_microtime_float();
  rdfxp_krumo('$triples_end: ' . ($triples_end - $graph_deps_end));

  //rdfxp_krumo($triples);
  $vocab_uri = 'public://files/rdf/rdfxp-bundle-deps-vocab.rdf';
  $data_store_name = 'bundle_data_store';
  if(drush_get_option('config-rdf')) {
    // vocab triples will be added to config triples in arc2 data store

    list($store, $graph) = rdfxp_arc2_triples_to_store($data_store_name, $deps, $triples, $vocab_uri, 'both', !$keep_triples);
    $graph_data_end = rdfxp_microtime_float();
    rdfxp_krumo('$graph_end: ' . ($graph_data_end - $triples_end));

    $rdf_content = $graph->serialise('rdf');
    $rdf_content = preg_replace('%' . $vocab_uri . '%', rdfxp_arc2_file_create_url($data_store_name), $rdf_content);
    file_put_contents($filename = 'public://files/rdf/rdfxp-config-auto.rdf', $rdf_content);
    drush_log(t('Config rdf file written to @file', array('@file' => $filename)), 'success');
    return ;
  }
  if($model_id = drush_get_option('model-rdf')) {

    // $ontModel;
    $store = new DependencyAware($model_id);
  }
  else {
    // Example when using a vocabulary:
    // list($store, $graph) = rdfxp_arc2_triples_to_store('bundle_data_store', $deps, $triples, 'public://files/rdf/rdfxp-bundle-vocab.rdf', 'both', !$keep_triples);
    $store = rdfxp_arc2_triples_to_store($data_store_name, $deps, $triples, $vocab_uri, 'store', !$keep_triples);
    $graph_data_end = rdfxp_microtime_float();
    rdfxp_krumo('$store_end: ' . ($graph_data_end - $triples_end));

    $store = new DependencyAware($store);
  }

  // TODO: Add to test cases.
  // Testing data graph
//   list($graph, $store) = rdfxp_arc2_config_graph_test1();
//   $graph_data_end = rdfxp_microtime_float();
//   rdfxp_krumo('$graph_data_end: ' . ($graph_data_end - $triples_end));

  $types = array(
    // comment next line for "all"
    // 'Module' => array('mod1' => 'enabled'),
  );
  $types = $modules_customs;
  // Don't inclure starting $types in $missing
  // fra equals to all(actives or in .profile) modules
  $prev = $types;
  $missing_modules = array();

  // Arc store set time limit to 60 so set it unlimited
  set_time_limit(0);

  // TODO: Get $look_for_items from rdfxp-bundle-deps-vocab.rdf
  foreach ($look_for_items = array(
      'FieldStorage', 'Bundle', 'FormMode', 'ViewMode',
      'FieldField',
      'FormDisplay', 'ViewDisplay',
      'EntityQueue',
      'ViewsView',

      // No matter
      // 'ViewsViewDisplay',
    ) as $look_for) {

      // TODO: #deprecated: see drush_rdfxp_arc2_rdfxp_deps_aware(): For a component get only his deps (rel)
      // TODO: #deprecated: see drush_rdfxp_arc2_rdfxp_deps_aware(): For entity or module get components that depends on (rev).
      //  When property is optional (ex. entity or module), all components are selected
      if(!isset($types[$look_for]) && !isset($types['Entity']) && !isset($types['Module'])) {
        continue;
      }

    $missing = $store->getMissing($look_for, $types, $prev);

    // Log (already in $prev) but not stores missing modules
    if(isset($missing['Module'])) {
      unset($missing['Module']);
    }
    rdfxp_krumo(/*$prev, */$missing);

    if(count($missing)) {
      $store->getMissingDeep($prev, $missing);
    }
  }


  // Nothing to do, but display result if not dealing with features
  if(!$bundle) {
    rdfxp_krumo($prev);
    return;
  }
  // TODO: Find a better way
  // TODO: compare with getDependanciesAppended
  $components = array();
  $added = array();
  foreach ($prev as $type => $type_components) {
    foreach ($type_components as $component => $module_or_added) {
      if($component == $module_or_added) {
        $added[$type][$component] = $component;
        if($type != 'Module') {
          $components[$type][$component] = $component;
        }
      }
      else if($type != 'Module') {
        $components[$type][$component] = $module_or_added . ':' . $component;
      }
    }
  }
  $to_enable = $no_modules_for = array();
  foreach ($added as $type => $type_components) {
    if($type == 'Module') {
      foreach ($type_components as $module => $no_matter) {
        if(!isset($core_extension_modules[$module])) {
          $to_enable[] = $module;
        }
      }
    }
    else {
      $rdfxp_arc2_deps_item = $store->getDependencies($type, array($type => $type_components));
      foreach ($type_components as $added_component => $same) {
        if(isset($rdfxp_arc2_deps_item[$type][$added_component])) {
          $components[$type][$added_component] = $module . ':' . $added_component;
        }
        else {
          $no_modules_for[$type][$added_component] = $components[$type][$added_component];
          unset($components[$type][$added_component]);
        }
      }
    }
  }

  // TODO: $added may have not always deps detected components
  rdfxp_krumo($components, $added, $to_enable, $no_modules_for);

  // TODO: tested only in multisite with $aliases['my_site'] = ... and $sites['site_url'] = 'my_site'
  $drush_alias = '';
  $core_st = drush_core_status();
  if(isset($core_st['drush-alias-files'])) {
    foreach ($core_st['drush-alias-files'] as $alias_file) {
      include $alias_file;
    }

    $alias = substr($core_st['site'], strrpos($core_st['site'], '/')+1);
    if(isset($aliases[$alias])) {
      $drush_alias = '@' . $alias;
    }
  }

  if($to_enable) {
    echo "\n# Modules to enable first: " . implode(', ', $to_enable) . "\n";
    echo "\ndrush " . $drush_alias . " en -y " . implode(' ', $to_enable) . "\n";
  }

  $chunk_size = 10;
  $type_prev = '';
  $i = $chunk = $missing_components_chunk = $missing_components = array();
  // Check if config is missing from active config
  foreach ($components as $type => $type_components) {
    foreach ($type_components as $config_name => $component) {
      if(!($config = \Drupal::config($config_name)->get())) {
        // Init vars for first or next type.
        if(!isset($i[$type])) {
          $i[$type] = 0;
        }
        if(!isset($chunk[$type])) {
          $chunk[$type] = array();
        }
        if(!$type_prev) {
          $type_prev = $type;
        }

        $missing_components[$config_name] = $chunk[$type][$config_name] = preg_replace('/^module\./', '', $component, 1);
        $i[$type]++;

        // There are some from prev $type
        if($type_prev != $type && count($chunk[$type_prev])) {
          $missing_components_chunk[] = $chunk[$type_prev];
        }

        // If chunk size is reached then write current and start new chunk
        if($i[$type] > $chunk_size-1) {
          $missing_components_chunk[] = $chunk[$type];
          $chunk[$type] = array();
          $i[$type] = 0;
        }

        $missing_components[$config_name] = preg_replace('/^module\./', '', $component, 1);

        // Backup $type value.
        $type_prev = $type;
      }
    }
  }

  // There some from last $type.
  if(count($chunk[$type_prev])) {
    $missing_components_chunk[] = $chunk[$type_prev];
  }

  // rdfxp_krumo($missing_components);
  if($missing_components_chunk) {
    echo "\n# missing components: \n";
    $separator = ' \\' . "\n" . '  ';
    foreach ($missing_components_chunk as $chunk) {

      if(!$dryrun) {
        call_user_func_array('drush_features_import', $chunk);
      }
      else {
        echo "\ndrush " . $drush_alias . " fr -y " . $separator . implode($separator, $chunk);
      }

      $missing_components = array_diff_key($missing_components, $chunk);
    }
    echo "\n";
  }
  // Verify that chunk transform is working.
  rdfxp_krumo($missing_components);

  $modules_to_fr = $prev['Module'];
  $drush_cmd_prefix = "\ndrush " . $drush_alias . " fr -y --bundle=" . $bundle;

  // Features modules to revert.
  $echo = "\n\n# You can now revert all:\n";
  foreach ($modules_to_fr as $module => $none) {

    $module = preg_replace('/^module\./', '', $module, 1);

    $module_info_file = drupal_get_path('module', $module) . '/' . $module . '.info.yml';
    $module_info = Symfony\Component\Yaml\Yaml::parse(file_get_contents($module_info_file));
    // Only features modules
    if(!empty($module_info['features'])) {
      $echo .= $drush_cmd_prefix . ' ' . str_replace($bundle . '_', '', $module);
    }
  }
  echo $echo;

  // Modules in active profile that are not yet enabled.
  $profile_name = drupal_get_profile();
  $profile_info_file = drupal_get_path('profile', $profile_name) . '/' . $profile_name . '.info.yml';
  $profile_info = Symfony\Component\Yaml\Yaml::parse(file_get_contents($profile_info_file));
  $echo = '';
  foreach ($profile_info['dependencies'] as $module) {
    if(!Drupal::moduleHandler()->moduleExists($module)) {
      $echo .= ($echo? ',' : 'Modules in active profile "' . $profile_name . '" that are not yet enabled:' . "\n") . $module;
    }
  }
  if($echo) { echo "\n\n" . $echo . "\n"; }

  if($no_modules_for) {
    echo "\n\n# Missing module info for the following component (maybe some dependancies\n" .
      "are not exported in modules/custom - ex. field.storage.node.body which is comming\n" .
      "from the install profile) : \n";
      var_dump($no_modules_for);
  }

  $end_end = rdfxp_microtime_float();
  rdfxp_krumo('$end_end: ' . ($end_end - $graph_data_end));

  // Restore original values to $_SERVER
  foreach ($unset as $key) {
    unset($_SERVER[$key]);
  }
  foreach ($copy as $key => $value) {
    $_SERVER[$key] = $value;
  }
}

/**
 * @param string $source_dir Witout trailing slash
 */
function rdfxp_arc2_bundle_config_to_triples($source_dir = 'modules/custom') {

  $triples = array();
  $configRdf = ConfigRdfManager::getManager();

  \Drupal\Core\Cache\Cache::invalidateTags(array(
//     'config_rdf_type:FieldStorage',
//     'config_rdf_type:ViewMode',
//     'config_rdf_type:FormMode',
//     'config_rdf_type:EntityQueue',
//     'config_rdf_type:Bundle',
//     'config_rdf_type:FieldField',
//     'config_rdf_type:ViewDisplay',
//     'config_rdf_type:FormDisplay',
//     'config_rdf_type:ViewsView',
//     'config_rdf_type:ViewsViewDisplay',
//     'config_rdf_type:ViewsViewRelationship'
  ));

  foreach ($configRdf->getComponentTypes() as $componentType) {

    $component = $configRdf->{'get' . $componentType}($source_dir);
    $component->allToTriples($triples);
  }

  // ViewsView component parts.
  // TODO #normal: Bad for performance: second loop of same component. Previous is (ViewsView): $component->allToTriples($triples);
  foreach ($viewView = $configRdf->getViewsView() as $config_view_id => $config_view) {
    $viewViewDisplay = $viewView->coreConfigGetDsiplay();
    $viewViewDisplay->allToTriples($triples);

    // Bad for performance: second loop of same component. Previous is: $viewViewDisplay->allToTriples($triples);
    foreach ($viewViewDisplay as $config_view_display_id => $config_view_display) {
      $viewViewRelationship = $viewViewDisplay->coreConfigGetRelationship();
      $viewViewRelationship->allToTriples($triples);
    }
  }

  return $triples;
}

/**
 * @todo rename ..._test1 by something named
 *
 * @return EasyRdf_Graph
 */
function rdfxp_arc2_config_graph_test1 () {

  $triples = array();
  $triples['mod1'] = array(
    'rdf:type' => array('Module' => 'Module'),
  );
  $triples['mod2'] = array(
    'rdf:type' => array('Module' => 'Module'),
  );
  $triples['mod3'] = array(
    'rdf:type' => array('Module' => 'Module'),
  );
  // s1: mod1
  $triples['s1'] = array(
    'rdf:type' => array('Storage' => 'Storage'),
    'module' => array('mod1' => 'mod1'),
  );
  // s2: mod1
  $triples['s2'] = $triples['s1'];
  // s3: mod2
  $triples['s3'] = array(
    'rdf:type' => array('Storage' => 'Storage'),
    'module' => array('mod2' => 'mod2'),
  );
  // b2: mod3
  $triples['b2'] = array(
    'rdf:type' => array('Bundle' => 'Bundle'),
    'module' => array('mod3' => 'mod3'),
  );
  // b3: mod3
  $triples['b3'] = array(
    'rdf:type' => array('Bundle' => 'Bundle'),
    'module' => array('mod3' => 'mod3'),
  );
  // f4: mod3 - Not shown because no deps for mod1 (or active mods)
  $triples['f4'] = array(
    'rdf:type' => array('Field' => 'Field'),
    'module' => array('mod3' => 'mod3'),
    'bundle' => array('b2' => 'b2'),
    'storage' => array('s4' => 's4'),
  );
  // f4: mod3 - Not shown because no deps for mod1 (or active mods)
  $triples['f5'] = array(
    'rdf:type' => array('Field' => 'Field'),
    'module' => array('mod2' => 'mod2'),
    'bundle' => array('b3' => 'b3'),
    'storage' => array('s2' => 's2'),
  );
  // s3: mod2
  $triples['s4'] = array(
    'rdf:type' => array('Storage' => 'Storage'),
    'module' => array('mod3' => 'mod3'),
  );
  // b1: mod1
  $triples['b1'] = array(
    'rdf:type' => array('Bundle' => 'Bundle'),
    'module' => array('mod1' => 'mod1'),
  );
  // f1: s1, b1, mod1
  $triples['f1'] = array(
    'rdf:type' => array('Field' => 'Field'),
    'module' => array('mod1' => 'mod1'),
    'storage' => array('s1' => 's1'),
    'bundle' => array('b1' => 'b1'),
  );
  // f2: s2, b1, mod1
  $triples['f2'] = array(
    'rdf:type' => array('Field' => 'Field'),
    'module' => array('mod1' => 'mod1'),
    'storage' => array('s2' => 's2'),
    'bundle' => array('b1' => 'b1'),
  );
  // f3: s3, b1, mod1
  $triples['f3'] = array(
    'rdf:type' => array('Field' => 'Field'),
    'module' => array('mod1' => 'mod1'),
    'storage' => array('s3' => 's3'),
    'bundle' => array('b1' => 'b1', 'b3' => 'b3'),
  );
  // m1: mod1
  $triples['m1'] = array(
    'rdf:type' => array('ViewMode' => 'ViewMode'),
    'module' => array('mod1' => 'mod1'),
  );
  // m1: mod1
  $triples['m2'] = array(
    'rdf:type' => array('ViewMode' => 'ViewMode'),
    'module' => array('mod1' => 'mod1'),
  );
  // f3: s3, b1, mod1
  $triples['d3'] = array(
    'rdf:type' => array('ViewDisplay' => 'ViewDisplay'),
    'module' => array('mod1' => 'mod1'),
    'field' => array('f3' => 'f3', 'f4' => 'f4', /*'f5' => 'f5'*/),
    'bundle' => array('b2' => 'b2'),
    'viewMode' => array('m2' => 'm2'),
  );


  $graph = new EasyRdf_Graph();

  $name = 'bundle_graph_test1';
  $store = rdfxp_arc2_init_store($name);
  $store->reset();
//   krumo($store->);
  //$result = $store->query('LOAD ' . $graph->serialise('turtle') . '', 'raw');

  $i = 0;
  $max = 6;
  $dot = 'digraph g {' . "\n";
  foreach ($triples as $s => $po) {

    $subject = $graph->resource('#' . $s);
    $graph->addType($subject, '#' . reset($po['rdf:type']));

    if(isset($po['rdf:type'])) {
      $dot .= '"' . $s . '" [ label="' . $s . ' : ' .reset($po['rdf:type']) . '"]';
    }
    $res = $store->query('INSERT INTO <http://example.com/> {
     <#' . $s . '> <rdf:type> <#' . reset($po['rdf:type']) . '> .
    }', 'raw');

    unset($po['rdf:type']);

    foreach ($po as $p => $os) {
      foreach ($os as $o => $same) {
        $graph->add($subject, '#' . $p, '#' . $o);
        $res = $store->query('INSERT INTO <http://example.com/> {
         <#' . $s . '> <#' . $p . '> <#' . $o . '> .
        }', 'raw');

        $dot .= '"' . $s . '" -> "' . $o . '" [arrowhead="normal" label="' . $p . '"]' . "\n";
      }
    }
  }
  $dot .= '}';
  file_put_contents('/tmp/' . $name . '.dot', $dot);

  return array($graph, $store);
}

function rdfxp_arc2_init_store($name) {

  $connection = \Drupal\Core\Database\Database::getConnection();
  $db_spec = $connection->getConnectionOptions();
  $config = array(
    /* db */
    'db_name' => $db_spec['database'],
    'db_user' => $db_spec['username'],
    'db_pwd' => isset($db_spec['password']) ? $db_spec['password'] : '',
    /* store */
    'store_name' => $name,

    'max_errors' => 100,
  );

  $store = ARC2::getStore($config);
  if (!$store->isSetUp()) {
    $store->setUp();
  }

  return $store;
}

function rdfxp_arc2_file_create_url($stream_uri) {

  // Using web
  if(!function_exists('drush_main')) {
    $filename = file_create_url($stream_uri);
  }
  // Using drush with "uri" option.
  else if($uri = drush_get_option('uri')) {
    $filename = file_create_url($stream_uri);
  }
  // TODO: #deprecated: add fallback if there is a multi site declaration to guess the uri
  else {
    drush_log(t('"--uri" option is mandatory for ARC2 library when using drush. You can also add it in sites/default/drushrc.php: $options[\'uri\'] = \'http://example.com\''), 'error');
    exit(1);

    include 'sites/sites.php';
    $drush_url = $site_url = '';

    $core_st = drush_core_status();
    $site = substr($core_st['site'], strrpos($core_st['site'], '/')+1);

    if(($site_url = array_search($site, $sites)) !== FALSE) {
      $drush_url = 'http://' . $site . '/';
    }
    $filename = str_replace($drush_url, 'http://' . $site_url . '/', file_create_url($stream_uri));
  }

  return $filename;
}

function rdfxp_arc2_bundle_deps_store() {
  $name = 'bundle_deps_vocab';
  $store = rdfxp_arc2_init_store($name);
  $store->reset();

  $filename = rdfxp_arc2_file_create_url('public://files/rdf/rdfxp-bundle-deps-vocab.rdf');

  $store->query('LOAD <' . $filename . '>');

  // domainOf {obj props} query
  $q = 'SELECT ?class ?prop ?dep WHERE {
    ?class rdf:type owl:Class .
    ?prop rdfs:domain ?class .
    ?prop rdf:type owl:ObjectProperty .
    ?prop rdfs:range ?dep
  }';

  $dot_dep = 'digraph g {' . "\n";
  $deps = array();
  if ($rows = $store->query($q, 'rows')) {
    foreach ($rows as $row) {
      $dot_dep .= '"' . $row['class'] . '" [label="' . ($class_localname = substr($row['class'], strrpos($row['class'], '#')+1)) . '"]' . "\n";
      $prop_localname = substr($row['prop'], strrpos($row['prop'], '#')+1);
      $dot_dep .= '"' . $row['dep'] . '" [label="' . ($dep_localname = substr($row['dep'], strrpos($row['dep'], '#')+1)) . '"]' . "\n";
      $deps[$class_localname][$prop_localname] = $dep_localname;

      $dot_dep .= '"' . $row['dep'] . '" -> "' . $row['class'] . '" [arrowhead="normal"]' . "\n";

    }
  }
  $dot_dep .= '}';

  return array($store, $deps);
}

/**
 * @param $name
 * @param $deps needed for triple properties.
 */
function rdfxp_arc2_triples_to_store($name, $deps, $triples, $vocab_uri = '', $return_as = 'grapĥ', $reset = TRUE) {


  if($return_as == 'store' || $return_as == 'both') {
    $store = rdfxp_arc2_init_store($name);
    if(!$reset) {
      rdfxp_krumo("Just init - no reset - using old values");
      return $store;
    }
    $store->reset();
  }

  if($return_as == 'graph' || $return_as == 'both') {
    if($vocab_uri) {
      $graph = EasyRdf_Graph::newAndLoad(rdfxp_arc2_file_create_url($vocab_uri));
    }
    else {
      $graph = new EasyRdf_Graph();
    }
  }

  $ns = rdfxp_arc2_file_create_url($name);
  $i = 0;
  $max = 6;
  $dot = 'digraph g {' . "\n";
  foreach ($triples as $s => $po) {

    // Add types (rdf:type)
    if(isset($po['rdf:type'])) {

      foreach ($po['rdf:type'] as $type => $same) {
        if($return_as == 'store' || $return_as == 'both') {
          $res = $store->query('INSERT INTO <' . $ns . '> {
           <#' . $s . '> <rdf:type> <#' . $type . '> .
          }', 'raw');
        }

        if($return_as == 'graph' || $return_as == 'both') {
          $s_rsrc = $graph->resource($ns . '#' . $s, $ns . '#' . $type);
        }
      }

      $dot .= '"' . $s . '" [ label="' . $s . ' : ' .implode(', ', $po['rdf:type']) . '"]';

      unset($po['rdf:type']);
    }
    else {
      $s_rsrc = $graph->resource($ns . '#' . $s);
    }

    foreach ($po as $p => $os) {
      $p_uri = strpos($p, ':') !== FALSE? $p : $ns . '#' . $p;

      foreach ($os as $o_or_i => $o) {
        // $o is literal
        if(is_numeric($o_or_i)) {

          // TODO: #normal: Missing data type "http://www.w3.org/1999/02/22-rdf-syntax-ns#XMLLiteral"
          //   but <?xml at the begining
          // TODO: #normal: See also rdfxp_add_unique_statement()
          if(is_array($o)) {
            $o = \ArrayToSimpleXml::fromPhpCode('<?php $config = ' . var_export($o, true) . ';');
            $is_xml = TRUE;
          }
          else {
            $is_xml = FALSE;
            $o = (string)$o;
          }
          if($return_as == 'store' || $return_as == 'both') {
            // TODO: #major: To test sparql query with literals. Using this way it seems that literals are resources
            $res = $store->query('INSERT INTO <' . $ns . '> {
             <#' . $s . '> <#' . $p . '> ' . var_export($o, TRUE) . ' .
            }', 'raw');
          }
          if($return_as == 'graph' || $return_as == 'both') {
            try{
              if($is_xml) {
                $graph->addLiteral($s_rsrc, $p_uri, new \EasyRdf_Literal_XML($o));
              }
              else {
                $graph->addLiteral($s_rsrc, $p_uri, $o);
              }
            }
            catch (Exception $e) {
              rdfxp_krumo($e->getMessage(), $o);exit();
            }
          }
        }
        else {
          if($return_as == 'store' || $return_as == 'both') {
            $res = $store->query('INSERT INTO <' . $ns . '> {
             <#' . $s . '> <#' . $p . '> <#' . $o . '> .
            }', 'raw');
          }
          if($return_as == 'graph' || $return_as == 'both') {
            $o_rsrc = $graph->resource($ns . '#' . $o);
            $graph->add($s_rsrc, $p_uri, $o_rsrc);
          }
        }

        $dot .= '"' . $s . '" -> "' . $o . '" [arrowhead="normal" label="' . $p . '"]' . "\n";
      }
    }
  }
  $dot .= '}';
  file_put_contents('/tmp/' . $name . '.dot', $dot);

  return $return_as == 'both'? array($store, $graph) : ${$return_as};
}