<?php
$rdfxp__model__rdf_rdfxp_test__classes_tree__renderable = array (
  'subClasses' => array(
    '#theme' => 'item_list',
    '#title' => '/subClasses',
    '#items' => array(
      0 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/Ontology" resource="owl:Ontology">Ontology</a>',
      ),
      1 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/Class" resource="owl:Class">Class</a>',
      ),
      2 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/Datatype" resource="rdfs:Datatype">Datatype</a>',
      ),
      3 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/AnnotationProperty" resource="owl:AnnotationProperty">AnnotationProperty</a>',
      ),
      4 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/DatatypeProperty" resource="owl:DatatypeProperty">DatatypeProperty</a>',
      ),
      5 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/ObjectProperty" resource="owl:ObjectProperty">ObjectProperty</a>',
      ),
      6 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/Documentation" resource="rdfxp-test:Documentation">Documentation</a>',
        'children' => array(
          0 => array(
            'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/CallbackForWalk" resource="rdfxp-test:CallbackForWalk">CallbackForWalk</a>',
          ),
          1 => array(
            'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/SwoopOntologyComponent" resource="rdfxp-test:SwoopOntologyComponent">SwoopOntologyComponent</a>',
            'children' => array(
              0 => array(
                'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/SwoopClass" resource="rdfxp-test:SwoopClass">SwoopClass</a>',
              ),
              1 => array(
                'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/SwoopIndividual" resource="rdfxp-test:SwoopIndividual">SwoopIndividual</a>',
              ),
              2 => array(
                'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/SwoopOntology" resource="rdfxp-test:SwoopOntology">SwoopOntology</a>',
              ),
              3 => array(
                'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/SwoopProperty" resource="rdfxp-test:SwoopProperty">SwoopProperty</a>',
              ),
            ),
            'children_title' => '6/1/subClasses',
          ),
        ),
        'children_title' => '6/subClasses',
      ),
      7 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/DataModel" resource="rdfxp-test:DataModel">DataModel</a>',
        'children' => array(
          0 => array(
            'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/Bundle" resource="rdfxp-test:Bundle">Bundle</a>',
          ),
          1 => array(
            'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/Entity" resource="rdfxp-test:Entity">Entity</a>',
          ),
          2 => array(
            'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/FieldBase" resource="rdfxp-test:FieldBase">FieldBase</a>',
          ),
          3 => array(
            'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/FieldInstance" resource="rdfxp-test:FieldInstance">FieldInstance</a>',
            'children' => array(
              0 => array(
                'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/FieldInstanceViewMode" resource="rdfxp-test:FieldInstanceViewMode">FieldInstanceViewMode</a>',
              ),
            ),
            'children_title' => '7/3/subClasses',
          ),
        ),
        'children_title' => '7/subClasses',
      ),
      8 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/RdfxpModel" resource="rdfxp-test:RdfxpModel">RdfxpModel</a>',
        'children' => array(
          0 => array(
            'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/DrupalBlock" resource="rdfxp-test:DrupalBlock">DrupalBlock</a>',
            'children' => array(
              0 => array(
                'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/MenuBlock" resource="rdfxp-test:MenuBlock">MenuBlock</a>',
              ),
            ),
            'children_title' => '8/0/subClasses',
          ),
          1 => array(
            'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/DrupalForm" resource="rdfxp-test:DrupalForm">DrupalForm</a>',
          ),
          2 => array(
            'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/DrupalMenu" resource="rdfxp-test:DrupalMenu">DrupalMenu</a>',
            'children' => array(
              0 => array(
                'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/MenuBlockMenu" resource="rdfxp-test:MenuBlockMenu">MenuBlockMenu</a>',
              ),
            ),
            'children_title' => '8/2/subClasses',
          ),
          3 => array(
            'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/ElementWalk" resource="rdfxp-test:ElementWalk">ElementWalk</a>',
          ),
          4 => array(
            'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/StructureForWalk" resource="rdfxp-test:StructureForWalk">StructureForWalk</a>',
          ),
        ),
        'children_title' => '8/subClasses',
      ),
      9 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/PresentationModel" resource="rdfxp-test:PresentationModel">PresentationModel</a>',
        'children' => array(
          0 => array(
            'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/ViewMode" resource="rdfxp-test:ViewMode">ViewMode</a>',
          ),
        ),
        'children_title' => '9/subClasses',
      ),
    ),
  ),
);

$rdfxp__model__rdf_rdfxp_test__properties_tree__renderable = array (
  'subProperties' => array(
    '#theme' => 'item_list',
    '#title' => '/subProperties',
    '#items' => array(
      0 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/comment" resource="rdfs:comment">comment</a>',
      ),
      1 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/seeAlso" resource="rdfs:seeAlso">seeAlso</a>',
      ),
      2 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldCardinality" resource="rdfxp-test:fieldCardinality">fieldCardinality</a>',
      ),
      3 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldLabel" resource="rdfxp-test:fieldLabel">fieldLabel</a>',
      ),
      4 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldRequired" resource="rdfxp-test:fieldRequired">fieldRequired</a>',
      ),
      5 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldTranslatable" resource="rdfxp-test:fieldTranslatable">fieldTranslatable</a>',
      ),
      6 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldViewModeSettings" resource="rdfxp-test:fieldViewModeSettings">fieldViewModeSettings</a>',
      ),
      7 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/walkChildrenLevelName" resource="rdfxp-test:walkChildrenLevelName">walkChildrenLevelName</a>',
        'children' => array(
          0 => array(
            'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/walkLevelName" resource="rdfxp-test:walkLevelName">walkLevelName</a>',
            'children' => array(
              0 => array(
                'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/walkCurrentPath" resource="rdfxp-test:walkCurrentPath">walkCurrentPath</a>',
              ),
              1 => array(
                'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/walkName" resource="rdfxp-test:walkName">walkName</a>',
              ),
            ),
            'children_title' => '7/0/subProperties',
          ),
          1 => array(
            'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/walkStructureItem" resource="rdfxp-test:walkStructureItem">walkStructureItem</a>',
          ),
        ),
        'children_title' => '7/subProperties',
      ),
      8 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/elementWalkContext" resource="rdfxp-test:elementWalkContext">elementWalkContext</a>',
      ),
      9 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/entityBundle" resource="rdfxp-test:entityBundle">entityBundle</a>',
      ),
      10 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldDefaultValue" resource="rdfxp-test:fieldDefaultValue">fieldDefaultValue</a>',
      ),
      11 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldInstance" resource="rdfxp-test:fieldInstance">fieldInstance</a>',
      ),
      12 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldInstanceEntity" resource="rdfxp-test:fieldInstanceEntity">fieldInstanceEntity</a>',
        'children' => array(
          0 => array(
            'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldInstanceBundle" resource="rdfxp-test:fieldInstanceBundle">fieldInstanceBundle</a>',
          ),
        ),
        'children_title' => '12/subProperties',
      ),
      13 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldInstanceViewMode" resource="rdfxp-test:fieldInstanceViewMode">fieldInstanceViewMode</a>',
      ),
      14 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldViewMode" resource="rdfxp-test:fieldViewMode">fieldViewMode</a>',
      ),
      15 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldWidget" resource="rdfxp-test:fieldWidget">fieldWidget</a>',
      ),
      16 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/structureForWalkOptions" resource="rdfxp-test:structureForWalkOptions">structureForWalkOptions</a>',
      ),
      17 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/swoopOntologyResource" resource="rdfxp-test:swoopOntologyResource">swoopOntologyResource</a>',
      ),
    ),
  ),
);

$rdfxp__model__rdf_rdfxp_test__DataModel__subclasses_and_instances__renderable = array (
  0 => array(
    'revRdfsSubClassOf' => array(
      '#theme' => 'item_list',
      '#title' => 'SubClasses + Instances',
      '#items' => array(
        0 => array(
          'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/Bundle" resource="rdfxp-test:Bundle">Bundle</a>',
        ),
        1 => array(
          'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/Entity" resource="rdfxp-test:Entity">Entity</a>',
        ),
        2 => array(
          'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/FieldBase" resource="rdfxp-test:FieldBase">FieldBase</a>',
        ),
        3 => array(
          'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/FieldInstance" resource="rdfxp-test:FieldInstance">FieldInstance</a>',
          'children' => array(
            0 => array(
              'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/FieldInstanceViewMode" resource="rdfxp-test:FieldInstanceViewMode">FieldInstanceViewMode</a>',
              'children' => array(
                0 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/node-actualite-field_picture-default" resource="rdfxp-test:node-actualite-field_picture-default">node-actualite-field_picture-default</a>',
                ),
                1 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/node-actualite-field_picture-teaser" resource="rdfxp-test:node-actualite-field_picture-teaser">node-actualite-field_picture-teaser</a>',
                ),
                2 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/node-publication-field_picture-default" resource="rdfxp-test:node-publication-field_picture-default">node-publication-field_picture-default</a>',
                ),
                3 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/node-publication-field_picture-full" resource="rdfxp-test:node-publication-field_picture-full">node-publication-field_picture-full</a>',
                ),
                4 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/node-publication-field_picture-teaser" resource="rdfxp-test:node-publication-field_picture-teaser">node-publication-field_picture-teaser</a>',
                ),
                5 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/bean-textuel_mod_1-field_picture-default" resource="rdfxp-test:bean-textuel_mod_1-field_picture-default">bean-textuel_mod_1-field_picture-default</a>',
                ),
                6 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/bean-textuel_mod_2-field_picture-default" resource="rdfxp-test:bean-textuel_mod_2-field_picture-default">bean-textuel_mod_2-field_picture-default</a>',
                ),
                7 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/node-basic_page-field_picture-default" resource="rdfxp-test:node-basic_page-field_picture-default">node-basic_page-field_picture-default</a>',
                ),
                8 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/node-basic_page-field_picture-teaser" resource="rdfxp-test:node-basic_page-field_picture-teaser">node-basic_page-field_picture-teaser</a>',
                ),
              ),
              'children_title' => '3/0Instances',
            ),
          ),
          'children_title' => '3SubClasses + Instances',
        ),
      ),
    ),
  ),
);

$rdfxp__model__rdf_rdfxp_test__node_actualite_field_picture_default__types_and_superclasses__renderable = array (
  0 => array(
    'relRdfType' => array(
      '#theme' => 'item_list',
      '#title' => 'Types + SuperClasses',
      '#items' => array(
        0 => array(
          'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/FieldInstanceViewMode" resource="rdfxp-test:FieldInstanceViewMode">FieldInstanceViewMode</a>',
          'children' => array(
            0 => array(
              'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/FieldInstance" resource="rdfxp-test:FieldInstance">FieldInstance</a>',
              'children' => array(
                0 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/DataModel" resource="rdfxp-test:DataModel">DataModel</a>',
                ),
              ),
              'children_title' => '0/0SuperClasses',
            ),
          ),
          'children_title' => '0SuperClasses',
        ),
      ),
    ),
  ),
);

$rdfxp__model__rdf_rdfxp_test__node_actualite_field_picture_default__types_and_superclasses__walkedToSparql__is_rev_false__contextual_neg1 = 'CONSTRUCT { ?s ?p ?o .} WHERE {
{

rdfxp-test:node-actualite-field_picture-default rdf:type ?o .
?s ?p ?o .
}
UNION {

rdfxp-test:node-actualite-field_picture-default rdf:type ?o .
?s ?p ?o .
FILTER str(?s) = str(rdfxp-test:node-actualite-field_picture-default) and str(?p) = str(rdf:type)
}
UNION {

rdfxp-test:FieldInstanceViewMode rdfs:subClassOf ?o .
?s ?p ?o .
}
UNION {

rdfxp-test:FieldInstanceViewMode rdfs:subClassOf ?o .
?s ?p ?o .
FILTER str(?s) = str(rdfxp-test:FieldInstanceViewMode) and str(?p) = str(rdfs:subClassOf)
}
UNION {

rdfxp-test:FieldInstance rdfs:subClassOf ?o .
?s ?p ?o .
}
UNION {

rdfxp-test:FieldInstance rdfs:subClassOf ?o .
?s ?p ?o .
FILTER str(?s) = str(rdfxp-test:FieldInstance) and str(?p) = str(rdfs:subClassOf)
}
UNION {

rdfxp-test:DataModel rdfs:subClassOf ?o .
?s ?p ?o .
}
UNION {

rdfxp-test:DataModel rdfs:subClassOf ?o .
?s ?p ?o .
FILTER str(?s) = str(rdfxp-test:DataModel) and str(?p) = str(rdfs:subClassOf)
}
}';

$rdfxp__model__rdf_rdfxp_test__node_actualite_field_picture_default__types_and_superclasses__walkedToSparql__is_rev_false__contextual_neg1__rdfxp_model_explore = array (
  '#theme' => 'item_list',
  '#title' => 'Explored',
  '#items' => array(
    'FieldInstanceViewMode' => array(
      'data' => 'FieldInstanceViewMode',
      'children' => array(
        0 => array(
          'data' => '0 : walkStructure',
          'children' => array(
            'revRdfsDomain' => array(
              'data' => 'revRdfsDomain',
              'children' => array(
                0 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldViewModeSettings" resource="rdfxp-test:fieldViewModeSettings">fieldViewModeSettings</a>',
                ),
                1 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldViewMode" resource="rdfxp-test:fieldViewMode">fieldViewMode</a>',
                ),
              ),
            ),
            'revRdfsRange' => array(
              'data' => 'revRdfsRange',
              'children' => array(
                0 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldInstanceViewMode" resource="rdfxp-test:fieldInstanceViewMode">fieldInstanceViewMode</a>',
                ),
              ),
            ),
            'revRdfType' => array(
              'data' => 'revRdfType',
              'children' => array(
                0 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/node-actualite-field_picture-default" resource="rdfxp-test:node-actualite-field_picture-default">node-actualite-field_picture-default</a>',
                ),
                1 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/node-actualite-field_picture-teaser" resource="rdfxp-test:node-actualite-field_picture-teaser">node-actualite-field_picture-teaser</a>',
                ),
                2 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/node-publication-field_picture-default" resource="rdfxp-test:node-publication-field_picture-default">node-publication-field_picture-default</a>',
                ),
                3 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/node-publication-field_picture-full" resource="rdfxp-test:node-publication-field_picture-full">node-publication-field_picture-full</a>',
                ),
                4 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/node-publication-field_picture-teaser" resource="rdfxp-test:node-publication-field_picture-teaser">node-publication-field_picture-teaser</a>',
                ),
                5 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/bean-textuel_mod_1-field_picture-default" resource="rdfxp-test:bean-textuel_mod_1-field_picture-default">bean-textuel_mod_1-field_picture-default</a>',
                ),
                6 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/bean-textuel_mod_2-field_picture-default" resource="rdfxp-test:bean-textuel_mod_2-field_picture-default">bean-textuel_mod_2-field_picture-default</a>',
                ),
                7 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/node-basic_page-field_picture-default" resource="rdfxp-test:node-basic_page-field_picture-default">node-basic_page-field_picture-default</a>',
                ),
                8 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/node-basic_page-field_picture-teaser" resource="rdfxp-test:node-basic_page-field_picture-teaser">node-basic_page-field_picture-teaser</a>',
                ),
              ),
            ),
          ),
        ),
      ),
    ),
    'FieldInstance' => array(
      'data' => 'FieldInstance',
      'children' => array(
        0 => array(
          'data' => '0 : walkStructure',
          'children' => array(
            'revRdfsSubClassOf' => array(
              'data' => 'revRdfsSubClassOf',
              'children' => array(
                0 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/FieldInstanceViewMode" resource="rdfxp-test:FieldInstanceViewMode">FieldInstanceViewMode</a>',
                  'children' => array(
                    0 => array(
                      'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldViewModeSettings" resource="rdfxp-test:fieldViewModeSettings">fieldViewModeSettings</a>',
                    ),
                    1 => array(
                      'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldViewMode" resource="rdfxp-test:fieldViewMode">fieldViewMode</a>',
                    ),
                  ),
                  'children_title' => '0/revRdfsDomain',
                ),
              ),
            ),
            'revRdfsDomain' => array(
              'data' => 'revRdfsDomain',
              'children' => array(
                0 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldLabel" resource="rdfxp-test:fieldLabel">fieldLabel</a>',
                ),
                1 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldRequired" resource="rdfxp-test:fieldRequired">fieldRequired</a>',
                ),
                2 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldDefaultValue" resource="rdfxp-test:fieldDefaultValue">fieldDefaultValue</a>',
                ),
                3 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldInstanceBundle" resource="rdfxp-test:fieldInstanceBundle">fieldInstanceBundle</a>',
                ),
                4 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldInstanceEntity" resource="rdfxp-test:fieldInstanceEntity">fieldInstanceEntity</a>',
                ),
                5 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldInstanceViewMode" resource="rdfxp-test:fieldInstanceViewMode">fieldInstanceViewMode</a>',
                ),
                6 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldWidget" resource="rdfxp-test:fieldWidget">fieldWidget</a>',
                ),
              ),
            ),
            'revRdfsRange' => array(
              'data' => 'revRdfsRange',
              'children' => array(
                0 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldInstance" resource="rdfxp-test:fieldInstance">fieldInstance</a>',
                ),
              ),
            ),
            'revRdfType' => array(
              'data' => 'revRdfType',
              'children' => array(
                0 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/node-actualite-field_picture" resource="rdfxp-test:node-actualite-field_picture">node-actualite-field_picture</a>',
                ),
                1 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/node-publication-field_picture" resource="rdfxp-test:node-publication-field_picture">node-publication-field_picture</a>',
                ),
                2 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/bean-textuel_mod_1-field_picture" resource="rdfxp-test:bean-textuel_mod_1-field_picture">bean-textuel_mod_1-field_picture</a>',
                ),
                3 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/bean-textuel_mod_2-field_picture" resource="rdfxp-test:bean-textuel_mod_2-field_picture">bean-textuel_mod_2-field_picture</a>',
                ),
                4 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/node-basic_page-field_picture" resource="rdfxp-test:node-basic_page-field_picture">node-basic_page-field_picture</a>',
                ),
              ),
            ),
          ),
        ),
      ),
    ),
    'DataModel' => array(
      'data' => 'DataModel',
      'children' => array(
        0 => array(
          'data' => '0 : walkStructure',
          'children' => array(
            'revRdfsSubClassOf' => array(
              'data' => 'revRdfsSubClassOf',
              'children' => array(
                0 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/Bundle" resource="rdfxp-test:Bundle">Bundle</a>',
                ),
                1 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/Entity" resource="rdfxp-test:Entity">Entity</a>',
                ),
                2 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/FieldBase" resource="rdfxp-test:FieldBase">FieldBase</a>',
                ),
                3 => array(
                  'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/FieldInstance" resource="rdfxp-test:FieldInstance">FieldInstance</a>',
                  'children' => array(
                    0 => array(
                      'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/FieldInstanceViewMode" resource="rdfxp-test:FieldInstanceViewMode">FieldInstanceViewMode</a>',
                      'children' => array(
                        0 => array(
                          'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldViewModeSettings" resource="rdfxp-test:fieldViewModeSettings">fieldViewModeSettings</a>',
                        ),
                        1 => array(
                          'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldViewMode" resource="rdfxp-test:fieldViewMode">fieldViewMode</a>',
                        ),
                      ),
                      'children_title' => '3/0/revRdfsDomain',
                    ),
                  ),
                  'children_title' => '3SubClasses + Instances',
                ),
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);

$rdfxp__model__rdf_rdfxp_test__FieldInstance__SwoopOntology__SwoopClass__renderable = array (
  'uri' => array(
    '#theme' => 'item_list',
    '#items' => array(
      'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/FieldInstance" resource="rdfxp-test:FieldInstance">FieldInstance</a>',
    ),
    '#title' => 'uri',
  ),
  'superClasses' => array(
  ),
  'instances' => array(
  ),
  'domainOf' => array(
    '#theme' => 'item_list',
    '#items' => array(
      0 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldLabel" resource="rdfxp-test:fieldLabel">fieldLabel</a>',
      ),
      1 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldRequired" resource="rdfxp-test:fieldRequired">fieldRequired</a>',
      ),
      2 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldDefaultValue" resource="rdfxp-test:fieldDefaultValue">fieldDefaultValue</a>',
      ),
      3 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldInstanceBundle" resource="rdfxp-test:fieldInstanceBundle">fieldInstanceBundle</a>',
      ),
      4 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldInstanceEntity" resource="rdfxp-test:fieldInstanceEntity">fieldInstanceEntity</a>',
      ),
      5 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldInstanceViewMode" resource="rdfxp-test:fieldInstanceViewMode">fieldInstanceViewMode</a>',
      ),
      6 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldWidget" resource="rdfxp-test:fieldWidget">fieldWidget</a>',
      ),
    ),
    '#title' => 'domainOf',
  ),
  'rangeOf' => array(
    '#theme' => 'item_list',
    '#items' => array(
      0 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldInstance" resource="rdfxp-test:fieldInstance">fieldInstance</a>',
      ),
    ),
    '#title' => 'rangeOf',
  ),
);

$rdfxp__model__rdf_rdfxp_test__fieldInstanceEntity__SwoopOntology__SwoopProperty__renderable = array (
  'uri' => array(
    '#theme' => 'item_list',
    '#items' => array(
      'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldInstanceEntity" resource="rdfxp-test:fieldInstanceEntity">fieldInstanceEntity</a>',
    ),
    '#title' => 'uri',
  ),
  'subProperties' => array(
    '#theme' => 'item_list',
    '#items' => array(
      0 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldInstanceBundle" resource="rdfxp-test:fieldInstanceBundle">fieldInstanceBundle</a>',
      ),
    ),
    '#title' => 'subProperties',
  ),
  'superProperties' => array(
  ),
  'domain' => array(
    '#theme' => 'item_list',
    '#items' => array(
      0 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/FieldInstance" resource="rdfxp-test:FieldInstance">FieldInstance</a>',
      ),
    ),
    '#title' => 'domain',
  ),
  'range' => array(
    '#theme' => 'item_list',
    '#items' => array(
      0 => array(
        'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/Entity" resource="rdfxp-test:Entity">Entity</a>',
      ),
    ),
    '#title' => 'range',
  ),
);

$rdfxp__model__rdf_rdfxp_test__node_actualite_field_picture_default__SwoopOntology__SwoopIndividual__renderable = array (
  'uri' => array(
    '#theme' => 'item_list',
    '#items' => array(
      'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/node-actualite-field_picture-default" resource="rdfxp-test:node-actualite-field_picture-default">node-actualite-field_picture-default</a>',
    ),
    '#title' => 'uri',
  ),
  'likelyProperties' => array(
  ),
  'objectAssertions' => array(
    'ObjectAssertions' => array(
      '#theme' => 'item_list',
      '#items' => array(
        0 => array(
          'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldViewMode" resource="rdfxp-test:fieldViewMode">fieldViewMode</a>',
          'children' => array(
            0 => array(
              'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/display_default" resource="rdfxp-test:display_default">display_default</a>',
            ),
          ),
        ),
      ),
      '#title' => 'ObjectPropertyAssertions',
    ),
  ),
  'datatypeAssertions' => array(
    'ObjectAssertions' => array(
      '#theme' => 'item_list',
      '#items' => array(
        0 => array(
          'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/fieldViewModeSettings" resource="rdfxp-test:fieldViewModeSettings">fieldViewModeSettings</a>',
          'children' => array(
            0 => array(
              'data' => 'Literal("<config><label>above</label>
<module>total_atom_reference</module>
<settings>array()</settings>
<type>title</type>
<weight>8</weight>
</config>", datatype="http://www.w3.org/1999/02/22-rdf-syntax-ns#XMLLiteral")',
            ),
          ),
        ),
      ),
      '#title' => 'DatatypePropertyAssertions',
    ),
  ),
  'propertyValueOf' => array(
    'fieldInstanceViewMode_Reverse' => array(
      '#theme' => 'item_list',
      '#items' => array(
        0 => array(
          'data' => '<a href="/unltd-new/en/swoop/rdf_rdfxp-test/node-actualite-field_picture" resource="rdfxp-test:node-actualite-field_picture">node-actualite-field_picture</a>',
        ),
      ),
      '#title' => 'fieldInstanceViewMode_Reverse',
    ),
  ),
);