<?php
namespace Drupal\rdfxp_test;

/**
 * @todo #critical: update this doc comment
 * To monitor a function:
 *  1) use \Drupal\rdfxp_test\CallStackTrait;
 *  2) $drupal_static_item = & $this->rdfxpItem(__FUNCTION__); or not yet
 *  Drupal\rdfxp_test\CallStack::rdfxpItem(__FUNCTION__). Then collected infos are
 *  object [, this] [, vars] [, return] (CallStackTrait is a trait, so rdf:domain
 *  to classes only).
 *  3) For get working $this->rdfxpItem(__FUNCTION__), except when called for the
 *  FIRST TIME, there must exist somewhere in the highter level in the call stack
 *  the following block:
 *    $drupal_static_copy = & $this->rdfxpPre(...);
 *    "heighter or __FUNCTION__"();
 *    $this->rdfxpPost(...);
 *  4) create/apply the named patch (@dev env with single git). Ex. for main_content_block:
 *    - git add core/modules/block
 *    - git diff --staged > [rdfxp dir]/Drupal\\rdfxp_test\\main_content_block.patch
 *    use new branch for patch creation if not applying on master
 *    - git reset HEAD core/modules/block
 *    apply: git apply -v [rdfxp dir]/Drupal\\rdfxp_test\\main_content_block.patch
 *  5) optionnaly create structure array to limit drupal_static output
 *  6) checkout patched files (@dev env with single git). Ex. for main_content_block:
 *    - git checkout core/modules/block
 *
 * @todo #major: for functions or static methods, create callstack logger service which uses CallStackTrait
 *
 * @todo #major: reviewing each_action for loop, assignement; how for alternative, option?
 * @todo lazy_loader: where to put this->rdfxpPre() and this->rdfxpPost() for
 *    \Drupal\block\BlockViewBuilder::buildPreRenderableBlock()
 *
 * @see Drupal\block\Entity\Block::access():
 *  1) overidden only data collecting.
 *  2) somewhere in deeper - \Drupal\block\BlockAccessControlHandler::checkAccess()
 *  we have $this->rdfxpItem(__FUNCTION__)
 * @see Drupal\block\Plugin\DisplayVariant\BlockPageVariant::build():
 *   $block in $getVisibleBlocksPerRegion[$region, $key]'][$region]
 * @see Drupal\block\BlockRepository::getVisibleBlocksPerRegion():
 *   second time in a loop
 *
 * @todo #minor: override \Drupal\block\Entity\Block for having \Drupal\block\Entity\Block::access. see webprofiler data collectors
 * Use named patches until data collectors are missing. Also needed is dependency between patches
 * @todo #minor: use named branches instead of named patches ? Inconvenient is: no meld from master when using branches
 *
 */
trait CallStackTrait {

  protected $rdfxpExport = TRUE;
  /**
   * @example
   * $drupal_static_item = & $this->rdfxpItem(__FUNCTION__);
   * @todo #minor: use array_object for wrapping public, protected and private access
   *  to $drupal_static_item. It is pass by reference: look out for 'this',
   *  'object', 'get_defined_vars', 'return', ...
   * @param string $function
   */
  public function & rdfxpItem($function, array $vars = array()) {

    $drupal_static = & drupal_static('call_stack__main_content_block', NULL);
    $method =
      '$return = ?->' .
//       __CLASS__ . '::' .
      $function . '()';
//     krumo($drupal_static);
    if(!isset($drupal_static)) {

      $drupal_static[$method][] = array();
      end($drupal_static[$method]);
      $key = key($drupal_static[$method]);

      $drupal_static_item = & $drupal_static[$method][$key];

      if(!$this->rdfxpExport) {
        $drupal_static_item['backtrace'] = debug_backtrace();
      }
    }
    else {
      $drupal_static[] = array();
      end($drupal_static);
      $key = key($drupal_static);

      $drupal_static_item = & $drupal_static[$key];

      if(is_null($drupal_static_item)) {
        var_dump($drupal_static, $toto);
      }
    }
//     // No need of object info for function and static methods.
//     if(__CLASS__ != 'Drupal\rdfxp_test\CallStack') {
//       // TODO: #major: link to rdfxp-object-vocab.
//       $drupal_static_item['object'] = __CLASS__;
//     }

//     // Instances with their roles, props in this.
//     foreach (get_object_vars($this) as $name => $object) {
//       if($name != 'rdfxpExport') {
//         $drupal_static_item['this'][$name] = $this->rdfxpExport($object);
//       }
//     }

//     krumo($drupal_static_item);
    $drupal_static_item += $this->rdfxpItemThis();
    $drupal_static_item += $this->rdfxpItemVars($vars);

    return $drupal_static_item;
  }

  public function rdfxpItemClass() {
    return spl_object_hash($this) . ':' . __CLASS__;
  }

  public function rdfxpItemThis() {
    $return = array();
  // No need of object info for function and static methods.
    if(__CLASS__ != 'Drupal\rdfxp_test\CallStack') {
      // TODO: #major: link to rdfxp-object-vocab.
      $return['object'] = spl_object_hash($this) . ':' . get_class($this);
    }

    // Instances with their roles, props in this.
    // TODO: #critical: recursion marker: if already then add the xpath reference
    foreach (get_object_vars($this) as $name => $object) {
      if($name != 'rdfxpExport') {
        $return['this'][$name] = $this->rdfxpExport($object);
      }
    }

    return $return;
  }

  public function rdfxpItemVars($vars = array()) {
    $return = array();
    // Locals instances in $function with their roles, vars.
    $get_defined_vars = array_diff_key($vars, array_flip(array(
      'return', 'object', 'this', 'get_defined_vars', 'callStack'
    )));
    foreach($get_defined_vars as $name => $object) {
      $return['get_defined_vars'][$name] = $this->rdfxpExport($object);
    }

    return $return;
  }

  public function & rdfxpItemKey($each_action) {
    // $drupal_static is cleared at every call of rdfxpPre()
    $drupal_static = & drupal_static('call_stack__main_content_block', NULL);
    $drupal_static[$each_action] = array();
    $drupal_static_item = & $drupal_static[$each_action];

    return $drupal_static_item;
  }

  /**
   * @example
   * $drupal_static_copy = & $this->rdfxpPre($drupal_static_item, $call_action = '$this->blockRepository->getVisibleBlocksPerRegion($cacheable_metadata_list : array)');
   * @param array $drupal_static_item
   * @param string $call_action
   */
  public function & rdfxpPre(& $drupal_static_item, $call_action) {
//     $call_action = '$this->blockStorage->loadByProperties(array(\'theme\' => $active_theme->getName()))';
    $drupal_static = & drupal_static('call_stack__main_content_block');
    $drupal_static_copy = $drupal_static;
    $drupal_static_item[$call_action] = array();
    $drupal_static = $drupal_static_item[$call_action];

    return $drupal_static_copy;
  }

  /**
   * @example
   * $this->rdfxpPost($drupal_static_item, $call_action, $drupal_static_copy, $getVisibleBlocksPerRegion, FALSE);
   *
   * @param array $drupal_static_item
   * @param string $call_action
   * @param array $drupal_static_copy
   * @param mixed $return
   * @param string $just_for_return
   */
  public function & rdfxpPost(& $drupal_static_item, $call_action, & $drupal_static_copy, $return = NULL, $just_for_return = TRUE) {
    // Last item. Probem if loadByProperties() is calling his self? Not if same thing "...[return]" when calling itself
    $drupal_static = & drupal_static('call_stack__main_content_block');
    if(isset($return)) {
      // no call to rdfxpItem()
      if($just_for_return) {
        $drupal_static[] = array();
      }
      // Last item.
      // TODO: #minor: It is ok - same thing "...[return]" is used when calling itself. Continue tests for ensuring that no probems when loadByProperties() is calling his self?
      end($drupal_static);
      $key = key($drupal_static);
      $drupal_static[$key]['return'] = $this->rdfxpExport($return);
    }
    $drupal_static_item[$call_action] = $drupal_static;
    $drupal_static = $drupal_static_copy;
  }

  /**
   * @todo #minor: merge with krumo like swoop traversable Litteral
   * @param unknown $value
   */
  public function rdfxpExport($value) {
    if($this->rdfxpExport) {
      if(is_object($value)) {
        $value = spl_object_hash($value) . ':' . get_class($value);
      }
      if(is_array($value)) {
        array_walk_recursive($value, function(& $item, $key) {
          if(is_object($item)) {
            $item = spl_object_hash($item) . ':' . get_class($item);
          }
        });
      }
    }
    return $value;
  }
}