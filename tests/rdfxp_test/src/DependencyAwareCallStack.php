<?php
namespace Drupal\rdfxp_test;

use Drupal\rdfxp_arc2\DependencyAware;

/**
 * For tracking method calls => override the thracked method and call the parent
 * For inside function debug:
 *   1) replace the "parent call" with the function body, then add debug
 *   2) @todo #major: Or create patch for variables/loops debug
 *
 */
class DependencyAwareCallStack extends DependencyAware {
  use CallStackTrait;

  public function get($types, $is_rev = FALSE) {
    $drupal_static_item = & $this->rdfxpItem(__FUNCTION__, array_intersect_key(get_defined_vars(), array_flip(array('types', 'is_rev'))));

    $drupal_static_copy = & $this->rdfxpPre($drupal_static_item, $call_action = '$deps_aware->' . __FUNCTION__ . '($types, $is_rev)');
    // Tracked method
    $return = parent::get($types, $is_rev);
    $this->rdfxpPost($drupal_static_item, $call_action, $drupal_static_copy, $return, FALSE); // [$each_action][$key]

    return $return;
  }

  public function getMissing($look_for, $types, &$prev) {
    $drupal_static_item = & $this->rdfxpItem(__FUNCTION__, array_intersect_key(get_defined_vars(), array_flip(array('look_for', 'types'))));

    $drupal_static_copy = & $this->rdfxpPre($drupal_static_item, $call_action = '$deps_aware->' . __FUNCTION__ . '($look_for, $types, $prev)');
    // Tracked method
    $return = parent::getMissing($look_for, $types, $prev);
    $this->rdfxpPost($drupal_static_item, $call_action, $drupal_static_copy, $return, FALSE);

    // Additional tracked variable
    $drupal_static_item['get_defined_vars']['prev (before return)'] = $prev;
    return $return;
  }

  public function getMissingDeep(&$prev, array $missing) {
    $drupal_static_item = & $this->rdfxpItem(__FUNCTION__, array_intersect_key(get_defined_vars(), array_flip(array('prev', 'missing'))));

    $drupal_static_copy = & $this->rdfxpPre($drupal_static_item, $call_action = '$deps_aware->' . __FUNCTION__ . '($prev, $missing)');
    // Tracked method
    $return = parent::getMissingDeep($prev, $missing);
    $this->rdfxpPost($drupal_static_item, $call_action, $drupal_static_copy, $return, FALSE);

    return $return;
  }

  public function getDependencies($type, $types) {
    $drupal_static_item = & $this->rdfxpItem(__FUNCTION__);

    $drupal_static_copy = & $this->rdfxpPre($drupal_static_item, $call_action = '$deps_aware->' . __FUNCTION__ . '($type, $types)');
    // Tracked method
    $return = parent::getDependencies($type, $types);
    $this->rdfxpPost($drupal_static_item, $call_action, $drupal_static_copy, $return, FALSE);

    return $return;
  }
}