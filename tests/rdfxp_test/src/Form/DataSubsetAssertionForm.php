<?php

namespace Drupal\rdfxp_test\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactory;

/**
 * Class DataSubsetAssertionForm.
 *
 *
 * Do assert and show diff if not equals
 *
 * Data providers + Assertions of specific outputs for automated testing from config
 * A step before Simpletest
 *
 * Motivation for why not using simpletest Test Class?
 *   - The test fixture is a rdf model from exported config from fresh install of standard
 *   profile.
 *   - For time saving, must be done only once when writing/updating assertions:
 *     - site install standard profile
 *     - drush cex --destination=sites/default/config/standard
 *     - drush rdfxp-fr --verbose --config-rdf
 *
 * @package Drupal\rdfxp_test\Form
 */
class DataSubsetAssertionForm extends FormBase {


  /**
   * Drupal\Core\Config\ConfigFactory definition.
   *
   * @var Drupal\Core\Config\ConfigFactory
   */
  protected $config_factory;

  public function __construct(
    ConfigFactory $config_factory
  ) {
    $this->config_factory = $config_factory;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'data_subset_assertion_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $header = array(
      'assertion' => t('Assertion'),
    );

    /** @var $config_factory \Drupal\Core\Config\ConfigFactory */
    $config_factory = \Drupal::service('config.factory');
    // TODO: #normal: Should replace config by annotation? Then drop the config. Still will need $module
    //   (from drupal get path or from class namespace) and $file (.yml) for $data
    $data_subset_assertion_list = $config_factory->listAll('rdfxp_test.data_subset_assertion');
    $options = array();
    foreach ($data_subset_assertion_list as $data_subset_assertion_name) {
      $options[$data_subset_assertion_name] = array(
        'assertion' => $data_subset_assertion_name,
      );
    }

    /** @var $tempstore \Drupal\user\PrivateTempStore */
    $tempstore = \Drupal::service('user.private_tempstore')->get('rdfxp_test');
    if($session_store = $tempstore->get('assertion_result')) {

      $form['assertion_result'] = array(
        '#type' => 'details',
        '#title' => t('Assertion results'),
      );
      $error = array();
      foreach ($session_store as $name => $assertion_result) {

        $form['assertion_result'][$name] = array(
          '#type' => 'details',
          '#title' => $name,
        );
        $form['assertion_result'][$name][] = $assertion_result;

        if($assertion_result['#error']) {
          // Optionally display the case output
          foreach ($assertion_result as $case => $case_result) {
            if(isset($case_result['#output'])) {
              krumo($case_result['#output']);
            }
          }
          $error[$name] = $assertion_result['#error'];
          $form['assertion_result'][$name]['#title'] = count($assertion_result['#error']) . ' FAILED: ' . $form['assertion_result'][$name]['#title'];
          $form['assertion_result'][$name]['#open'] = TRUE;
        }
      }

      if($error) {

        drupal_set_message(count($error) . ' assertions failed', 'error');
        $form['assertion_result']['#title'] = count($error) . ' FAILED: ' . $form['assertion_result']['#title'];
        $form['assertion_result']['#open'] = TRUE;
        $form['#attached']['library'][] = 'system/diff';
      }
      else {
        drupal_set_message('All assertions passed');
      }

      $tempstore->delete('assertion_result');
    }

    $form['assertion'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
    );

    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Run'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $build = $error = array();
    foreach ($form_state->getValue('assertion') as $data_subset_assertion_name) {
      if($data_subset_assertion_name) {
        $config = \Drupal::config($data_subset_assertion_name);

        // Run assertion
        $class = $config->get('class');
        $data_subset_assertion = new $class($config);
        $build[$data_subset_assertion_name] = $data_subset_assertion->run();

        if($build['#error']) {
          $error[$data_subset_assertion_name] = $build['#error'];
        }

        // Store results to be displayed in the form build
        $tempstore = \Drupal::service('user.private_tempstore')->get('rdfxp_test');
        $tempstore->set('assertion_result', $build);
      }
    }
  }
}
