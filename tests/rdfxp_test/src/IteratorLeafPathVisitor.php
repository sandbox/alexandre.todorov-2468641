<?php
namespace Drupal\rdfxp_test;

/**
 * @see Drupal\rdfxp_test\DataSubsetAssertion::run()
 */
class IteratorLeafPathVisitor extends \IteratorVisitor {
  public function leafCallback($item, $key) {
    parent::leafCallback($item, $key);

    $this->context['leafPath'][] = implode('/', $this->context['currentPathItems']);
  }
}