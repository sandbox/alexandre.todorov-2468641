<?php

namespace Drupal\rdfxp_test\DataSubsetAssertion;

use Drupal\rdfxp_test\DataSubsetAssertion;
use Drupal\rdfxp_test\DependencyAwareCallStack;

/**
 * Example of subset assertion class for creating yml data for Drupal\rdfxp_test\DependencyAwareCallStack:
 *   0) adapt self::$data['Some case']['args']
 *   1) copy path from krumo then visually adapt levels for readability for $deps_path in setDataForYmlCreation()
 *   2) Run the current class assertion and copy path: from the failed assertion to the yml data
 *   3) Run the current class assertion and copy value: from the failed assertion to the yml data
 *
 * @todo #normal: The infos for the real class (here Drupal\rdfxp_test\DataSubsetAssertion\DependencyAware)
 *   must be dynamic: arguments $this->data['Some case']['args'], callable in $this->runCase() and $this->object
 */
class DataCreation extends DataSubsetAssertion {
  public function __construct($config) {

    parent::__construct($config);
    $this->setDataForYmlCreation();

    $this->object = new DependencyAwareCallStack('files__rdf__rdfxp-config-auto');

    return;
  }

  public function setDataForYmlCreation() {

    // This is an example of path for subset assertion
    $deps_path
    ['$deps_aware->get($types, $is_rev)']
    ['$component_name in $components/ViewDisplay']
    ['1/$deps_aware->getMissingDeep($prev, $missing)']
    ['$type, $values in $missing/ViewDisplay']
    ['1/$deps_aware->getMissingDeep($prev, $missing)']
    ['$type, $values in $missing/FieldField']
    ['/$deps_aware->getMissingDeep($prev, $missing)']
    ['$type, $values in $missing/Entity']
    ['0/$deps_aware->getMissing($look_for, $types, $prev)/0/return'] = array();

    $deps_path_value = 'not existing value for this $deps_path';

    $this->data = array(
      'Some case' => array(
        'struct' => array(
          array(
            'path' => $deps_path,
            'value' => $deps_path_value,
          )
        ),
        'args' => array(
          'is_rev' => FALSE,
          'types' => array(
            'ViewDisplay' => array (
              'core.entity_view_display.node.article.teaser' => 'core.entity_view_display.node.article.teaser'
            ),
          ),
        ),
      ),
    );
  }

  public function runCase(array $case_data) {
    $assertion = new DependencyAware($this->config);
    return $assertion->runCase($case_data);
  }
}