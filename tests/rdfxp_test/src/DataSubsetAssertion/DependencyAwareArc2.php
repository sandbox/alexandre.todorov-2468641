<?php

namespace Drupal\rdfxp_test\DataSubsetAssertion;

use Drupal\rdfxp_test\DependencyAwareCallStack;

class DependencyAwareArc2 extends DependencyAware {
  public function __construct($config) {

    parent::__construct($config);

    $name = 'bundle_data_store_loaded';
    $store = rdfxp_arc2_init_store($name);
    $store->reset();
    $filename = rdfxp_arc2_file_create_url('public://files/rdf/rdfxp-config-auto.rdf');
    $store->query('LOAD <' . $filename . '>');

    $name_when_created = 'bundle_data_store';
    $store->rdfxp_ns0 = rdfxp_arc2_file_create_url($name_when_created) . '#';

    $this->object = new DependencyAwareCallStack($store);
  }
}