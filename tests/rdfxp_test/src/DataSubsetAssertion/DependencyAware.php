<?php

namespace Drupal\rdfxp_test\DataSubsetAssertion;

use Drupal\rdfxp_test\DataSubsetAssertion;
use Drupal\rdfxp_test\DependencyAwareCallStack;

class DependencyAware extends DataSubsetAssertion {

  public function __construct($config) {
    parent::__construct($config);

    $this->object = new DependencyAwareCallStack('files__rdf__rdfxp-config-auto');
  }

  public function runCase(array $case_data) {
    $data = $case_data;
    $deps_aware = $this->object;

    // Reset for each loop
    $deps_aware->reset();
    $drupal_static = & drupal_static('call_stack__main_content_block');
    $drupal_static = NULL;

    // Call of the tested function
    $prev = $deps_aware->get($data['args']['types'], $data['args']['is_rev']);
    // Get call stack info
    $call_stack = $drupal_static['$return = ?->get()'][0];
    $call_stack['$deps_aware->getDependenciesAppended()'] = $deps_aware->getDependenciesAppended();

    return $call_stack;
  }
}