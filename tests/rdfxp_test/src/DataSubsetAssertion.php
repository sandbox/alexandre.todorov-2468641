<?php
namespace Drupal\rdfxp_test;

use Drupal\Component\Serialization\Yaml;
use Drupal\Component\Utility\NestedArray;
use Drupal\Component\Diff\Diff;

abstract class DataSubsetAssertion {

  protected $world = array();
  protected $object;
  protected $data = array();
  // TODO: #normal: If using annotation: config get 'rdfxp_test.data_subset_assertion.' . $this->name . '.yml'
  protected $config;

  public function __construct($config) {
    $this->config = $config;

    $yml_data = file_get_contents(drupal_get_path('module', 'rdfxp_test') . '/' . $this->config->get('file'));
    $this->data = Yaml::decode($yml_data);
  }

  protected abstract function runCase(array $case_data);

  /**
   * @return array Renderable test cases list with diff for failed ones
   */
  public function run(){

    /** @var $diff_formatter \Drupal\Core\Diff\DiffFormatter */
    $diff_formatter = \Drupal::service('diff.formatter');

    $cases = $error = array();
    foreach ($this->data as $case_label => $case_data) {
      $data = $case_data;
      $case = $case_label;

      $cases[$case] = array(
        '#type' => 'details',
        '#title' => $case,
        array('#markup' => '<div style="text-align:center;"><pre>' . Yaml::encode(($data['args'])) . '</pre></div>')
      );

      $call_stack = $this->runCase($case_data);

      foreach ($data['struct'] as $struct_i => $struct) {
        $path = $struct['path'];
        // It is a pseudo path more human readable in yml
        if(is_array($path)) {
          $walker = new IteratorLeafPathVisitor();
          $swoopElement = $walker->setContainer($path);
          $swoopElement->walkRecursive1();
          $path = $walker->context['leafPath'][0];
        }

        // Get expected subset value from assertion ($struct['value'])
        $expected = NestedArraySelectCreate(array($path => $struct['value']));
        // Get real subset value from call stack ($drupal_static)
        $real = NestedArraySelectCreate(array($path => NULL), $call_stack);

        // Get diffs for failing tests
        $source_data = explode("\n", Yaml::encode($expected));
        $target_data = explode("\n", Yaml::encode($real));

        $diff = new Diff($source_data, $target_data);
        $diff_formatted = $diff_formatter->format($diff);

        $cases[$case][$struct_i] = array(
          '#type' => 'details',
          '#title' => 'Assertion ' . $struct['label'],
          array('#markup' => '<div style="text-align:center;"><strong>Path:</strong><pre>' . Yaml::encode($struct['path']) . '</pre></div>'),
        );
        $cases[$case][$struct_i][] = array('#markup' => '<strong>Value:</strong><pre>' . Yaml::encode(NestedArray::getValue($real, explode('/', $path))) . '</pre>');

        if($diff_formatted) {

          $error[] = $case;
          $cases[$case][$struct_i]['#title'] = 'FAILED: ' . $cases[$case][$struct_i]['#title'];
          $cases[$case][$struct_i][] = array('#markup' => '<strong>Diff:</strong>');
          $cases[$case][$struct_i][] = array(
            '#type' => 'table',
            '#attributes' => array(
              'class' => array('diff'),
            ),
            '#header' => array(
              array('data' => 'Expected', 'colspan' => '2'),
              array('data' => 'Real', 'colspan' => '2'),
            ),
            '#rows' => $diff_formatted,
          );
        }
      }

      if(array_search($case, $error) !== FALSE) {
        $cases[$case]['#output'] = $call_stack;
        $cases[$case]['#title'] = 'FAILED: ' . $cases[$case]['#title'];
      }
    }

    $cases['#error'] = $error;

    return $cases;
  }
}