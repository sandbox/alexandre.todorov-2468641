jQuery(document).ready(function($) {
  // Code that uses jQuery's $ can follow here.
		
	
	
    
	// Graph params for img src
	$('#edit-g input').change(function(){
		
		params = [];
		pieces = $('#edit-img img').attr('src').split("?");
		
		if(pieces[1]) {
			
			
			var sURLVariables = pieces[1].split('&');
		    for (var i = 0; i < sURLVariables.length; i++) 
		    {
		        var sParameterName = sURLVariables[i].split('=');
		        params[sParameterName[0]] = sParameterName[1];
		    }
			
				
		}
		
		console.log(params);
		
		G_params_array = [];
		$.each($(this).closest('form').serializeArray(), function(i, item){
//			console.log(item);
			G_params_array[item.name] = item.value;
		});
		
		console.log(G_params_array);
		$(this).each(function(){
			
			
			
//			params['G[' + $(this).attr('name') + ']'] = G_params_array[$(this).attr('name')];
			params[$(this).attr('name')] = G_params_array[$(this).attr('name')];
		});
		
		$('#edit-img img').attr({src: pieces[0] + '?' + decodeURIComponent($.param($.extend({}, params)))});
		
		
		
		console.log($('#edit-img img').attr('src'), pieces[0] + '?' + decodeURIComponent($.param($.extend({}, params))));
	});

	if($(window.parent.document).find('body.rdfxp-html').size()) {
		$('#edit-img').css({width : '1480px'});
	}
	
	// Insert resource in textarea
	//rdfxp_html_dom = $('body.rdfxp-html');
	rdfxp_html_dom = $('body');
	rdfxp_html_dom.find('#edit-query').blur(function(event){
//		console.log(event, $(this).prop("selectionStart"), $(this).prop("selectionEnd")); //.is('a')
		
//		console.log($(window.parent.document).find('a:hover').size());
		
//		a_hovered_dom = $(window.parent.document).find('a[resource]:hover');
//		
//		if(!a_hovered_dom.size()) {
			a_hovered_dom = rdfxp_html_dom.find('a[resource]:hover');
//		}
		
		if(a_hovered_dom.size()) { 
		
			selectionStart = $(this).prop('selectionStart');
			selectionEnd = $(this).prop('selectionEnd');
			
			var textAreaTxt = $(this).val();
		    var txtToAdd = "stuff";
		    $(this).val(textAreaTxt.substring(0, selectionStart) + a_hovered_dom.attr('resource') + textAreaTxt.substring(selectionEnd) );
		    a_hovered_dom.attr('preventdefault', 'preventdefault');
		}		
	});
	
	// prevent default when inserting resource
	prevent_default_a_handler = function(event){
		
		if($(this).attr('preventdefault') == 'preventdefault') {
			event.preventDefault();
			$(this).removeAttr('preventdefault');
		}
	};
	
	//$('body.rdfxp-html a[resource]').click(prevent_default_a_handler);
	$('body a[resource]').click(prevent_default_a_handler);
	$(window.parent.document).find('a[resource]').click(prevent_default_a_handler);


	/*$( '#edit-img img' ).mousedown(function(event) {*/
	$( '#edit-img img' ).click(function(event) {
		
		if($(this).css('cursor') == 'pointer') {
			$(this).css({cursor:''});
		}
		else {
			$(this).css({cursor:'pointer'});
			
			$(this).attr({startx:event.pageX});
			$(this).attr({left:$(this).css('left')});
			$(this).attr({starty:event.pageY});
			$(this).attr({top:$(this).css('top')});
			
			$(this).attr({outerWidth:$(this).outerWidth()});
			$(this).attr({ratio:parseInt($(this).outerWidth())/parseInt($(this).outerHeight())});
		}
	});
	
	/*$( '#edit-img img' ).mouseup(function() {
		$(this).css({cursor:''});
	});*/
	
	$( '#edit-img img' ).mouseenter(function(event) {
		$(this).attr({startx:event.pageX});
		$(this).attr({left:$(this).css('left')});
		$(this).attr({starty:event.pageY});
		$(this).attr({top:$(this).css('top')});
		
		$(this).attr({outerWidth:$(this).outerWidth()});
		$(this).attr({ratio:parseInt($(this).outerWidth())/parseInt($(this).outerHeight())});
	});
	$( '#edit-img img' ).mouseleave(function(event) {
		$(this).removeAttr('startx');
		$(this).removeAttr('starty');
	});
	
	$( '#edit-img img' ).mousemove(function(event) {
		if($(this).css('cursor') == 'pointer') {
			
//			oldleft = parseInt($(this).css('left'));
			newleft = parseInt($(this).attr('left')) + event.pageX - parseInt($(this).attr('startx'));			
			$(this).css({left:  newleft + 'px'});
			newtop = parseInt($(this).attr('top')) + event.pageY - parseInt($(this).attr('starty'));			
			$(this).css({top:  newtop + 'px'});
						
			console.log($(this).attr('top'), $(this).attr('starty'), event.pageY, newtop + 'px' );
			
		}
		
//		$(this).css({cursor:'pointer'});
	});
	
	$('#edit-img img').bind('mousewheel DOMMouseScroll', function(e){
		
		/*width = parseInt($(this).attr('outerWidth')) + e.originalEvent.wheelDelta;
		height = parseInt(width/parseFloat($(this).attr('ratio')));*/
		
		if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
			wheelDelta = 360/e.originalEvent.detail;
//			console.log(e.originalEvent.detail);
		}
		else {
			console.log(e.originalEvent.wheelDelta);
			wheelDelta = e.originalEvent.wheelDelta;
		}
		
		
		width = parseInt($(this).outerWidth()) + wheelDelta;
		height = parseInt(width/parseFloat($(this).attr('ratio')));
		
		$(this).css({width: width, height: height});
		
        if(e.originalEvent.wheelDelta > 0) {
//            $(this).text('scrolling up !');
//            console.log(e.originalEvent.wheelDelta);
            
            
        }
        else{
//            $(this).text('scrolling down !');
//            console.log(e.originalEvent.wheelDelta);
        }
        
        e.preventDefault();
    });
	
//	if(!$(window.parent.document).find('body.rdfxp-html').size()) {
	/*$(window.parent.document).find('#rdfxp-model-classes-properties-tabs').tabs();
	console.log($(window.parent.document).find('#rdfxp-model-classes-properties-tabs').size());*/
//	}
	
	$('.rdfxp-item-list .rel-searched-for').each(function() {
		$searched_for = $(this);
		$li_first = $searched_for.next('.item-list').find('li:eq(0)')
		//$li_first.hide();
		
		$searched_for.parent().css({'position': 'relative'});
		
		$li_first.css({
		  'position': 'absolute',
		  'top': 0,
		  'margin-left': ($searched_for.innerWidth()-15) + 'px',
		  'background-color': '#fff',
		  'padding': '10px',
		  'border': '1px solid #ddd',
		  'z-index': 10,
		  'display': 'none'
		});
		
		$searched_for.add($li_first).mouseenter({'searched_for': $searched_for}, function(e) {
			$searched_for = e.data.searched_for;
			$searched_for.next('.item-list').find('li:eq(0)').show();
		});
		
		$searched_for.add($li_first).mouseleave({'searched_for': $searched_for}, function(e) {
			$searched_for = e.data.searched_for;
			$searched_for.next('.item-list').find('li:eq(0)').hide();
		});
	});
	
	
});