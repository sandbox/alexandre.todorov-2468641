<?php
/**
 * @file
 * Contains \Drupal\rdfxp\Routing\RdfxpRoutes.
 */

namespace Drupal\rdfxp\Routing;
use Symfony\Component\Routing\Route;
/**
 * Defines dynamic routes.
 */
class RdfxpRoutes {

  /**
   * {@inheritdoc}
   */
  public function routes() {
    $routes = array();
//     return $routes;
    $modelmap = rdfxp_modelmap();

//     foreach ($modelmap as $model_id => $model_file) {

      // Declares a single route under the name 'rdfxp.content'.
      // Returns an array of Route objects.
      $routes['rdfxp.model'] = new Route(
          // Path to attach this route to:
          '/rdfxp/{modelId}',
          // Route defaults:
          array(
              '_controller' => '\Drupal\rdfxp\Controller\RdfxpController::model',
//               '_title' => 'Model',
              '_title_callback' => '\Drupal\rdfxp\Controller\RdfxpController::modelTitle',
          ),
          // Route requirements:
          array(
              '_permission'  => 'access content',
          )
      );
//     }

//     $routes['rdfxp.default'] = new Route(
//         // Path to attach this route to:
//         '/rdfxp',
//         // Route defaults:
//         array(
//             '_controller' => '\Drupal\rdfxp\Controller\RdfxpController::model',
//             '_title' => 'Hello',
//       		'modelId' => $model_id,
//         ),
//         // Route requirements:
//         array(
//             '_permission'  => 'access content',
//         )
//     );

//     var_export($routes);

    return $routes;
  }

}