<?php
/**
 * @file
 * Contains \Drupal\example\Form\ExampleForm.
 */

namespace Drupal\rdfxp\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements an example form.
 */
class SparqlForm extends FormBase {

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'rdfxp_sparql_form';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $model_id = NULL) {
//     $form['query'] = array(
//     '#title' => $this->t('Sparql query'),
//     '#type' => 'textarea',
//     '#rows' => 15,
//     '#default_value' => 'CONSTRUCT {?s ?p ?o .} WHERE
//  {
//   ?s rdf:type owl:Class .
//   ?s ?p ?o .
// }'
//   );

//   $form['submit'] = array(
//       '#type' => 'submit',
//       '#value' => $this->t('Submit'),
// //       '#button_type' => 'primary',
//   );

  $form = rdfxp_sparql_form($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    return parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
//     drupal_set_message($this->t('Your phone number is @number', array('@number' => $form_state->getValue('phone_number'))));

  	rdfxp_sparql_form_submit($form, $form_state);

//     krumo($form_state->getValues(), $form_state->getUserInput());exit;
  }

}