<?php
namespace Drupal\rdfxp;


use Drupal\Core\Url;

/**
 * For some object serialization in renderable for filesystem cache. Currently only
 * for Drupal\Core\Url. See drush_rdfxp_cc_model($model_cid) near
 * "\Drupal::config('rdfxp.settings')->get('filesystem_index.enabled')"
 *
 * @todo Potentially there will be a huge number of Drupal objects, #node and so on.
 *   For entity: load entity from $values[entity_key]? Keep caching renderable (better)
 *   or cache rendered html instead (safe)?
 *
 */
class SetState {

  public static function __set_state($values) // Depuis PHP 5.1.0
  {
    // Drupal\Core\Url::__set_state(array(
    switch($values['#object']) {
      case 'Drupal\Core\Url':
        $object = Url::fromRoute($values['routeName'], $values['routeParameters'], $values['options']);
        break;
      default:
        $object = new stdClass;
        $object->values = array();
        foreach ($values as $name => $value) {
          $object->values[$name] = $value;
        }
    }

    return $object;
  }
}