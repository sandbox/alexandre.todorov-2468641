<?php
namespace Drupal\rdfxp\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Sparql' block.
 *
 * @Block(
 *   id = "sparql_block",
 *   admin_label = @Translation("Sparql block"),
 * )
 */
class SparqlBlock extends BlockBase {

	/**
   * {@inheritdoc}
   */
  public function build() {

    $build = array();
    $currentRoute = \Drupal::routeMatch();
    $modelId = $currentRoute->getParameter('modelId');
    if(isset($modelId)) {
      $form = \Drupal::formBuilder()->getForm('Drupal\rdfxp\Form\SparqlForm', $modelId);

      $build = array(
   	    '#type' => 'details',
   	    '#title' => 'Form',
   	    '#weight' => '-999',
        $form,
      );
      if(isset($form['G']['img']) || isset($form['result_html'])) {
        $build['#open'] = TRUE;
      }
    }

    return $build;
  }

}
