<?php
namespace Drupal\rdfxp\Plugin\Block;

use Drupal\Core\Block\BlockBase;
// use Drupal\Core\Routing\CurrentRouteMatch;

/**
 * Provides a 'Rdfnav' block.
 *
 * @Block(
 *   id = "rdfnav_block",
 *   admin_label = @Translation("Rdfnav block"),
 * )
 */
class RdfnavBlock extends BlockBase {

  // Override BlockPluginInterface methods here.

	/**
   * {@inheritdoc}
   */
  public function build() {

    $currentRoute = \Drupal::routeMatch();

    // @TODO: these are interesting individuals of currentRoute service and object
    // rdfxp.test > modelId is in menu.links.yml and not in routing > NULL
    // rdfxp.crge_data_model > modelId is in Url > rdf_rdfxp-object
    // rdfxp.default> modelId is defaults > drush-sparql "Last in model_map"

    $modelId = $currentRoute->getParameter('modelId');
    if(isset($modelId)) {

      $modelmap = rdfxp_modelmap();

      // Model related, but not a valid model i.e. files list.
      if(array_key_exists($modelId, $modelmap) && !isset($modelmap[$modelId])) {
        return array('#markup' => 'Model related: files list');
      }

      $classes = rdfxp_swoop_model_content_build($modelId, 'classes');
      $properties = rdfxp_swoop_model_content_build($modelId, 'properties');

      return
        array(
          $classes,
          $properties,
        ) + array(
          '#attached' => array('library' => array('rdfxp/rdfxp.rdfxp')),
        );
    }

    return array('#markup' => 'Not model related.');
  }

}
