<?php

/**
 * @file
 * Contains \Drupal\user\Plugin\Condition\UserRole.
 */

namespace Drupal\rdfxp\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\Context\ContextDefinition;

/**
 * Provides a 'User Role' condition.
 *
 * @Condition(
 *   id = "rdfxp_model",
 *   label = @Translation("Rdfxp model"),
 *   context = {
 *     "model" = @ContextDefinition("any", label = @Translation("Model"))
 *   }
 * )
 *
 */
class IsModel extends ConditionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['ismodel'] = array(
      '#type' => 'radios',
      '#title' => $this->t('If is a model related page (current route has param modelId)'),
      '#default_value' => $this->configuration['ismodel'],
      '#options' => array_map('\Drupal\Component\Utility\SafeMarkup::checkPlain', array(
        'exceptmodel' => $this->t('Except model'),
        'ismodel' => $this->t('Is model'),
        'any' => $this->t('Any')
      )),
      '#description' => $this->t('If no selections, the condition will evaluate to TRUE.'),
    );
    $form = parent::buildConfigurationForm($form, $form_state);
//     $form['context_mapping']['model']['#options'] = array_merge(array('' => $this->t('Nothing')), $form['context_mapping']['model']['#options']);
//     $form['context_mapping']['model']['#required'] = FALSE;

    // Do not show the select
    //$form['context_mapping']['model']['#type'] = 'value';
    //$form['context_mapping']['model']['#value'] = 'rdfxp.model';
    //krumo($form);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return array(
      'ismodel' => NULL,
    ) + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    //$this->configuration['ismodel'] = array_filter($form_state->getValue('ismodel'));
    $this->configuration['ismodel'] = $form_state->getValue('ismodel');
    if($this->configuration['ismodel'] == 'any') {
      $this->configuration['ismodel'] = NULL;
    }

    parent::submitConfigurationForm($form, $form_state);

    if(empty($this->configuration['ismodel'])) {
//       $this->configuration['context_mapping'] = NULL;
//       unset($this->configuration['context_mapping']);

      // is working
      //$this->configuration = $this->defaultConfiguration();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    return $this->t('Is model') . var_export($this->configuration['ismodel'], true);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    //krumo($this->configuration['ismodel']);
    $model = $this->getContextValue('model');
    //krumo($model);

    if (empty($this->configuration['ismodel']) && !$this->isNegated()) {
      return TRUE;
    }



    return (bool) (
      ($this->configuration['ismodel'] == 'ismodel') && $model instanceof \SwoopModel
      // Doesn't work because there is no context mapping, so evaluate is not called at all
      || ($this->configuration['ismodel'] == 'exceptmodel') && !($model instanceof \SwoopModel)
    );
  }

}
