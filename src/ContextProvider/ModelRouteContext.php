<?php

/**
 * @file
 * Contains \Drupal\rdfxp\ContextProvider\ModelRouteContext.
 */

namespace Drupal\rdfxp\ContextProvider;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\Core\Plugin\Context\ContextProviderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Sets the current model as a context on model routes.
 */
class ModelRouteContext implements ContextProviderInterface {
  use StringTranslationTrait;

  /**
   * The route match object.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a new NodeRouteContext.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match object.
   */
  public function __construct(RouteMatchInterface $route_match) {
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public function getRuntimeContexts(array $unqualified_context_ids) {

    $result = [];
    $modelmap = rdfxp_modelmap();
    // by default context definition is required
    // TODO: test difference from context definition is not required
    // The "rdfxp:model" plugin does not exist
    // TODO: $context = new ContextDefinition('rdfxp:model', ...)) which will remove the context mapping dropdown list
    $context_definition = new ContextDefinition('any', $this->t('Current model'));

    $value = NULL;
    if(($modelId = $this->routeMatch->getParameter('modelId')) && (array_key_exists($modelId, $modelmap))) {


      if($ontModel = rdfxp_get_model($modelId)) {
        $value = $ontModel;
      }
      else {
        // Must be not null for triggering condition evaluation (model related but not valid modelId).
        $value = FALSE;
      }
    }

    $cacheability = new CacheableMetadata();
    $cacheability->setCacheContexts(['route']);

    $context = new Context($context_definition, $value);
    $context->addCacheableDependency($cacheability);
    $result['model'] = $context;

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailableContexts() {
    $context = new Context(new ContextDefinition('any', $this->t('Current model (available)')));
    return ['model' => $context];
  }

}
