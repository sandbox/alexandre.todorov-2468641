<?php

/**
 * @file
 * Contains \Drupal\rdfxp\EventSubscriber\ModelRouteContext.
 */

namespace Drupal\rdfxp\EventSubscriber;

use Drupal\block\EventSubscriber;
use Drupal\block\Event\BlockContextEvent;
use Drupal\block\Event\BlockEvents;
use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\block\EventSubscriber\BlockContextSubscriberBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Sets the current node as a context on node routes.
 */
class ModelRouteContext extends BlockContextSubscriberBase {

  use StringTranslationTrait;
  /**
   * The route match object.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a new NodeRouteContext.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match object.
   */
  public function __construct(RouteMatchInterface $route_match) {
    $this->routeMatch = $route_match;
  }


  /**
   * {@inheritdoc}
   */
  public function onBlockActiveContext(BlockContextEvent $event) {

    if (($route_object = $this->routeMatch->getRouteObject())) { //  && ($route_contexts = $route_object->getOption('parameters'))
      //krumo($modelId = $this->routeMatch->getParameter('modelId'));
    }


    $modelmap = rdfxp_modelmap();
    if(($modelId = $this->routeMatch->getParameter('modelId')) && (array_key_exists($modelId, $modelmap))) {

      $context = new Context(new ContextDefinition('any', $this->t('Current model')));
      // The "rdfxp:model" plugin does not exist
      //$context = new Context(new ContextDefinition('rdfxp:model'));

      if($ontModel = rdfxp_get_model($modelId)) {
        $context->setContextValue($ontModel);
      }
      else {
        $context->setContextValue(NULL);
      }

      $event->setContext('rdfxp.model', $context);
    }
//     else {
//       $context->setContextValue(NULL);
//     }


  }

  /**
   * {@inheritdoc}
   */
  public function onBlockAdministrativeContext(BlockContextEvent $event) {
    $context = new Context(new ContextDefinition('any', $this->t('Current model (admin)')));
    $event->setContext('rdfxp.model', $context);
  }

}
