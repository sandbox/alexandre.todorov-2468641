<?php

/**
 * @file
 * Contains \Drupal\rdfxp\EventSubscriber\ModelRouteContext.
 */

namespace Drupal\rdfxp\EventSubscriber;

use Drupal\rdfxp\EventSubscriber\ModelRouteContext;
// use Drupal\block\EventSubscriber;
use Drupal\block\Event\BlockContextEvent;
// use Drupal\block\Event\BlockEvents;
use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\ContextDefinition;

/**
 * Sets the current node as a context on node routes.
 * @TODO add dependancy to rdfxp.model?
 */
class ModelSiteWideContext extends ModelRouteContext {

  /**
   * {@inheritdoc}
   */
  public function onBlockActiveContext(BlockContextEvent $event) {

    $context = new Context(new ContextDefinition('any', $this->t('Current sitewide model')));

    if(($modelId = $this->routeMatch->getParameter('modelId')) && ($ontModel = rdfxp_get_model($modelId))) {
      $context->setContextValue($ontModel);
    }
    else {
      $context->setContextValue(NULL);
    }

    $event->setContext('rdfxp.model.sitewide', $context);
  }

  public function onBlockAdministrativeContext(BlockContextEvent $event) {
    $context = new Context(new ContextDefinition('any', $this->t('Current model sitewide (admin)')));
    $event->setContext('rdfxp.model.sitewide', $context);
  }
}
