<?php
/**
 * @file
 * Contains \Drupal\example\Controller\ExampleController.
 */

namespace Drupal\rdfxp\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;

class RdfxpController extends ControllerBase {


  public function resourceTitle($modelId, $resource) {

//     $ontModel = rdfxp_get_model($modelId);
//     $baseUri = $ontModel->model->baseURI;

//     $ontResource = new \SwoopResource($baseUri . $resource);
//     $ontResource->setAssociatedModel($ontModel);
//     $ontResource->setVocabulary($ontModel->vocabulary);

//     $swoopOntology = new \SwoopOntology($ontResource);

//     return $swoopOntology->getShortName();
    return $resource;
  }

  /**
   * {@inheritdoc}
   */
  public function resource($modelId, $resource) {

  	 return rdfxp_swoop_resource_build($modelId, $resource);
  }

  public function modelTitle($modelId) {
    return ($strrpos = strrpos($modelId, '__')) !== FALSE? substr($modelId, (int)$strrpos + 2) : $modelId;
  }

  public function model($modelId) {

    $modelmap = rdfxp_modelmap();

//     krumo($modelmap, $modelId);
    if(array_key_exists($modelId, $modelmap) && !isset($modelmap[$modelId])) {
  	  return array('#markup' => 'is dir' );
  	}
  	else {
  	  return array('#markup' => 'is model' );
  	}

    $return = rdfxp_swoop_model($modelId);

    return $return;
  }

  public function svg() {

  	$return = array();


  	$content = rdfxp_swoop_svg();

// krumo($content);

  $response = new Response(
    $content,
    Response::HTTP_OK,
    array('content-type' => 'image/svg+xml')
);

  	return $response;
  }

}